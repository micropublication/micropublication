module.exports = {
  parser: 'babel-eslint',
  env: {
    es6: true,
    browser: true,
  },
  extends: 'pubsweet',
  rules: {
    'sort-keys': 'off',
    'import/no-extraneous-dependencies': [
      'error',
      {
        devDependencies: [
          '.storybook/**',
          'scripts/seedManuscripts.js',
          'scripts/seedUsers.js',
          'server/models/__tests__/**',
          'ui/stories/**',
          'webpack/webpack.development.config.js',
        ],
      },
    ],
    'react/prop-types': [
      2,
      { ignore: ['children', 'className', 'onClick', 'theme'] },
    ],
  },
}

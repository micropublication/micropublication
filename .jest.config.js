module.exports = {
  collectCoverage: false,
  collectCoverageFrom: [
    '**/*.{js,jsx}',
    '!**/*test.{js,jsx}',
    '!**/test/**',
    '!**/node_modules/**',
    '!**/config/**',
    '!**/coverage/**',
  ],
  coverageDirectory: '<rootDir>/coverage',
  projects: [
    // {
    //   displayName: 'app',
    //   // not needed?
    //   moduleNameMapper: {
    //     '\\.s?css$': 'identity-obj-proxy',
    //   },
    //   rootDir: '<rootDir>/app',
    //   // setupTestFrameworkScriptFile: '<rootDir>/test/setup.js',
    //   setupFilesAfterEnv: ['<rootDir>/test/setup.js'],
    //   snapshotSerializers: ['enzyme-to-json/serializer'],
    //   transformIgnorePatterns: ['node_modules/(?!(@?pubsweet|xpub-edit))'],
    // },
    {
      displayName: 'models',
      testEnvironment: 'node',
      testRegex: 'server/models/__tests__/.+test.js$',
    },
  ],
}

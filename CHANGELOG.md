# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

# [0.23.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.22.0...v0.23.0) (2020-06-02)


### Bug Fixes

* **assign reviewers:** trim whitespace in form ([59c4805](https://gitlab.coko.foundation/micropubs/wormbase/commit/59c4805))
* **dashboard:** filter the dashboard when there is stored status ([3258576](https://gitlab.coko.foundation/micropubs/wormbase/commit/3258576))
* **dashboard:** fix duplicate editor section ([aa75f1c](https://gitlab.coko.foundation/micropubs/wormbase/commit/aa75f1c))
* **editor panel:** fix duplicate entries in editor selection dropdown ([e5adb1c](https://gitlab.coko.foundation/micropubs/wormbase/commit/e5adb1c))
* **editor panel:** so panel breaks when main editor is not assigned ([949f96c](https://gitlab.coko.foundation/micropubs/wormbase/commit/949f96c))
* **preview:** show image changes in highlighting ([867629d](https://gitlab.coko.foundation/micropubs/wormbase/commit/867629d))


### Features

* add curator role ([05bedc5](https://gitlab.coko.foundation/micropubs/wormbase/commit/05bedc5))
* add favicon ([3818a8b](https://gitlab.coko.foundation/micropubs/wormbase/commit/3818a8b))
* separate main editors and section editors ([86d3eb1](https://gitlab.coko.foundation/micropubs/wormbase/commit/86d3eb1))
* **api:** add email property to users ([7dcde0c](https://gitlab.coko.foundation/micropubs/wormbase/commit/7dcde0c))
* **api:** add graphql authorization middleware ([967fa00](https://gitlab.coko.foundation/micropubs/wormbase/commit/967fa00))
* **api:** add pubmed api lookup ([3e24778](https://gitlab.coko.foundation/micropubs/wormbase/commit/3e24778))
* **assign reviewers:** include email in reviewer table ([161864e](https://gitlab.coko.foundation/micropubs/wormbase/commit/161864e))
* **assign reviewers:** make email clickable ([f1081a0](https://gitlab.coko.foundation/micropubs/wormbase/commit/f1081a0))
* **models:** migration for adding section editor teams to manuscripts ([839f62d](https://gitlab.coko.foundation/micropubs/wormbase/commit/839f62d))
* **notifications:** email authors on new submission ([6aaed08](https://gitlab.coko.foundation/micropubs/wormbase/commit/6aaed08))
* **reviewer panel:** change labels for reviewer recommendation ([8b43926](https://gitlab.coko.foundation/micropubs/wormbase/commit/8b43926))
* **submission form:** add figure title ([5cf8c18](https://gitlab.coko.foundation/micropubs/wormbase/commit/5cf8c18))
* **submission form:** clarify middle initial entry ([3645981](https://gitlab.coko.foundation/micropubs/wormbase/commit/3645981))
* **submission form:** make suggested reviewer required ([457eb2d](https://gitlab.coko.foundation/micropubs/wormbase/commit/457eb2d))
* **submission form:** note for comments to editor ([9089af1](https://gitlab.coko.foundation/micropubs/wormbase/commit/9089af1))
* **user profile:** add orcid to user ([18d1dbb](https://gitlab.coko.foundation/micropubs/wormbase/commit/18d1dbb))



# [0.22.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.21.0...v0.22.0) (2020-04-30)


### Bug Fixes

* **client:** allow cors requests from client on another port ([edcbb32](https://gitlab.coko.foundation/micropubs/wormbase/commit/edcbb32))
* **editor panel:** only editor can see chats ([43ee9f5](https://gitlab.coko.foundation/micropubs/wormbase/commit/43ee9f5))
* **reviewer panel:** fix case where reviewer would see the form ([2f7866d](https://gitlab.coko.foundation/micropubs/wormbase/commit/2f7866d))
* **submission form:** identify optional fields ([a727ba6](https://gitlab.coko.foundation/micropubs/wormbase/commit/a727ba6))


### Features

* add section editor role ([4f0c2ac](https://gitlab.coko.foundation/micropubs/wormbase/commit/4f0c2ac))
* **dashboard:** filter by status in editor section ([0d6915c](https://gitlab.coko.foundation/micropubs/wormbase/commit/0d6915c))
* **reviewer panel:** ask reviewer if they want to look at revision ([6cfbc3a](https://gitlab.coko.foundation/micropubs/wormbase/commit/6cfbc3a))



# [0.21.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.20.0...v0.21.0) (2020-03-30)

### Bug Fixes

- **team manager:** allow for empty graphql responses ([e48735f](https://gitlab.coko.foundation/micropubs/wormbase/commit/e48735f))

### Features

- **server:** add recurring task support with cron ([649a53a](https://gitlab.coko.foundation/micropubs/wormbase/commit/649a53a))

# [0.20.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.19.2...v0.20.0) (2020-03-23)

### Bug Fixes

- **assign reviewers:** move status section to top ([c8ed432](https://gitlab.coko.foundation/micropubs/wormbase/commit/c8ed432))
- **dashboard:** apply border to reviewer list ([099e6df](https://gitlab.coko.foundation/micropubs/wormbase/commit/099e6df))
- **notifications:** only notify assigned editor when possible ([3b12563](https://gitlab.coko.foundation/micropubs/wormbase/commit/3b12563))
- **submission form:** emails with whitespace fail validation ([bf2a7a7](https://gitlab.coko.foundation/micropubs/wormbase/commit/bf2a7a7))
- **submission form:** remove bold from title field ([b0691f4](https://gitlab.coko.foundation/micropubs/wormbase/commit/b0691f4))
- **user profile:** fix potential page crash ([58a7121](https://gitlab.coko.foundation/micropubs/wormbase/commit/58a7121))

### Features

- **dashboard:** show author in editor dashboard ([341654d](https://gitlab.coko.foundation/micropubs/wormbase/commit/341654d))
- **dashboard:** show conflict of interest disclaimer for reviewers ([db62a79](https://gitlab.coko.foundation/micropubs/wormbase/commit/db62a79))
- **notifications:** send reviewer thank you email on review submission ([681fa5f](https://gitlab.coko.foundation/micropubs/wormbase/commit/681fa5f))
- **preview:** highlight differences between versions ([79aa7bd](https://gitlab.coko.foundation/micropubs/wormbase/commit/79aa7bd))
- **submission form:** add limits to image size & restrict file extensions ([a317e12](https://gitlab.coko.foundation/micropubs/wormbase/commit/a317e12))
- **submission form:** add orcid to authors ([4f2142d](https://gitlab.coko.foundation/micropubs/wormbase/commit/4f2142d))
- **submission form:** allow link creation in text editors ([e640c19](https://gitlab.coko.foundation/micropubs/wormbase/commit/e640c19))
- **submission form:** change text for submission modals ([700c3c3](https://gitlab.coko.foundation/micropubs/wormbase/commit/700c3c3))
- **submission form:** clarify references text ([cc8a740](https://gitlab.coko.foundation/micropubs/wormbase/commit/cc8a740))

## [0.19.2](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.19.1...v0.19.2) (2020-02-19)

### Bug Fixes

- give admin full editor and so access ([cd0c2b2](https://gitlab.coko.foundation/micropubs/wormbase/commit/cd0c2b2))
- **api:** send verification email to invited users when they sign up ([80c2c11](https://gitlab.coko.foundation/micropubs/wormbase/commit/80c2c11))
- **notifications:** fix chat not sent if article is not submitted ([04f69ab](https://gitlab.coko.foundation/micropubs/wormbase/commit/04f69ab))
- **user profile:** sign fields are required ([49c36f1](https://gitlab.coko.foundation/micropubs/wormbase/commit/49c36f1))

## [0.19.1](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.19.0...v0.19.1) (2020-02-11)

### Bug Fixes

- **models:** fix communication history migration script ([e9ab2ea](https://gitlab.coko.foundation/micropubs/wormbase/commit/e9ab2ea))

# [0.19.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.18.0...v0.19.0) (2020-02-11)

### Bug Fixes

- remove validations and print names in export HTML ([971b6f5](https://gitlab.coko.foundation/micropubs/wormbase/commit/971b6f5))
- **api:** update html export to support model changes ([f46272a](https://gitlab.coko.foundation/micropubs/wormbase/commit/f46272a))
- **editor panel:** hide reviewer chat from science officers ([d671b4a](https://gitlab.coko.foundation/micropubs/wormbase/commit/d671b4a))
- **models:** make references column jsonb ([d06101c](https://gitlab.coko.foundation/micropubs/wormbase/commit/d06101c))
- **notifications:** change reinvite email link to dashboard ([b1a3020](https://gitlab.coko.foundation/micropubs/wormbase/commit/b1a3020))
- **preview:** preserve author order ([1108ba9](https://gitlab.coko.foundation/micropubs/wormbase/commit/1108ba9))
- **reviewer panel:** fix possible crash if versions are not fetched yet ([4cfd0f4](https://gitlab.coko.foundation/micropubs/wormbase/commit/4cfd0f4))
- **reviewer panel:** fix review autosave cache not being updated ([38a0863](https://gitlab.coko.foundation/micropubs/wormbase/commit/38a0863))
- **submission form:** allow tables in reagents ([d98b9e7](https://gitlab.coko.foundation/micropubs/wormbase/commit/d98b9e7))
- **submission form:** need to allow more than one corresponding author ([af0e524](https://gitlab.coko.foundation/micropubs/wormbase/commit/af0e524))
- **submission form:** remove laboratory as required from validations ([6ef7999](https://gitlab.coko.foundation/micropubs/wormbase/commit/6ef7999))

### Features

- change labels for version tabs ([ef3f2ae](https://gitlab.coko.foundation/micropubs/wormbase/commit/ef3f2ae))
- editors can chat with authors and reviewers ([81e6e2d](https://gitlab.coko.foundation/micropubs/wormbase/commit/81e6e2d))
- **editor panel:** labels for version tabs ([a9ded5e](https://gitlab.coko.foundation/micropubs/wormbase/commit/a9ded5e))
- **preview:** correspondence to ([c89e4d4](https://gitlab.coko.foundation/micropubs/wormbase/commit/c89e4d4))
- **preview:** hide additional data box from reviewers ([5bc54cc](https://gitlab.coko.foundation/micropubs/wormbase/commit/5bc54cc))
- **reviewer panel:** add revision labels ([d08c29f](https://gitlab.coko.foundation/micropubs/wormbase/commit/d08c29f))
- **reviewer panel:** autosave reviews ([31782a8](https://gitlab.coko.foundation/micropubs/wormbase/commit/31782a8))

# [0.18.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.17.0...v0.18.0) (2020-01-23)

### Bug Fixes

- **dashboard:** fix reviewer invitation response ([807ea69](https://gitlab.coko.foundation/micropubs/wormbase/commit/807ea69))
- **editor panel:** fix split screen height going out of screen ([df4b81a](https://gitlab.coko.foundation/micropubs/wormbase/commit/df4b81a))
- **reviewer panel:** fix form breaking & improve errors ([c3a508c](https://gitlab.coko.foundation/micropubs/wormbase/commit/c3a508c))
- **submission form:** fix affiliations textbox not working ([9e2dadf](https://gitlab.coko.foundation/micropubs/wormbase/commit/9e2dadf))
- **submission form:** fix disclaimer error showing up prematurely ([0ebd057](https://gitlab.coko.foundation/micropubs/wormbase/commit/0ebd057))
- **submission form:** remove calls to flat() for compatibility ([8b60ba6](https://gitlab.coko.foundation/micropubs/wormbase/commit/8b60ba6))
- **submission form:** validat lastName instead of name ([096fa23](https://gitlab.coko.foundation/micropubs/wormbase/commit/096fa23))

### Features

- **editor panel:** reinvite reviewers button ([e3b7a90](https://gitlab.coko.foundation/micropubs/wormbase/commit/e3b7a90))
- **notifications:** expanded email for reviewer invite ([4cc8494](https://gitlab.coko.foundation/micropubs/wormbase/commit/4cc8494))
- **submission form:** add author first/last name ([8d43659](https://gitlab.coko.foundation/micropubs/wormbase/commit/8d43659))
- **submission form:** add submitting / corresponding / equal contribution author checkboxes ([e54f896](https://gitlab.coko.foundation/micropubs/wormbase/commit/e54f896))
- **submission form:** allow mutliple references with doi/pubmedID ([828b207](https://gitlab.coko.foundation/micropubs/wormbase/commit/828b207))
- **submission form:** allow tables in text editor ([d20c748](https://gitlab.coko.foundation/micropubs/wormbase/commit/d20c748))
- **submission form:** error message near submit button ([c7fcecf](https://gitlab.coko.foundation/micropubs/wormbase/commit/c7fcecf))
- **submission form:** multiple affiliations per author ([cf0f2dc](https://gitlab.coko.foundation/micropubs/wormbase/commit/cf0f2dc))
- **submission-form:** split reagents out to its own field ([1f06386](https://gitlab.coko.foundation/micropubs/wormbase/commit/1f06386))

# [0.17.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.16.0...v0.17.0) (2019-12-18)

### Bug Fixes

- change http to https for wbapi ([9d561be](https://gitlab.coko.foundation/micropubs/wormbase/commit/9d561be))
- **assign reviewers:** take care of surname typos ([0d7435e](https://gitlab.coko.foundation/micropubs/wormbase/commit/0d7435e))
- **client:** make buttons a little smaller ([7e9e5e0](https://gitlab.coko.foundation/micropubs/wormbase/commit/7e9e5e0))
- **editor panel:** so button with no value should not say change ([9888f3d](https://gitlab.coko.foundation/micropubs/wormbase/commit/9888f3d))
- **models:** force emails to be lowercase in db ([8055748](https://gitlab.coko.foundation/micropubs/wormbase/commit/8055748))
- **submission form:** improve placeholder text for funding and email ([c6a49c8](https://gitlab.coko.foundation/micropubs/wormbase/commit/c6a49c8))

### Features

- sign up email verification ([c9bfe33](https://gitlab.coko.foundation/micropubs/wormbase/commit/c9bfe33))
- **client:** new spinner ([e7b87de](https://gitlab.coko.foundation/micropubs/wormbase/commit/e7b87de))
- **dashboard:** add dedicated science officer section ([49d949d](https://gitlab.coko.foundation/micropubs/wormbase/commit/49d949d))
- **editor panel:** add reviewer recommendation in full text ([a32addd](https://gitlab.coko.foundation/micropubs/wormbase/commit/a32addd))
- **editor panel:** add time to version tabs ([819a139](https://gitlab.coko.foundation/micropubs/wormbase/commit/819a139))
- **editor panel:** improve reviewer count labels ([2f40807](https://gitlab.coko.foundation/micropubs/wormbase/commit/2f40807))
- **editor panel:** keep version tabs in sync ([bab312c](https://gitlab.coko.foundation/micropubs/wormbase/commit/bab312c))
- **editor panel:** reinstate request reviewer attention buttons ([5fb4f5c](https://gitlab.coko.foundation/micropubs/wormbase/commit/5fb4f5c))

# [0.16.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.15.0...v0.16.0) (2019-11-20)

### Features

- manuscript versions ([3340603](https://gitlab.coko.foundation/micropubs/wormbase/commit/3340603))

# [0.15.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.14.6...v0.15.0) (2019-09-27)

### Bug Fixes

- **notifications:** change request reviewer attention email text ([2bce1d0](https://gitlab.coko.foundation/micropubs/wormbase/commit/2bce1d0))

### Features

- **dashboard:** sort manuscripts in review section by last updated ([03430c0](https://gitlab.coko.foundation/micropubs/wormbase/commit/03430c0))
- **user profile:** add user profile page ([7a7dac8](https://gitlab.coko.foundation/micropubs/wormbase/commit/7a7dac8))

## [0.14.6](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.14.5...v0.14.6) (2019-09-25)

### Bug Fixes

- clear false errors about password reset component ([d0036aa](https://gitlab.coko.foundation/micropubs/wormbase/commit/d0036aa))
- **editor panel:** improve ux for science officer approval ([fa6983d](https://gitlab.coko.foundation/micropubs/wormbase/commit/fa6983d))

## [0.14.5](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.14.4...v0.14.5) (2019-09-24)

### Bug Fixes

- fix production build ([ae7fd23](https://gitlab.coko.foundation/micropubs/wormbase/commit/ae7fd23))

## [0.14.4](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.14.3...v0.14.4) (2019-09-24)

### Bug Fixes

- **models:** add some error handling to external team migrations ([8527d76](https://gitlab.coko.foundation/micropubs/wormbase/commit/8527d76))

## [0.14.3](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.14.2...v0.14.3) (2019-09-24)

### Bug Fixes

- **models:** add some error handling to existing teams migration ([f81496d](https://gitlab.coko.foundation/micropubs/wormbase/commit/f81496d))

## [0.14.2](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.14.1...v0.14.2) (2019-09-24)

### Bug Fixes

- **models:** add some error handling to external user migrations ([a271c2e](https://gitlab.coko.foundation/micropubs/wormbase/commit/a271c2e))

## [0.14.1](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.14.0...v0.14.1) (2019-09-24)

### Bug Fixes

- **models:** user & team migration fixes ([63921b5](https://gitlab.coko.foundation/micropubs/wormbase/commit/63921b5))

# [0.14.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.13.0...v0.14.0) (2019-09-24)

### Bug Fixes

- **reviewer panel:** fix crash when there is no review ([626b4d0](https://gitlab.coko.foundation/micropubs/wormbase/commit/626b4d0))

### Features

- **models:** brand new user & team models ([acb8069](https://gitlab.coko.foundation/micropubs/wormbase/commit/acb8069))

# [0.13.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.12.0...v0.13.0) (2019-09-16)

### Bug Fixes

- fix non-available dependency ([850ed5a](https://gitlab.coko.foundation/micropubs/wormbase/commit/850ed5a))

### Features

- **editor panel:** add visual feedback when notifying reviewers ([e85f271](https://gitlab.coko.foundation/micropubs/wormbase/commit/e85f271))
- **reviewer panel:** add open acknowledgement option to reviews ([73c2c06](https://gitlab.coko.foundation/micropubs/wormbase/commit/73c2c06))

# [0.12.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.11.0...v0.12.0) (2019-09-03)

### Bug Fixes

- **config:** correct typo in email sender address ([887d891](https://gitlab.coko.foundation/micropubs/wormbase/commit/887d891))

### Features

- **editor panel:** change wording for 'ping reviewer button' ([2142742](https://gitlab.coko.foundation/micropubs/wormbase/commit/2142742))
- **editor panel:** give editors the ability to ping reviewers ([210827c](https://gitlab.coko.foundation/micropubs/wormbase/commit/210827c))
- **notifications:** send editors email when revision is submitted ([8d079ad](https://gitlab.coko.foundation/micropubs/wormbase/commit/8d079ad))
- **reviewer panel:** reopen reviews after revision ([da6db80](https://gitlab.coko.foundation/micropubs/wormbase/commit/da6db80))

# [0.11.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.10.0...v0.11.0) (2019-08-05)

### Features

- **editor panel:** hide decision section from science officer ([5b654a7](https://gitlab.coko.foundation/micropubs/wormbase/commit/5b654a7))
- **submission form:** disallow global users to manage own articles ([45de4c9](https://gitlab.coko.foundation/micropubs/wormbase/commit/45de4c9))

# [0.10.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.9.1...v0.10.0) (2019-07-28)

### Features

- **submission form:** autosave submission form in the background ([e31e49f](https://gitlab.coko.foundation/micropubs/wormbase/commit/e31e49f))

## [0.9.1](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.9.0...v0.9.1) (2019-06-19)

### Bug Fixes

- **editor panel:** fix blank screen when loading editor panel ([0ccd74e](https://gitlab.coko.foundation/micropubs/wormbase/commit/0ccd74e))

# [0.9.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.8.1...v0.9.0) (2019-06-04)

### Bug Fixes

- **submission form:** fix coauthor error when all fields are empty ([a306154](https://gitlab.coko.foundation/micropubs/wormbase/commit/a306154))

### Features

- **dashboard:** add ability to assign science officer to manuscript ([9a7aab6](https://gitlab.coko.foundation/micropubs/wormbase/commit/9a7aab6))

## [0.8.1](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.8.0...v0.8.1) (2019-05-14)

### Bug Fixes

- **submission form:** make datatype form appear again ([802b7b4](https://gitlab.coko.foundation/micropubs/wormbase/commit/802b7b4))

# [0.8.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.7.1...v0.8.0) (2019-05-14)

### Features

- **submission form:** re-open form for revisions ([5bfe005](https://gitlab.coko.foundation/micropubs/wormbase/commit/5bfe005))

## [0.7.1](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.7.0...v0.7.1) (2019-05-10)

### Bug Fixes

- **submission form:** prevent empty coauthor from being written to db ([91e53dd](https://gitlab.coko.foundation/micropubs/wormbase/commit/91e53dd))

# [0.7.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.6.0...v0.7.0) (2019-04-05)

### Bug Fixes

- **editor panel:** fix chat messages not being registered ([3a89a1c](https://gitlab.coko.foundation/micropubs/wormbase/commit/3a89a1c))
- **editor panel:** show external reviewer info in editor panel ([1f556f2](https://gitlab.coko.foundation/micropubs/wormbase/commit/1f556f2))

### Features

- **dashboard:** order articles by last updated ([9df9583](https://gitlab.coko.foundation/micropubs/wormbase/commit/9df9583))
- **preview:** export to HTML ([9938046](https://gitlab.coko.foundation/micropubs/wormbase/commit/9938046))

# [0.6.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.5.0...v0.6.0) (2019-03-22)

### Bug Fixes

- **submission form:** fix credit needing double click to open ([c28a98a](https://gitlab.coko.foundation/micropubs/wormbase/commit/c28a98a))
- **submission form:** stop credit breaking author touched state on blur ([8d19a3d](https://gitlab.coko.foundation/micropubs/wormbase/commit/8d19a3d))

### Features

- **submission form:** add no datatype option ([b371001](https://gitlab.coko.foundation/micropubs/wormbase/commit/b371001))

# [0.5.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.4.0...v0.5.0) (2019-03-15)

### Bug Fixes

- style all references to microPublication in camelcase ([a365f7e](https://gitlab.coko.foundation/micropubs/wormbase/commit/a365f7e))

### Features

- **notifications:** change email link for revisions to point to article ([4df91b5](https://gitlab.coko.foundation/micropubs/wormbase/commit/4df91b5))
- **submission form:** allow free text in initial form text fields ([33babcf](https://gitlab.coko.foundation/micropubs/wormbase/commit/33babcf))

<a name="0.4.0"></a>

# [0.4.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.3.0...v0.4.0) (2019-03-04)

### Bug Fixes

- **email:** make sure title html does not introduce line breaks ([f523981](https://gitlab.coko.foundation/micropubs/wormbase/commit/f523981))
- **preview:** remove trailing 'and' from preview with only one author ([1e1e80b](https://gitlab.coko.foundation/micropubs/wormbase/commit/1e1e80b))
- **submission form:** correct text editor keys giving errors ([d7f40cc](https://gitlab.coko.foundation/micropubs/wormbase/commit/d7f40cc))
- **submission form:** validate co-authors correctly ([e0075d4](https://gitlab.coko.foundation/micropubs/wormbase/commit/e0075d4))

### Features

- **submission form:** change initial submission confirmation modal text ([3278303](https://gitlab.coko.foundation/micropubs/wormbase/commit/3278303))

<a name="0.3.0"></a>

# [0.3.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.2.0...v0.3.0) (2019-02-28)

### Bug Fixes

- **dashboard:** display title correctly with styled (italics etc.) ([3a48f2f](https://gitlab.coko.foundation/micropubs/wormbase/commit/3a48f2f))
- **editor panel:** fix chat messages being written on all manuscripts ([e9bcdcc](https://gitlab.coko.foundation/micropubs/wormbase/commit/e9bcdcc))
- **submission form:** fix credit validation passing with no value ([093f9cc](https://gitlab.coko.foundation/micropubs/wormbase/commit/093f9cc))

### Features

- **preview:** list affiliations on separate lines ([64e0034](https://gitlab.coko.foundation/micropubs/wormbase/commit/64e0034))
- **preview:** make title text stylable (italics etc.) ([72192d3](https://gitlab.coko.foundation/micropubs/wormbase/commit/72192d3))
- **preview:** show laboratory under 'additional data' ([ceed9e6](https://gitlab.coko.foundation/micropubs/wormbase/commit/ceed9e6))
- **submission form:** make affiliations required for author input ([bdae897](https://gitlab.coko.foundation/micropubs/wormbase/commit/bdae897))
- **submission form:** show confirmation modal on initial submission ([965dcdd](https://gitlab.coko.foundation/micropubs/wormbase/commit/965dcdd))

<a name="0.2.0"></a>

# [0.2.0](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.1.1...v0.2.0) (2019-02-21)

### Bug Fixes

- **dashboard:** correct dropdown widths back to original ([ce46a37](https://gitlab.coko.foundation/micropubs/wormbase/commit/ce46a37))
- **preview:** correct image caption alignment and paragraph breaks ([f7812ee](https://gitlab.coko.foundation/micropubs/wormbase/commit/f7812ee))
- **preview:** preserve title capitalization ([42dbade](https://gitlab.coko.foundation/micropubs/wormbase/commit/42dbade))
- **submission form:** correct positioning of references placeholder ([44246b0](https://gitlab.coko.foundation/micropubs/wormbase/commit/44246b0))

### Features

- **dashboard:** reviewers can preview article before accepting or not ([756d310](https://gitlab.coko.foundation/micropubs/wormbase/commit/756d310))
- **editor panel:** show author email ([97fc745](https://gitlab.coko.foundation/micropubs/wormbase/commit/97fc745))
- **editor-panel:** send emails for chat messages & remove send to button ([2f60119](https://gitlab.coko.foundation/micropubs/wormbase/commit/2f60119))
- **preview:** change "metadata" to "additional data" ([6340c12](https://gitlab.coko.foundation/micropubs/wormbase/commit/6340c12))
- **preview:** show affiliations instead of lab ([1810655](https://gitlab.coko.foundation/micropubs/wormbase/commit/1810655))
- **submission form:** add explanatory text to contributions dropdown ([d05acb6](https://gitlab.coko.foundation/micropubs/wormbase/commit/d05acb6))
- **submission form:** combine methods & reagents into one field ([4de4f9f](https://gitlab.coko.foundation/micropubs/wormbase/commit/4de4f9f))
- **submission form:** make warning modal on full submission ([822c9b8](https://gitlab.coko.foundation/micropubs/wormbase/commit/822c9b8))
- **submission form:** update laboratory placeholder ([1586b8a](https://gitlab.coko.foundation/micropubs/wormbase/commit/1586b8a))
- **submission form:** update references placeholder ([85101a8](https://gitlab.coko.foundation/micropubs/wormbase/commit/85101a8))
- **ui:** improve modal close icon interaction ([c1f715b](https://gitlab.coko.foundation/micropubs/wormbase/commit/c1f715b))

<a name="0.1.1"></a>

## [0.1.1](https://gitlab.coko.foundation/micropubs/wormbase/compare/v0.1.0...v0.1.1) (2019-02-01)

### Bug Fixes

- **assign reviewers:** resolve external reviewer accept / reject crash ([36bc6e2](https://gitlab.coko.foundation/micropubs/wormbase/commit/36bc6e2))

<a name="0.1.0"></a>

# 0.1.0 (2019-01-31)

### Bug Fixes

- add ignore to babelrc for server ([17d74a0](https://gitlab.coko.foundation/micropubs/wormbase/commit/17d74a0))
- add wb id to expression pattern validation ([158749b](https://gitlab.coko.foundation/micropubs/wormbase/commit/158749b))
- bump ui version to get appbar fix ([eb57d3f](https://gitlab.coko.foundation/micropubs/wormbase/commit/eb57d3f))
- change app name to micropublications ([0566366](https://gitlab.coko.foundation/micropubs/wormbase/commit/0566366))
- correct host for production config ([a5064e8](https://gitlab.coko.foundation/micropubs/wormbase/commit/a5064e8))
- correct order of css properties according to stylelint ([6c776a5](https://gitlab.coko.foundation/micropubs/wormbase/commit/6c776a5))
- correct select style when moving through options with keyboard ([6dff4ba](https://gitlab.coko.foundation/micropubs/wormbase/commit/6dff4ba))
- fix broken layouts when editor panel is open ([5680b5d](https://gitlab.coko.foundation/micropubs/wormbase/commit/5680b5d))
- fix radio group default styles breaking the whole submission page ([77be385](https://gitlab.coko.foundation/micropubs/wormbase/commit/77be385))
- **submission-form:** correct comments placeholder ([07aacd4](https://gitlab.coko.foundation/micropubs/wormbase/commit/07aacd4))
- fix broken ui in autocomplete suggestions list ([b1f5675](https://gitlab.coko.foundation/micropubs/wormbase/commit/b1f5675))
- fix co-author bugs ([0866f8c](https://gitlab.coko.foundation/micropubs/wormbase/commit/0866f8c))
- fix dashboard action underline on hover ([4e43367](https://gitlab.coko.foundation/micropubs/wormbase/commit/4e43367))
- fix dashboard error when there is no assigned editor yet ([61a9898](https://gitlab.coko.foundation/micropubs/wormbase/commit/61a9898))
- fix disclaimer checkbox not responding ([939bf8e](https://gitlab.coko.foundation/micropubs/wormbase/commit/939bf8e))
- fix error when there is no assigned editor in dashboard article ([a7f5953](https://gitlab.coko.foundation/micropubs/wormbase/commit/a7f5953))
- fix for select ui component ([64c442f](https://gitlab.coko.foundation/micropubs/wormbase/commit/64c442f))
- fix observe expression broken change functions ([95ec7cd](https://gitlab.coko.foundation/micropubs/wormbase/commit/95ec7cd))
- fix observe expression inputs losing focus on rerender ([19b3046](https://gitlab.coko.foundation/micropubs/wormbase/commit/19b3046))
- fix positioning of pending review marker on editor panel ([717562a](https://gitlab.coko.foundation/micropubs/wormbase/commit/717562a))
- fix styles for rejection warning ([fe241e0](https://gitlab.coko.foundation/micropubs/wormbase/commit/fe241e0))
- fix text editors becoming readonly on submit ([94b590f](https://gitlab.coko.foundation/micropubs/wormbase/commit/94b590f))
- fix when observe expression validation triggers ([dd32449](https://gitlab.coko.foundation/micropubs/wormbase/commit/dd32449))
- give user permission to read his own user object ([9dba05c](https://gitlab.coko.foundation/micropubs/wormbase/commit/9dba05c))
- impove mouse interaction with icon button in observe expression ([5cec22c](https://gitlab.coko.foundation/micropubs/wormbase/commit/5cec22c))
- make editor dropdown in dashboard not searchable ([4b8dfbf](https://gitlab.coko.foundation/micropubs/wormbase/commit/4b8dfbf))
- make formik and autocomplete play nice ([714b292](https://gitlab.coko.foundation/micropubs/wormbase/commit/714b292))
- **auth:** remove console log ([2cef7b4](https://gitlab.coko.foundation/micropubs/wormbase/commit/2cef7b4))
- **authsome:** doi editing now passes for global users ([794130f](https://gitlab.coko.foundation/micropubs/wormbase/commit/794130f))
- **dashboard:** admin could not see editors section ([30ef23a](https://gitlab.coko.foundation/micropubs/wormbase/commit/30ef23a))
- **dashboard:** editor section item typo ([7cf4c47](https://gitlab.coko.foundation/micropubs/wormbase/commit/7cf4c47))
- **dashboard:** handle team creation when no so is assigned ([6b4dd4f](https://gitlab.coko.foundation/micropubs/wormbase/commit/6b4dd4f))
- **dashboard:** make decision statuses appear in author section ([2bdc11e](https://gitlab.coko.foundation/micropubs/wormbase/commit/2bdc11e))
- **dashboard:** make decision statuses appear in ui ([4c647ed](https://gitlab.coko.foundation/micropubs/wormbase/commit/4c647ed))
- **dashboard:** science officer statuses ([9628931](https://gitlab.coko.foundation/micropubs/wormbase/commit/9628931))
- **dashboard:** update statuses on events on other pages ([855d0a4](https://gitlab.coko.foundation/micropubs/wormbase/commit/855d0a4))
- **data-model:** make editor-SO ping-pong work with new data model ([eb6df19](https://gitlab.coko.foundation/micropubs/wormbase/commit/eb6df19))
- **editor panel:** show previous doi when editing it ([9b31e94](https://gitlab.coko.foundation/micropubs/wormbase/commit/9b31e94))
- **editor panel:** whitelist doi field for editors ([18276c1](https://gitlab.coko.foundation/micropubs/wormbase/commit/18276c1))
- **editor-panel:** fix discussion entry not being registered ([4b608eb](https://gitlab.coko.foundation/micropubs/wormbase/commit/4b608eb))
- **editor-panel:** fix infinite loop caused by setting ribbon state ([f28120c](https://gitlab.coko.foundation/micropubs/wormbase/commit/f28120c))
- **editor-panel:** remove assign reviewers link for so ([a6e6fe0](https://gitlab.coko.foundation/micropubs/wormbase/commit/a6e6fe0))
- **graphql:** remove console log ([08984d3](https://gitlab.coko.foundation/micropubs/wormbase/commit/08984d3))
- **new submission:** empty preview appearing instead of form fixed ([03ad535](https://gitlab.coko.foundation/micropubs/wormbase/commit/03ad535))
- **password reset:** temp reset fix until resolved in pubsweet ([d9c6e1d](https://gitlab.coko.foundation/micropubs/wormbase/commit/d9c6e1d))
- prevent autocomplete selection enter from submitting form ([1e1cc55](https://gitlab.coko.foundation/micropubs/wormbase/commit/1e1cc55))
- refetch dashboard data on reviewer invitation response ([33b230f](https://gitlab.coko.foundation/micropubs/wormbase/commit/33b230f))
- remove extension of team type, since it's now in core ([c4dad18](https://gitlab.coko.foundation/micropubs/wormbase/commit/c4dad18))
- **preview:** add more space between sections ([98a7fab](https://gitlab.coko.foundation/micropubs/wormbase/commit/98a7fab))
- remove gibberish aria label ([0989de9](https://gitlab.coko.foundation/micropubs/wormbase/commit/0989de9))
- specify correct component paths ([1d2f662](https://gitlab.coko.foundation/micropubs/wormbase/commit/1d2f662))
- typo correction ([297d33c](https://gitlab.coko.foundation/micropubs/wormbase/commit/297d33c))
- **preview:** editor styles now show on preview ([6f7d678](https://gitlab.coko.foundation/micropubs/wormbase/commit/6f7d678))
- update configs and change page name ([70d169a](https://gitlab.coko.foundation/micropubs/wormbase/commit/70d169a))
- **preview:** hide metadata before full submission ([d17918a](https://gitlab.coko.foundation/micropubs/wormbase/commit/d17918a))
- **reviewer-panel:** fix reviews not being created ([e3dcf17](https://gitlab.coko.foundation/micropubs/wormbase/commit/e3dcf17))
- **submission:** fix user teams not updated ([0a49f63](https://gitlab.coko.foundation/micropubs/wormbase/commit/0a49f63))
- **submission form:** make methods & reagents optional ([b61a53a](https://gitlab.coko.foundation/micropubs/wormbase/commit/b61a53a))
- **submission form:** update disclaimer text ([e96c8e7](https://gitlab.coko.foundation/micropubs/wormbase/commit/e96c8e7))
- **submission form:** update placeholders ([8208240](https://gitlab.coko.foundation/micropubs/wormbase/commit/8208240))
- **submission-form:** admin is able to view own article's form ([fd63159](https://gitlab.coko.foundation/micropubs/wormbase/commit/fd63159))
- **submission-form:** disable preview ([4a643c4](https://gitlab.coko.foundation/micropubs/wormbase/commit/4a643c4))
- **submission-form:** let editors create articles ([1619b8a](https://gitlab.coko.foundation/micropubs/wormbase/commit/1619b8a))
- **submission-form:** remove comments placeholder ([8bb8c62](https://gitlab.coko.foundation/micropubs/wormbase/commit/8bb8c62))

### Features

- add ability to assign an editor to an article ([ae75dfa](https://gitlab.coko.foundation/micropubs/wormbase/commit/ae75dfa))
- add admin-level team manager ([21b4f77](https://gitlab.coko.foundation/micropubs/wormbase/commit/21b4f77))
- add credit taxonomy to author ([8f39484](https://gitlab.coko.foundation/micropubs/wormbase/commit/8f39484))
- add decision tool to editor panel && make all statuses dynamic ([dac9d11](https://gitlab.coko.foundation/micropubs/wormbase/commit/dac9d11))
- add discuss section to editor panel ([f269f49](https://gitlab.coko.foundation/micropubs/wormbase/commit/f269f49))
- add general info to editor panel ([9043307](https://gitlab.coko.foundation/micropubs/wormbase/commit/9043307))
- add preview of article when it is submitted ([6769b05](https://gitlab.coko.foundation/micropubs/wormbase/commit/6769b05))
- add reject checkbox and ribbon to editor panel ([4fdcab1](https://gitlab.coko.foundation/micropubs/wormbase/commit/4fdcab1))
- add reviewer info section to editor panel ([38a72cc](https://gitlab.coko.foundation/micropubs/wormbase/commit/38a72cc))
- add reviewer section to dashboard ([9013d70](https://gitlab.coko.foundation/micropubs/wormbase/commit/9013d70))
- add reviewsForArticle ([80645d3](https://gitlab.coko.foundation/micropubs/wormbase/commit/80645d3))
- add science officer approval tool to editor panel ([87dff8b](https://gitlab.coko.foundation/micropubs/wormbase/commit/87dff8b))
- add status labels to items in dashboard ([cc7a829](https://gitlab.coko.foundation/micropubs/wormbase/commit/cc7a829))
- add table of reviewers and their status to assign reviewers page ([51da956](https://gitlab.coko.foundation/micropubs/wormbase/commit/51da956))
- add validations for prosemirror elements ([872e6e7](https://gitlab.coko.foundation/micropubs/wormbase/commit/872e6e7))
- add validations with yup on formik ([ae64dbe](https://gitlab.coko.foundation/micropubs/wormbase/commit/ae64dbe))
- all fields get submitted properly to the db ([4e1232a](https://gitlab.coko.foundation/micropubs/wormbase/commit/4e1232a))
- all validations done until new transgene ([6aa2108](https://gitlab.coko.foundation/micropubs/wormbase/commit/6aa2108))
- assign reviewers to corresponding team for article ([43cf5b8](https://gitlab.coko.foundation/micropubs/wormbase/commit/43cf5b8))
- async validation for author now works ([87bf400](https://gitlab.coko.foundation/micropubs/wormbase/commit/87bf400))
- autocomplete with delay and react-autosuggest ([b9bd9ad](https://gitlab.coko.foundation/micropubs/wormbase/commit/b9bd9ad))
- basic dash with delete article works ([e66e21e](https://gitlab.coko.foundation/micropubs/wormbase/commit/e66e21e))
- build all api connections with wb db ([1c131e4](https://gitlab.coko.foundation/micropubs/wormbase/commit/1c131e4))
- build all wb api endpoints with autocomplete in the ui ([4a30d89](https://gitlab.coko.foundation/micropubs/wormbase/commit/4a30d89))
- complete manuscript and review model ([7d131bc](https://gitlab.coko.foundation/micropubs/wormbase/commit/7d131bc))
- conditionally render author's input as read only ([e099bae](https://gitlab.coko.foundation/micropubs/wormbase/commit/e099bae))
- configurable editor ([4eb85da](https://gitlab.coko.foundation/micropubs/wormbase/commit/4eb85da))
- connect gocc with subcellular localization ([d9e7070](https://gitlab.coko.foundation/micropubs/wormbase/commit/d9e7070))
- connect observe expression fields with wormbase db ([44c1f7d](https://gitlab.coko.foundation/micropubs/wormbase/commit/44c1f7d))
- create author team when new submission is created ([3a1b6b7](https://gitlab.coko.foundation/micropubs/wormbase/commit/3a1b6b7))
- create review on invitation accept ([3ddf7a7](https://gitlab.coko.foundation/micropubs/wormbase/commit/3ddf7a7))
- create UI for reviewer panel in article page ([795d436](https://gitlab.coko.foundation/micropubs/wormbase/commit/795d436))
- display accepted and rejected reviewers in assign reviewers page ([71a9e27](https://gitlab.coko.foundation/micropubs/wormbase/commit/71a9e27))
- finish auth first version and preview conditionals ([b52468b](https://gitlab.coko.foundation/micropubs/wormbase/commit/b52468b))
- finish new transgene validations ([0fa0539](https://gitlab.coko.foundation/micropubs/wormbase/commit/0fa0539))
- gene expression validations & observe expression ui ([c3b1969](https://gitlab.coko.foundation/micropubs/wormbase/commit/c3b1969))
- get review from db ([59bfbd6](https://gitlab.coko.foundation/micropubs/wormbase/commit/59bfbd6))
- hide observe expression add button if row is empty ([044f04d](https://gitlab.coko.foundation/micropubs/wormbase/commit/044f04d))
- initial pass at standalone models ([75da9dd](https://gitlab.coko.foundation/micropubs/wormbase/commit/75da9dd))
- initial submission saves properly ([bd84e55](https://gitlab.coko.foundation/micropubs/wormbase/commit/bd84e55))
- introduce editor panel ([005a02e](https://gitlab.coko.foundation/micropubs/wormbase/commit/005a02e))
- introduce initial validations object ([1d2923d](https://gitlab.coko.foundation/micropubs/wormbase/commit/1d2923d))
- keep track of review status ([812f285](https://gitlab.coko.foundation/micropubs/wormbase/commit/812f285))
- mark article as submitted ([9549d64](https://gitlab.coko.foundation/micropubs/wormbase/commit/9549d64))
- metadata preview ([45cf2cf](https://gitlab.coko.foundation/micropubs/wormbase/commit/45cf2cf))
- move decision section in editor panel above discussion section ([a773d89](https://gitlab.coko.foundation/micropubs/wormbase/commit/a773d89))
- observe expression add and remove rows ([45ddeb6](https://gitlab.coko.foundation/micropubs/wormbase/commit/45ddeb6))
- readonly state for observeexpression ([2d196ba](https://gitlab.coko.foundation/micropubs/wormbase/commit/2d196ba))
- remove ability to delete articles from editors ([e815db8](https://gitlab.coko.foundation/micropubs/wormbase/commit/e815db8))
- save review ([44fefda](https://gitlab.coko.foundation/micropubs/wormbase/commit/44fefda))
- show gene expression form once datatype is selected ([fb64258](https://gitlab.coko.foundation/micropubs/wormbase/commit/fb64258))
- show submitted review for reviewer in dashboard ([bb0f4c0](https://gitlab.coko.foundation/micropubs/wormbase/commit/bb0f4c0))
- start building the submission form ([b06ee7d](https://gitlab.coko.foundation/micropubs/wormbase/commit/b06ee7d))
- **\$assignreviewers:** new assign reviewers page ([411f3a3](https://gitlab.coko.foundation/micropubs/wormbase/commit/411f3a3))
- **article-preview:** add observe expression data to preview ([adcb2b7](https://gitlab.coko.foundation/micropubs/wormbase/commit/adcb2b7))
- **assign reviewers:** display external reviewer in table ([00e8b21](https://gitlab.coko.foundation/micropubs/wormbase/commit/00e8b21))
- write invalid wb item for observe expression ([8b7d790](https://gitlab.coko.foundation/micropubs/wormbase/commit/8b7d790))
- **assign reviewers:** normalize teams once external user signs up ([bd17054](https://gitlab.coko.foundation/micropubs/wormbase/commit/bd17054))
- **assign reviewers:** refresh table when inviting external reviewer ([802d739](https://gitlab.coko.foundation/micropubs/wormbase/commit/802d739))
- **assign reviewers:** save user-to-be in 'external' tables ([19876c5](https://gitlab.coko.foundation/micropubs/wormbase/commit/19876c5))
- **assign reviewers:** send email invitation to external reviewer ([c59dc47](https://gitlab.coko.foundation/micropubs/wormbase/commit/c59dc47))
- **assign reviewers:** ui to invite external reviewers ([2430c6b](https://gitlab.coko.foundation/micropubs/wormbase/commit/2430c6b))
- **assign reviewers:** update table when external reviewer added ([cbc8bbc](https://gitlab.coko.foundation/micropubs/wormbase/commit/cbc8bbc))
- **dashboard:** add review submitted status ([2bea245](https://gitlab.coko.foundation/micropubs/wormbase/commit/2bea245))
- **dashboard:** statuses for reviewer invite & accept ([6c5ee32](https://gitlab.coko.foundation/micropubs/wormbase/commit/6c5ee32))
- **editor panel:** add metadata section with editable doi field ([9f2385f](https://gitlab.coko.foundation/micropubs/wormbase/commit/9f2385f))
- write out all validation endpoints ([e7ba74b](https://gitlab.coko.foundation/micropubs/wormbase/commit/e7ba74b))
- **editor-panel:** add validations to decision section ([60fa510](https://gitlab.coko.foundation/micropubs/wormbase/commit/60fa510))
- **editor-panel:** disable accept article when not approved by SO ([3e43b84](https://gitlab.coko.foundation/micropubs/wormbase/commit/3e43b84))
- **editor-panel:** handle existing revise decision ([52f305c](https://gitlab.coko.foundation/micropubs/wormbase/commit/52f305c))
- **editor-panel:** real data for reviewer counts ([59170b2](https://gitlab.coko.foundation/micropubs/wormbase/commit/59170b2))
- **editor-panel:** real data for reviews preview ([fc8136d](https://gitlab.coko.foundation/micropubs/wormbase/commit/fc8136d))
- **editor-panel:** remove reject article section ([6b6c652](https://gitlab.coko.foundation/micropubs/wormbase/commit/6b6c652))
- **editor-panel:** send to editor / SO button ([fe08817](https://gitlab.coko.foundation/micropubs/wormbase/commit/fe08817))
- **editor-panel:** show status for pending SO approval ([df543b1](https://gitlab.coko.foundation/micropubs/wormbase/commit/df543b1))
- **editor-panel:** show suggested reviewers to SO section ([c9a6675](https://gitlab.coko.foundation/micropubs/wormbase/commit/c9a6675))
- **email:** send author email when datatype selected ([ff8cdf8](https://gitlab.coko.foundation/micropubs/wormbase/commit/ff8cdf8))
- **email:** send decision letter email to author ([48e289f](https://gitlab.coko.foundation/micropubs/wormbase/commit/48e289f))
- **email:** send editors email on article full submission ([1f6a768](https://gitlab.coko.foundation/micropubs/wormbase/commit/1f6a768))
- **email:** send editors email when a review is submitted ([f96fa92](https://gitlab.coko.foundation/micropubs/wormbase/commit/f96fa92))
- **email:** send editors email when so changes approval status ([34b8496](https://gitlab.coko.foundation/micropubs/wormbase/commit/34b8496))
- **email:** send email to editors on initial submission ([3eadd36](https://gitlab.coko.foundation/micropubs/wormbase/commit/3eadd36))
- **email:** send email to editors on reviewer invitation response ([d71d977](https://gitlab.coko.foundation/micropubs/wormbase/commit/d71d977))
- update graphql query and mutation to submit final stage of form ([0430505](https://gitlab.coko.foundation/micropubs/wormbase/commit/0430505))
- **email:** send email to so/editor when attention requested ([f588d90](https://gitlab.coko.foundation/micropubs/wormbase/commit/f588d90))
- **email:** send reviewer invitation email ([3ca677e](https://gitlab.coko.foundation/micropubs/wormbase/commit/3ca677e))
- **preview:** hide empty sections and fields ([f64c46c](https://gitlab.coko.foundation/micropubs/wormbase/commit/f64c46c))
- **private:** check for external team membership on entering the app ([2360ff6](https://gitlab.coko.foundation/micropubs/wormbase/commit/2360ff6))
- **submission form:** add author affiliations text fields ([5b18c9b](https://gitlab.coko.foundation/micropubs/wormbase/commit/5b18c9b))
- **submission form:** add explanatory text for author input ([d5b398a](https://gitlab.coko.foundation/micropubs/wormbase/commit/d5b398a))
- **submission form:** add image caption field ([769e8dc](https://gitlab.coko.foundation/micropubs/wormbase/commit/769e8dc))
- **submission-form:** add article preview button ([43d2785](https://gitlab.coko.foundation/micropubs/wormbase/commit/43d2785))
- **submission-form:** add disclaimer text ([f234d4b](https://gitlab.coko.foundation/micropubs/wormbase/commit/f234d4b))
- **submission-form:** add link to credit dropdown ([5d25672](https://gitlab.coko.foundation/micropubs/wormbase/commit/5d25672))
- **submission-form:** add methods, reagents & references to initial ([a6c3f87](https://gitlab.coko.foundation/micropubs/wormbase/commit/a6c3f87))
- **submission-form:** author can see a live preview of the article ([b21b087](https://gitlab.coko.foundation/micropubs/wormbase/commit/b21b087))
- **submission-form:** change label for pattern description ([2ecea7f](https://gitlab.coko.foundation/micropubs/wormbase/commit/2ecea7f))
- **submission-form:** move title field before image ([42fb43c](https://gitlab.coko.foundation/micropubs/wormbase/commit/42fb43c))
- **submission-form:** update comments label ([993ad89](https://gitlab.coko.foundation/micropubs/wormbase/commit/993ad89))
- **submission-form:** update credit placeholder ([d47adf0](https://gitlab.coko.foundation/micropubs/wormbase/commit/d47adf0))
- validate observe expression field group ([83d1a88](https://gitlab.coko.foundation/micropubs/wormbase/commit/83d1a88))
- wire ui up with pubsweet db ([3a532a6](https://gitlab.coko.foundation/micropubs/wormbase/commit/3a532a6))
- wire up observe expression with formik ([943d87f](https://gitlab.coko.foundation/micropubs/wormbase/commit/943d87f))
- working dataType select authsome mode ([b0f4106](https://gitlab.coko.foundation/micropubs/wormbase/commit/b0f4106))

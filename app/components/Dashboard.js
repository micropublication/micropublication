/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { State } from 'react-powerplug'

import { Action as UIAction } from '@pubsweet/ui'

import {
  ArticlePreviewModal,
  AuthorSectionItem,
  EditorSectionItem,
  ReviewerSectionItem,
  Section,
} from './ui'

import ComposedDashboard from './compose/Dashboard'
import Loading from './Loading'
import { DASHBOARD_MANUSCRIPTS } from './compose/pieces/dashboardManuscripts'
import { CURRENT_USER } from './Private'
import Select from './ui/Select'

const SubmitButton = props => {
  const { client, createManuscript, currentUser, history } = props

  const onClick = () => {
    createManuscript().then(res => {
      const manuscriptId = res.data.createManuscript

      /* 
        TO DO -- This needs to go. See comment in mutation.
      */
      client
        .query({
          fetchPolicy: 'network-only',
          query: CURRENT_USER,
        })
        .then(() => {
          client.query({
            fetchPolicy: 'network-only',
            query: DASHBOARD_MANUSCRIPTS,
            variables: { reviewerId: currentUser.id },
          })

          history.push(`/article/${manuscriptId}`)
        })
    })
  }

  return (
    <Action data-test-id="new-submission-button" onClick={onClick}>
      New Submission
    </Action>
  )
}

const Action = styled(UIAction)`
  line-height: unset;
`

const DashboardWrapper = styled.div`
  margin: 0 auto;
  max-width: 1024px;
`

const Dashboard = props => {
  const {
    allCurators,
    allEditors,
    allScienceOfficers,
    allSectionEditors,
    authorArticles,
    client,
    createManuscript,
    curatorArticles,
    currentUser,
    deleteArticle,
    editorArticles,
    handleInvitation,
    history,
    loading,
    reviewerArticles,
    scienceOfficerArticles,
    sectionEditorArticles,
    updateAssignedEditor,
    updateAssignedScienceOfficer,
  } = props

  if (loading) return <Loading />

  const isAdmin = currentUser.admin
  const isEditor = currentUser.auth.isGlobalEditor
  const isScienceOfficer = currentUser.auth.isGlobalScienceOfficer
  const isSectionEditor = currentUser.auth.isGlobalSectionEditor
  const isCurator = currentUser.auth.isGlobalCurator

  const options = [
    {
      label: 'Submitted - Needs Triage',
      value: 'submitted - needs triage',
    },
    {
      label: 'Datatype Selected',
      value: 'datatype selected',
    },
    {
      label: 'Submitted',
      value: 'submitted',
    },
    {
      label: 'Approved By Science Officer',
      value: 'approved by science officer',
    },
    {
      label: 'Not Approved By Science Officer',
      value: 'not approved by science officer',
    },
    {
      label: 'Reviewer Invited',
      value: 'reviewer invited',
    },
    {
      label: 'Reviewer Accepted',
      value: 'reviewer accepted',
    },
    {
      label: 'Review Submitted',
      value: 'review submitted',
    },
    {
      label: 'Under Revision',
      value: 'under revision',
    },
    {
      label: 'Declined',
      value: 'declined',
    },
    {
      label: 'Rejected',
      value: 'rejected',
    },
    {
      label: 'Published',
      value: 'published',
    },
    {
      label: 'Accepted To Proofs',
      value: 'accepted to proofs',
    },
    {
      label: 'Revision Submitted',
      value: 'revision submitted',
    },
    {
      label: 'Proofs Submitted',
      value: 'proofs submitted',
    },
    {
      label: 'Editorial Re-submission',
      value: 'editorial re-submission',
    },
  ]

  const globalTeamMembers = [
    ...allScienceOfficers,
    ...allCurators,
    ...allEditors,
    ...allSectionEditors,
  ]

  const teamMemberOptions = globalTeamMembers.map(member => ({
    label: member.displayName,
    value: member.id,
  }))

  const headerActions = [
    <SubmitButton
      client={client}
      createManuscript={createManuscript}
      currentUser={currentUser}
      history={history}
      key="createManuscript"
    />,
  ]

  const storedStatuses = localStorage.getItem('selectedStatuses')
    ? JSON.parse(localStorage.getItem('selectedStatuses'))
    : []

  const initialArticles = () => {
    if (storedStatuses.length > 0) {
      return editorArticles.filter(article =>
        storedStatuses.map(s => s.value).includes(article.displayStatus),
      )
    }

    return editorArticles.filter(
      article =>
        !['published', 'rejected', 'declined'].includes(article.displayStatus),
    )
  }

  return (
    <State
      initial={{
        filteredArticles: initialArticles(),
        previewData: null,
        savedStatuses: storedStatuses,
        showModal: false,
        teamFilter: null,
      }}
    >
      {({ state, setState }) => {
        const {
          previewData,
          showModal,
          savedStatuses,
          teamMemberFilter,
        } = state

        const openModal = article =>
          setState({
            previewData: article,
            showModal: true,
          })

        const closeModal = () =>
          setState({
            previewData: null,
            showModal: false,
          })

        const openReviewerPreviewModal = articleId => {
          const article = reviewerArticles.find(a => a.id === articleId)
          openModal(article)
        }

        const filterArticles = (selectedStatuses, teamMember) => {
          const filteredArticles =
            selectedStatuses.length > 0
              ? editorArticles.filter(article =>
                  selectedStatuses
                    .map(s => s.value)
                    .includes(article.displayStatus),
                )
              : editorArticles.filter(
                  article =>
                    !['published', 'rejected', 'declined'].includes(
                      article.displayStatus,
                    ),
                )

          if (teamMember) {
            filterByTeamMember(teamMember, filteredArticles)
          } else {
            setState({ filteredArticles })
          }
        }

        const handleSelectedStatuses = selectedStatuses => {
          localStorage.setItem(
            'selectedStatuses',
            JSON.stringify(selectedStatuses),
          )
          setState({ savedStatuses: selectedStatuses })
          filterArticles(selectedStatuses, state.teamMemberFilter)
        }

        const deleteFilterArticle = articleId => {
          deleteArticle({ variables: { id: articleId } }).then(() => {
            const filteredArticles = state.filteredArticles.filter(
              article => article.id !== articleId,
            )
            setState({ filteredArticles })
          })
        }

        const filterByTeamMember = (teamMember, articles) => {
          const teamTypes = [
            'curator',
            'scienceOfficer',
            'editor',
            'sectionEditor',
          ]

          const filteredArticles = articles.filter(article =>
            teamTypes.some(
              type => article[type] && article[type].id === teamMember.value,
            ),
          )
          setState({ filteredArticles })
        }

        const handleSelectedTeamMember = teamMember => {
          setState({ teamMemberFilter: teamMember })
          if (teamMember) {
            filterByTeamMember(teamMember, state.filteredArticles)
          } else {
            filterArticles(savedStatuses, teamMember)
          }
        }

        const editorActions = [
          <Select
            closeMenuOnSelect={false}
            isMulti
            onChange={handleSelectedStatuses}
            options={options}
            placeholder="Filter By Status"
            value={savedStatuses}
          />,
          <Select
            isClearable
            isSearchable
            onChange={handleSelectedTeamMember}
            options={teamMemberOptions}
            placeholder="Filter By Team Member"
            value={teamMemberFilter}
          />,
        ]

        return (
          <>
            <DashboardWrapper>
              <Section
                actions={headerActions}
                deleteArticle={deleteArticle}
                itemComponent={AuthorSectionItem}
                items={authorArticles}
                label="My Articles"
              />

              <Section
                handleInvitation={handleInvitation}
                itemComponent={ReviewerSectionItem}
                items={reviewerArticles}
                label="Reviews"
                openReviewerPreviewModal={openReviewerPreviewModal}
              />

              {(isEditor || isAdmin) && (
                <Section
                  actions={editorActions}
                  allEditors={allEditors}
                  allScienceOfficers={allScienceOfficers}
                  deleteArticle={deleteFilterArticle}
                  isAdmin={isAdmin}
                  itemComponent={EditorSectionItem}
                  items={state.filteredArticles}
                  label="Editor Section"
                  updateAssignedEditor={updateAssignedEditor}
                  updateAssignedScienceOfficer={updateAssignedScienceOfficer}
                  variant="editor"
                />
              )}

              {isSectionEditor && !isEditor && (
                <Section
                  actions={editorActions}
                  allEditors={allEditors}
                  allScienceOfficers={allScienceOfficers}
                  itemComponent={EditorSectionItem}
                  items={sectionEditorArticles}
                  label="Editor Section"
                  updateAssignedEditor={updateAssignedEditor}
                  updateAssignedScienceOfficer={updateAssignedScienceOfficer}
                  variant="editor"
                />
              )}

              {isScienceOfficer && (
                <Section
                  itemComponent={EditorSectionItem}
                  items={scienceOfficerArticles}
                  label="Science Officer Section"
                />
              )}

              {isCurator && (
                <Section
                  itemComponent={EditorSectionItem}
                  items={curatorArticles}
                  label="Curator Section"
                />
              )}
            </DashboardWrapper>

            <ArticlePreviewModal
              article={previewData}
              isOpen={showModal}
              onRequestClose={closeModal}
            />
          </>
        )
      }}
    </State>
  )
}

const Composed = props => <ComposedDashboard render={Dashboard} {...props} />
export default Composed

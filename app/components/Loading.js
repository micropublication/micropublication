/* eslint-disable react/prop-types */

import React from 'react'
import styled, { keyframes } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

// From loading.io/css
// const Spinner = styled.div`
//   display: inline-block;
//   height: 64px;
//   width: 64px;

//   &:after {
//     animation: ${rotate360} 1s linear infinite;
//     border: 5px solid ${th('colorPrimary')};
//     border-color: ${th('colorPrimary')} transparent ${th('colorPrimary')}
//       transparent;
//     border-radius: 50%;
//     content: ' ';
//     display: block;
//     height: 46px;
//     margin: 1px;
//     width: 46px;
//   }
// `

// From https://tobiasahlin.com/spinkit/
const animation = keyframes`
  0% { 
    transform: perspective(120px) rotateX(0deg) rotateY(0deg);
  } 50% { 
    transform: perspective(120px) rotateX(-180.1deg) rotateY(0deg);
  } 100% { 
    transform: perspective(120px) rotateX(-180deg) rotateY(-179.9deg);
  }
`

const Spinner = styled.div`
  animation: ${animation} 1.2s infinite ease-in-out;
  background-color: ${th('colorPrimary')};
  height: calc(${th('gridUnit')} * 6);
  width: calc(${th('gridUnit')} * 6);
`

const LoadingPage = styled.div`
  align-items: center;
  display: flex;
  height: 100%;
  justify-content: center;
  padding-bottom: calc(${th('gridUnit')} * 2);
`

export default ({ className }) => (
  <LoadingPage className={className}>
    <Spinner />
  </LoadingPage>
)

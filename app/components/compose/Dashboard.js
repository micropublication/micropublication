/* eslint-disable react/prop-types */

import React from 'react'
import { Adopt } from 'react-adopt'
import { withApollo } from '@apollo/react-hoc'
import { withRouter } from 'react-router-dom'
import { last, maxBy } from 'lodash'

import { withCurrentUser } from '../../userContext'

import {
  // queries
  dashboardManuscripts as dashboardManuscriptsQuery,
  getGlobalTeams as getGlobalTeamsQuery,

  // mutations
  createManuscript,
  deleteArticle,
  handleInvitation,
  updateAssignedEditor,
  updateAssignedScienceOfficer,
} from './pieces'

/* eslint-disable sort-keys */
const mapper = {
  createManuscript,
  deleteArticle,
  dashboardManuscriptsQuery,
  getGlobalTeamsQuery,
  handleInvitation,
  updateAssignedEditor,
  updateAssignedScienceOfficer,
}
/* eslint-enable sort-keys */

const getTeamByType = (teams, type) => teams && teams.find(t => t.role === type)

const getCurrentStatus = manuscript => {
  const { versions } = manuscript
  const versionCount = versions.length
  const lastVersion = last(versions)

  if (lastVersion.decision === 'reject') return 'rejected'
  if (lastVersion.decision === 'publish') return 'published'
  if (lastVersion.decision === 'decline') return 'declined'

  if (lastVersion.reviews) {
    const hasSubmittedReview = !!lastVersion.reviews.find(
      r => r.status.submitted,
    )
    if (hasSubmittedReview) return 'review submitted'
  }

  if (versionCount > 1) {
    const beforeLastVersion = versions[versions.length - 2]
    if (!lastVersion.submitted && beforeLastVersion.decision === 'revise') {
      const hasSubmittedReview = !!beforeLastVersion.reviews.find(
        r => r.status.submitted,
      )
      if (!hasSubmittedReview) return 'editorial re-submission'
      return 'under revision'
    }
    if (lastVersion.submitted && beforeLastVersion.decision === 'revise') {
      return 'revision submitted'
    }
    if (!lastVersion.submitted && beforeLastVersion.decision === 'accept') {
      return 'accepted to proofs'
    }
    if (lastVersion.submitted && beforeLastVersion.decision === 'accept') {
      return 'proofs submitted'
    }
  }

  if (lastVersion.teams) {
    const reviewerTeam = lastVersion.teams.find(t => t.role === 'reviewer')

    const hasAcceptedReviewers = !!reviewerTeam.members.find(
      m => m.status === 'acceptedInvitation',
    )
    if (hasAcceptedReviewers) return 'reviewer accepted'

    const hasInvitedReviewers = !!reviewerTeam.members.find(
      m => m.status === 'invited',
    )
    if (hasInvitedReviewers) return 'reviewer invited'
  }

  if (lastVersion.isApprovedByScienceOfficer)
    return 'approved by science officer'
  if (lastVersion.isApprovedByScienceOfficer === false)
    return 'not approved by science officer'

  if (lastVersion.submitted) return 'submitted'
  if (manuscript.isDataTypeSelected) return 'datatype selected'
  if (manuscript.isInitiallySubmitted) return 'submitted - needs triage'

  return 'not submitted'
}

/* eslint-disable-next-line arrow-body-style */
const mapProps = args => {
  const { data } = args.dashboardManuscriptsQuery
  const { data: globalTeamsData } = args.getGlobalTeamsQuery

  const globalTeams = globalTeamsData && globalTeamsData.globalTeams

  const makeArticleData = type =>
    data &&
    data[type] &&
    data[type].map(manuscript => {
      let version
      if (type === 'author') {
        version = last(manuscript.versions)
      } else if (
        manuscript.versions.length === 1 &&
        manuscript.isInitiallySubmitted &&
        !last(manuscript.versions).submitted
      ) {
        /* eslint-disable-next-line prefer-destructuring */
        version = manuscript.versions[0]
      } else {
        version = last(manuscript.versions.filter(v => v.submitted))
      }

      let formattedData = {
        displayStatus: getCurrentStatus(manuscript),
        id: manuscript.id,
        status: {},
        title: version.title,
        updated: version.updated,
      }

      formattedData.status.decision = version.decision
      formattedData.status.submission = {
        datatypeSelected: manuscript.isDataTypeSelected,
        full: version.submitted,
        initial: manuscript.isInitiallySubmitted,
      }
      formattedData.status.scienceOfficer = {
        approved: version.isApprovedByScienceOfficer,
        pending: version.isApprovedByScienceOfficer === null,
      }

      if (type === 'review')
        formattedData.reviewerStatus = manuscript.reviewerStatus

      if (type === 'editor' || type === 'sectionEditor') {
        const authorTeam = version.teams.find(t => t.role === 'author')
        const authorIds = authorTeam.members.map(m => m.user.id)
        formattedData.authorIds = authorIds

        const submittingAuthor = version.authors.find(a => a.submittingAuthor)
        formattedData.author = submittingAuthor

        const curatorTeam = manuscript.teams.find(t => t.role === 'curator')
        const curators =
          curatorTeam.members.length > 0 &&
          curatorTeam.members.map(member => member.user)

        formattedData.curator = curators

        const editorTeam = manuscript.teams.find(t => t.role === 'editor')
        const editor = editorTeam.members[0] && editorTeam.members[0].user
        formattedData.editor = editor

        const scienceOfficerTeam = manuscript.teams.find(
          t => t.role === 'scienceOfficer',
        )
        const scienceOfficer =
          scienceOfficerTeam.members[0] && scienceOfficerTeam.members[0].user
        formattedData.scienceOfficer = scienceOfficer

        const sectionEditorTeam = manuscript.teams.find(
          t => t.role === 'sectionEditor',
        )
        const sectionEditor =
          sectionEditorTeam.members[0] && sectionEditorTeam.members[0].user
        formattedData.sectionEditor = sectionEditor
      }

      if (type === 'reviewer') {
        const latestReview = last(version.reviews)
        const reviewSubmitted = latestReview && latestReview.status.submitted

        formattedData.reviewerStatus = manuscript.reviewerStatus
        formattedData.reviewSubmitted = reviewSubmitted

        if (manuscript.reviewerStatus === 'invited') {
          formattedData.dataType = manuscript.dataType
          const {
            acknowledgements,
            authors,
            comments,
            funding,
            geneExpression,
            image,
            imageCaption,
            imageTitle,
            laboratory,
            methods,
            reagents,
            patternDescription,
            references,
            suggestedReviewer,
          } = version

          formattedData = {
            ...formattedData,
            acknowledgements,
            authors,
            comments,
            funding,
            geneExpression,
            image,
            imageCaption,
            imageTitle,
            laboratory,
            methods,
            patternDescription,
            reagents,
            references,
            suggestedReviewer,
          }
        }
      }

      return formattedData
    })

  const authorArticles = makeArticleData('author')
  const curatorArticles = makeArticleData('curator')
  const reviewerArticles = makeArticleData('reviewer')
  const editorArticles = makeArticleData('editor')
  const scienceOfficerArticles = makeArticleData('scienceOfficer')
  const sectionEditorArticles = makeArticleData('sectionEditor')

  const handleInvitationFn = (action, manuscriptId) => {
    const manuscript =
      data && data.reviewer && data.reviewer.find(m => manuscriptId === m.id)
    if (!manuscript) throw new Error('Handle Invitation: Invalid manuscript id')
    const latestVersion = maxBy(manuscript.versions, 'created')

    return args.handleInvitation.handleInvitation({
      variables: {
        action,
        articleVersionId: latestVersion.id,
      },
    })
  }

  const loading =
    args.getGlobalTeamsQuery.loading || args.dashboardManuscriptsQuery.loading

  const allEditors =
    globalTeams &&
    getTeamByType(globalTeams, 'editors').members.map(m => m.user)

  const allScienceOfficers =
    globalTeams &&
    getTeamByType(globalTeams, 'scienceOfficers').members.map(m => m.user)

  const allCurators =
    globalTeams &&
    getTeamByType(globalTeams, 'globalCurator').members.map(m => m.user)
  const allSectionEditors =
    globalTeams &&
    getTeamByType(globalTeams, 'globalSectionEditor').members.map(m => m.user)

  return {
    allCurators,
    allEditors,
    allScienceOfficers,
    allSectionEditors,
    authorArticles,
    curatorArticles,
    createManuscript: args.createManuscript.createManuscript,
    deleteArticle: args.deleteArticle.deleteArticle,
    editorArticles,
    handleInvitation: handleInvitationFn,
    loading,
    reviewerArticles,
    scienceOfficerArticles,
    sectionEditorArticles,
    updateAssignedEditor: args.updateAssignedEditor.updateAssignedEditor,
    updateAssignedScienceOfficer:
      args.updateAssignedScienceOfficer.updateAssignedScienceOfficer,
  }
}

const Composed = ({ currentUser, render, ...props }) => (
  <Adopt mapper={mapper} mapProps={mapProps}>
    {mappedProps => render({ ...props, ...mappedProps, currentUser })}
  </Adopt>
)

export default withApollo(withRouter(withCurrentUser(Composed)))

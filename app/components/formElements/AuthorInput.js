/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { get, has, omit } from 'lodash'

import { Icon as UIIcon, Button } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import { getWBPerson } from '../../fetch/WBApi'
import { onAutocompleteChange, onSuggestionSelected } from './helpers'
import { Credit } from './index'
import AutoComplete from './AutoComplete'
import TextField from './TextField'
import Checkbox from './Checkbox'

const Wrapper = styled.div`
  align-items: flex-end;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  margin-bottom: calc(${th('gridUnit')} * 2);
`

const StyledAutoComplete = styled(AutoComplete)`
  margin-bottom: 0;
  margin-right: ${th('gridUnit')};
`

const StyledTextField = styled(TextField)`
  margin-bottom: 0;
  margin-right: ${th('gridUnit')};
`

const Label = styled.label`
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  padding: 0;
`

const Error = styled.span`
  color: ${th('colorError')};
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  padding-left: ${th('gridUnit')};
`

const Icon = styled(UIIcon)`
  svg {
    height: calc(${th('gridUnit')} * 2);
    stroke: ${th('colorFurniture')};
  }
`

const AffiliationsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: left;
  margin-bottom: ${th('gridUnit')};
`

const AffiliationLine = styled.div`
  display: flex;
  flex-direction: row;
`

const QuestionMark = () => (
  <a
    href="http://credit.niso.org"
    rel="noopener noreferrer"
    target="_blank"
    title="See CRediT for definition of roles."
  >
    <Icon>help_circle</Icon>
  </a>
)

const IconButton = styled(Button)`
  min-width: calc(${th('gridUnit')} * 6);
`

const AuthorInput = props => {
  const {
    errors,
    handleChange,
    label,
    name,
    readOnly,
    required,
    setFieldTouched,
    setFieldValue,
    touched,
    values,
  } = props

  const authorFirstName = `${name}.firstName`
  const authorLastName = `${name}.lastName`
  const creditName = `${name}.credit`
  const affiliationsName = `${name}.affiliations`
  const emailName = `${name}.email`
  const isCorrespondingName = `${name}.correspondingAuthor`
  const isSubmittingName = `${name}.submittingAuthor`
  const equalContributionName = `${name}.equalContribution`
  const orcidName = `${name}.orcid`

  const affiliations = get(values, affiliationsName)

  const err =
    get(errors, authorFirstName) ||
    get(errors, authorLastName) ||
    get(errors, creditName) ||
    get(errors, affiliationsName) ||
    get(errors, emailName)

  const touchedThis = get(touched, name)

  const autoCompleteProps = omit(props, [
    'label',
    'name',
    'value',
    'placeholder',
    'required',
  ])

  const handleAddAffiliations = () => {
    const { value } = props
    value.affiliations.push('')
    setFieldValue(name, value)
  }

  const handleRemoveAffiliation = i => {
    const affiliationsRemoved = get(values, affiliationsName).filter(
      (_, index) => index !== i,
    )
    setFieldValue(affiliationsName, affiliationsRemoved)
    handleChange(affiliationsName)
  }

  const submittingAuthorText = (
    <React.Fragment>
      Submitting author
      <Icon title="The submitting author is responsible for all correspondences during the submission, review, and revision stages of the article.">
        help_circle
      </Icon>
    </React.Fragment>
  )

  const correspondingAuthorText = (
    <React.Fragment>
      Corresponding author
      <Icon title="The corresponding author is responsible for all correspondences concerning the article after publication.">
        help_circle
      </Icon>
    </React.Fragment>
  )

  return (
    <div data-test-id={`authorinput-${name}`}>
      {label && (
        <Label>
          {label} {required && ' *'}{' '}
          {touchedThis && err && <Error>{err}</Error>}
        </Label>
      )}

      <Wrapper>
        <StyledAutoComplete
          data-test-id={`authorinput-${authorFirstName}`}
          error={get(errors, authorFirstName)}
          fetchData={getWBPerson}
          hideErrorMessage
          name={authorFirstName}
          onChange={e =>
            onAutocompleteChange(
              e,
              authorFirstName,
              setFieldValue,
              handleChange,
            )
          }
          onSuggestionSelected={onSuggestionSelected}
          placeholder="First name e.g. John W"
          required
          value={get(values, authorFirstName)}
          {...autoCompleteProps}
        />

        <StyledTextField
          data-test-id={`authorinput-${authorLastName}`}
          error={get(errors, authorLastName)}
          hideErrorMessage
          name={authorLastName}
          placeholder="Last name"
          required
          value={get(values, authorLastName)}
          {...autoCompleteProps}
        />
      </Wrapper>

      <Wrapper>
        <Credit
          name={creditName}
          readOnly={readOnly}
          setFieldTouched={setFieldTouched}
          setFieldValue={setFieldValue}
          touched={touchedThis}
          values={get(values, creditName)}
        />

        <QuestionMark />
      </Wrapper>

      <AffiliationsWrapper>
        <Label>Affiliations *</Label>
        {affiliations &&
          affiliations.length > 0 &&
          affiliations.map((item, i) => (
            /* eslint-disable-next-line react/no-array-index-key */
            <AffiliationLine key={`${affiliationsName}-${i}`}>
              <StyledTextField
                name={`${affiliationsName}[${i}]`}
                value={item}
                {...autoCompleteProps}
                error={has(errors, affiliationsName)}
                placeholder="Enter affiliation" // override placeholder
              />
              {affiliations.length > 1 && !readOnly && (
                <IconButton
                  onClick={() => handleRemoveAffiliation(i)}
                  title="Remove affiliation"
                  value={i}
                >
                  <Icon>minus_circle</Icon>
                </IconButton>
              )}
              {i === affiliations.length - 1 && !readOnly && (
                <Button onClick={handleAddAffiliations}>Add Affiliation</Button>
              )}
            </AffiliationLine>
          ))}
      </AffiliationsWrapper>

      <Wrapper>
        <StyledTextField
          data-test-id="author-email"
          error={get(errors, emailName)}
          hideErrorMessage
          label="Email address"
          name={emailName}
          placeholder="Please type in the email"
          required
          {...autoCompleteProps}
          value={get(values, emailName)}
        />

        <StyledTextField
          data-test-id="author-orcid"
          error={get(errors, orcidName)}
          hideErrorMessage
          label="ORCID iD"
          name={orcidName}
          placeholder="Optional"
          {...autoCompleteProps}
          value={get(values, orcidName)}
        />
      </Wrapper>

      <Wrapper>
        <Checkbox
          checkBoxText={correspondingAuthorText}
          checked={get(values, isCorrespondingName)}
          data-test-id={`author-input-${isCorrespondingName}`}
          name={isCorrespondingName}
          onChange={handleChange}
          value={get(values, isCorrespondingName)}
          {...autoCompleteProps}
        />

        <Checkbox
          checkBoxText={submittingAuthorText}
          checked={get(values, isSubmittingName)}
          data-test-id={`author-input-${isSubmittingName}`}
          name={isSubmittingName}
          onChange={handleChange}
          value={get(values, isSubmittingName)}
          {...autoCompleteProps}
        />

        <Checkbox
          checkBoxText="Equal contribution"
          checked={get(values, equalContributionName)}
          data-test-id={`author-input-${equalContributionName}`}
          name={equalContributionName}
          onChange={handleChange}
          value={get(values, equalContributionName)}
          {...autoCompleteProps}
        />
      </Wrapper>
    </div>
  )
}

export default AuthorInput

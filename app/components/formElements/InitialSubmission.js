/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { get } from 'lodash'
import { th } from '@pubsweet/ui-toolkit'

import { getWBLaboratory, getWBPerson } from '../../fetch/WBApi'

import { onAutocompleteChange, onSuggestionSelected } from './helpers'

import AutoComplete from './AutoComplete'
import Checkbox from './Checkbox'
import Image from './Image'
import References from './References'
import TextEditor from './TextEditor'
import TextField from './TextField'
import TextFieldGroup from './TextFieldGroup'

const Info = styled.div`
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  margin-bottom: ${th('gridUnit')};
  width: 600px;
`

const disclaimerDescription = (
  <React.Fragment>
    <p>
      I/we declare to the best of my/our knowledge that the experiment is
      reproducible; that the submission has been approved by all authors; that
      the submission has been approved by the laboratory&#39;s Principal
      Investigator, and that the results have not been published elsewhere. The
      author(s) declare no conflict of interest.
    </p>
  </React.Fragment>
)

const InitialSubmission = props => {
  const {
    errors,
    handleBlur,
    handleChange,
    setFieldTouched,
    setFieldValue,
    setErrors,
    touched,
    values,
    upload,
  } = props

  return (
    <>
      <Info>
        Enter author names in the order they will appear in the article. If you
        have a middle initial, enter it after your fist name.
      </Info>

      <Info>
        Note: Some fields query WormBase for autocomplete. Researchers
        submitting non-C. elegans manuscripts should disregard the autocomplete
        for those fields.
      </Info>

      <TextFieldGroup
        authors
        data={getWBPerson}
        data-test-id="authors"
        errors={errors}
        handleBlur={handleBlur}
        handleChange={handleChange}
        label="Authors"
        name="authors"
        placeholder="Please type the author's name"
        required
        setFieldTouched={setFieldTouched}
        setFieldValue={setFieldValue}
        touched={touched}
        values={values}
      />

      <TextEditor
        data-test-id="title"
        error={get(errors, 'title')}
        italic
        key={`title-${props.readOnly}`}
        label="Title"
        name="title"
        placeholder="Please enter the title of your manuscript"
        required
        setFieldTouched={setFieldTouched}
        setFieldValue={setFieldValue}
        subscript
        superscript
        touched={touched}
        value={get(values, 'title')}
      />

      <TextEditor
        data-test-id="abstract"
        error={get(errors, 'abstract')}
        italic
        key={`abstract-${props.readOnly}`}
        label="Abstract"
        name="abstract"
        placeholder="Please enter an abstract with a suggested length of 100 words"
        required
        setFieldTouched={setFieldTouched}
        setFieldValue={setFieldValue}
        subscript
        superscript
        touched={touched}
        underline
        value={get(values, 'abstract')}
      />

      <Image
        data-test-id="image"
        errors={errors}
        label="Image"
        name="image"
        required
        setFieldTouched={setFieldTouched}
        setFieldValue={setFieldValue}
        touched={touched}
        upload={upload}
        values={values}
      />

      <TextEditor
        bold
        data-test-id="image-title"
        error={get(errors, 'imageTitle')}
        italic
        key={`image-title-${props.readOnly}`}
        label="Image title"
        name="imageTitle"
        placeholder="The 'Figure 1' label will be added automatically"
        setFieldTouched={setFieldTouched}
        setFieldValue={setFieldValue}
        subscript
        superscript
        touched={touched}
        value={get(values, 'imageTitle')}
      />

      <TextEditor
        bold
        data-test-id="image-caption"
        error={get(props.errors, 'imageCaption')}
        italic
        key={`image-caption-${props.readOnly}`}
        label="Image caption"
        name="imageCaption"
        placeholder="Image caption"
        required
        setFieldTouched={setFieldTouched}
        setFieldValue={setFieldValue}
        subscript
        superscript
        touched={touched}
        underline
        value={get(values, 'imageCaption')}
      />

      <TextEditor
        bold
        createtable
        data-test-id="pattern-description"
        error={get(props.errors, 'patternDescription')}
        italic
        key={`pattern-description-${props.readOnly}`}
        label="Main article text"
        link
        name="patternDescription"
        placeholder="Enter article text here"
        required
        setFieldTouched={setFieldTouched}
        setFieldValue={setFieldValue}
        subscript
        superscript
        table
        touched={touched}
        underline
        value={get(values, 'patternDescription')}
      />

      <TextEditor
        bold
        createtable
        data-test-id="methods"
        error={get(props.errors, 'methods')}
        italic
        key={`methods-${props.readOnly}`}
        label="Methods"
        link
        name="methods"
        placeholder="Describe the methods (optional)"
        setFieldTouched={setFieldTouched}
        setFieldValue={setFieldValue}
        subscript
        superscript
        table
        touched={touched}
        underline
        value={get(values, 'methods')}
      />

      <TextEditor
        bold
        createtable
        data-test-id="reagents"
        error={get(props.errors, 'reagents')}
        italic
        key={`reagents-${props.readOnly}`}
        label="Reagents"
        link
        name="reagents"
        placeholder="Provide the reagents used (optional)"
        setFieldTouched={setFieldTouched}
        setFieldValue={setFieldValue}
        subscript
        superscript
        table
        touched={touched}
        underline
        value={get(values, 'reagents')}
      />

      <References
        errors={errors}
        handleBlur={handleBlur}
        handleChange={handleChange}
        references={get(values, 'references')}
        setErrors={setErrors}
        setFieldTouched={setFieldTouched}
        setFieldValue={setFieldValue}
        touched={touched}
      />

      <TextField
        data-test-id="acknowledgements"
        error={get(errors, 'acknowledgements')}
        handleBlur={handleBlur}
        handleChange={handleChange}
        label="Acknowledgements"
        name="acknowledgements"
        placeholder="Enter acknowledgements (optional)"
        touched={touched}
        value={get(values, 'acknowledgements')}
      />

      <AutoComplete
        data-test-id="laboratory"
        error={get(errors, 'laboratory.name')}
        fetchData={getWBLaboratory}
        handleBlur={handleBlur}
        label="Laboratory"
        name="laboratory.name"
        onChange={e =>
          onAutocompleteChange(
            e,
            'laboratory.name',
            setFieldValue,
            handleChange,
          )
        }
        onSuggestionSelected={onSuggestionSelected}
        placeholder="Enter your PI’s registered WormBase Laboratory (optional)"
        setFieldValue={setFieldValue}
        value={get(values, 'laboratory.name')}
      />

      <TextField
        data-test-id="funding"
        error={get(errors, 'funding')}
        handleBlur={handleBlur}
        handleChange={handleChange}
        label="Funding"
        name="funding"
        placeholder="Please state the sources of your funding"
        required
        touched={touched}
        value={get(values, 'funding')}
      />

      <TextField
        data-test-id="suggested-reviewer"
        error={get(errors, 'suggestedReviewer.name')}
        handleBlur={handleBlur}
        handleChange={handleChange}
        label="Suggested Reviewers"
        name="suggestedReviewer.name"
        placeholder="Please suggest reviewers, including email or ORCiD if possible"
        required
        setFieldValue={setFieldValue}
        touched={touched}
        value={get(values, 'suggestedReviewer.name')}
      />

      <Checkbox
        checkBoxText="I agree"
        checked={get(values, 'disclaimer')}
        data-test-id="disclaimer"
        description={disclaimerDescription}
        errors={errors}
        label="Disclaimer"
        name="disclaimer"
        // onBlur
        onChange={handleChange}
        required
        setFieldTouched={setFieldTouched}
        touched={touched}
        value={get(values, 'disclaimer')}
      />

      <Info>
        If you are planning to submit similar articles within a week or if this
        microPublication is part of an integration set, please notify the editor
        with a comment in the box below. This will help us speed the review
        process.
      </Info>

      <TextEditor
        data-test-id="comments"
        error={get(props.errors, 'comments')}
        key={`comments-${props.readOnly}`}
        label="Comments to the editor"
        name="comments"
        placeholder="Enter comments (optional)"
        setFieldTouched={setFieldTouched}
        setFieldValue={setFieldValue}
        touched={touched}
        value={get(values, 'comments')}
      />
    </>
  )
}

export default InitialSubmission

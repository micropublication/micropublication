/* eslint-disable react/prop-types */

import React, { useState } from 'react'
import styled from 'styled-components'
import { get } from 'lodash'
import { v4 as uuid } from 'uuid'

import { th } from '@pubsweet/ui-toolkit'
import { Icon } from '@pubsweet/ui'

import TextEditor from './TextEditor'
import TextField from './TextField'
import Button from '../../../ui/src/common/Button'

import { getReference } from '../../fetch/PubMedApi'

const Error = styled.span`
  color: ${th('colorError')};
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  padding-left: ${th('gridUnit')};
`

const ReferencesWrapper = styled.div`
  margin-bottom: ${th('gridUnit')};
  width: 620px;
`

const ReferenceField = styled(TextField)`
  width: calc(${th('gridUnit')} * 35);
`

const FieldWrapper = styled.div`
  display: flex;
  flex-direction: row;
`

const IconButton = styled(Button)`
  min-width: calc(${th('gridUnit')} * 6);
  svg {
    stroke: ${th('colorFurniture')};
  }
`

const ReferenceWrapper = styled.div`
  border-bottom: 1px solid ${th('colorPrimary')};
  margin-bottom: ${th('gridUnit')};
`

const Info = styled.div`
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  margin-bottom: ${th('gridUnit')};
  width: 600px;
`

const Reference = props => {
  const {
    errors,
    handleBlur,
    handleChange,
    handleRemoveReference,
    name,
    fetchReference,
    readOnly,
    reference,
    setFieldTouched,
    setFieldValue,
    touched,
  } = props

  const [loadingPubMed, setLoadingPubMed] = useState(false)

  const pubMedLookup = async event => {
    const trimmedValue = event.clipboardData
      ? event.clipboardData.getData('Text')
      : reference.pubmedId.trim()

    if (!/^\d+$/.test(trimmedValue)) {
      return
    }

    setLoadingPubMed(true)
    await fetchReference(trimmedValue, name)
    setLoadingPubMed(false)
  }

  return (
    <ReferenceWrapper>
      <Error>{errors}</Error>
      <FieldWrapper>
        <ReferenceField
          handleBlur={handleBlur}
          handleChange={handleChange}
          label="PubMed ID"
          name={`${name}.pubmedId`}
          onPaste={pubMedLookup}
          placeholder={`PubMed IDs are required unless they don't exist`}
          touched={touched}
          value={reference.pubmedId}
        />

        <IconButton
          loading={loadingPubMed}
          onClick={pubMedLookup}
          title="PubMed lookup"
        >
          <Icon>search</Icon>
        </IconButton>

        <ReferenceField
          handleBlur={handleBlur}
          handleChange={handleChange}
          label="DOI"
          name={`${name}.doi`}
          placeholder="Recommended if PubMed ID is not available"
          setFieldTouched={setFieldTouched}
          setFieldValue={setFieldValue}
          touched={touched}
          value={reference.doi}
        />

        {!readOnly && (
          <IconButton
            onClick={() => handleRemoveReference(reference.id)}
            title="Remove reference"
            value={reference.id}
          >
            <Icon>minus_circle</Icon>
          </IconButton>
        )}
      </FieldWrapper>

      <TextEditor
        bold
        data-test-id="references"
        error={get(errors, 'references')}
        italic
        name={`${name}.reference`}
        placeholder="Reference text if PubMed lookup is not available (only enter one per text box)."
        required
        setFieldTouched={setFieldTouched}
        setFieldValue={setFieldValue}
        subscript
        superscript
        touched={touched}
        value={reference.reference}
      />
    </ReferenceWrapper>
  )
}

const References = props => {
  const {
    errors,
    handleBlur,
    handleChange,
    readOnly,
    references,
    setFieldValue,
    setFieldTouched,
    setErrors,
    touched,
  } = props

  const handleAddReference = () => {
    references.push({ doi: '', id: uuid(), pubmedId: '', reference: '' })
    setFieldValue('references', references)
  }

  const handleRemoveReference = async id => {
    const referencesSaved = references.filter(val => val.id !== id)
    setFieldValue('references', referencesSaved)
  }

  const fetchReference = async (value, name) =>
    getReference(value)
      .then(async response => {
        if (!response.ok) {
          throw await response.text()
        }
        return response.json()
      })
      .then(json => {
        const { reference: pubMedResult } = json
        const referenceField = `${name}.reference`
        setFieldValue(referenceField, pubMedResult)
        const referenceId = `${name}.id`
        setFieldValue(referenceId, uuid())
      })
      .catch(e => {
        const referenceIndex = name.replace(/[^\d]/g, '')
        // eslint-disable-next-line no-console
        if (!errors.references) {
          errors.references = []
        }

        errors.references[referenceIndex] = e.message || e
        setErrors(errors)
      })

  return (
    <ReferencesWrapper>
      <Info>
        References
        <p>
          References should be listed in alphabetical order. Enter the PubMed ID
          (not PMC ID) of the reference into the PubMed ID box and click the
          download button to retrieve the citation from PubMed. Please make sure
          the retrieved citation is correct.
        </p>
        <p>
          If the citation does not have a PubMed ID, enter the DOI in the DOI
          field and enter the reference as text. In this case, please format the
          reference to be consistent with all other references.
        </p>
      </Info>

      {references &&
        references.length > 0 &&
        references.map((item, i) => {
          if (!references[i].id) references[i].id = uuid()
          const itemId = references[i].id
          const error = errors.references && errors.references[i]
          return (
            <Reference
              errors={error}
              fetchReference={fetchReference}
              handleBlur={handleBlur}
              handleChange={handleChange}
              handleRemoveReference={handleRemoveReference}
              key={itemId}
              name={`references[${i}]`}
              readOnly={readOnly}
              reference={item}
              setFieldTouched={setFieldTouched}
              setFieldValue={setFieldValue}
              touchec={touched}
            />
          )
        })}

      {!readOnly && (
        <Button onClick={handleAddReference} primary>
          Add reference
        </Button>
      )}
    </ReferencesWrapper>
  )
}

export default References

/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { get, isEmpty, keys, omit, set } from 'lodash'
import { v4 as uuid } from 'uuid'

import { Button } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import { AuthorInput, AutoComplete } from './index'

import { onAutocompleteChange, onSuggestionSelected } from './helpers'

const Field = props => {
  const {
    authors,
    data,
    handleChange,
    index,
    name,
    placeholder,
    setFieldValue,
    value,
  } = props
  const passProps = omit(props, ['data-test-id', 'label'])

  if (authors) {
    return (
      <AuthorInput
        name={name}
        placeholder={placeholder}
        values={value}
        {...passProps}
      />
    )
  }

  return (
    <AutoComplete
      data-test-id={`${props['data-test-id']}-item-${index}`}
      fetchData={data}
      name={name}
      onChange={e => onAutocompleteChange(e, name, setFieldValue, handleChange)}
      onSuggestionSelected={onSuggestionSelected}
      placeholder={placeholder}
      value={value}
      {...passProps}
    />
  )
}

// TO DO -- extract Labels from TextField
const Label = styled.label`
  display: block;
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
`

const LineWrapper = styled.div`
  align-items: flex-start;
  display: flex;
`

const StyledButton = styled(Button)`
  margin-top: calc(${th('gridUnit')});
`

const GroupWrapper = styled.div`
  margin-bottom: calc(${th('gridUnit')} * 2);
`

const Error = styled.span`
  color: ${th('colorError')};
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  padding-left: ${th('gridUnit')};
`

const TextFieldGroup = props => {
  const handleAdd = () => {
    const { authors, handleChange, maxItems, name, values } = props
    let data = get(values, name)

    if (!data) data = []
    if (data.length >= maxItems) return

    const newItem = {
      email: '',
      firstName: '',
      id: uuid(),
      lastName: '',
    }

    if (authors) {
      newItem.credit = ['']
      newItem.affiliations = ['']
      newItem.submittingAuthor = false
    }

    const newValues = data.concat(newItem)
    // setValues(values, name, newValues)
    setFieldValue(name, newValues)
    handleChange(name)
  }

  const handleRemove = async id => {
    const { name, setFieldValue, values } = props
    const data = get(values, name)
    const changed = data.filter(val => val.id !== id)

    setFieldValue(name, changed)
    handleChange(name)
  }

  const {
    authors,
    handleChange,
    label,
    name,
    placeholder,
    readOnly,
    required,
    setFieldValue,
    values,
  } = props

  let data = get(values, name)
  if (!data || data.length === 0) {
    data = [{ email: '', firstName: '', lastName: '' }]
    if (authors) {
      data[0].credit = ['']
      data[0].affiliations = ['']
    }

    setFieldValue(name, data)
    handleChange(name)
    set(values, name, data)
  }

  const error = get(props.errors, name)
  const touched = get(props.touched, name)

  let err
  if (Array.isArray(error)) {
    const firstItemWithErr = error.find(e => !isEmpty(e))

    if (name === 'authors') {
      err =
        get(firstItemWithErr, 'firstName') ||
        get(firstItemWithErr, 'lastName') ||
        get(firstItemWithErr, 'credit') ||
        get(firstItemWithErr, 'affiliations') ||
        get(firstItemWithErr, 'email')
    } else {
      const firstKey = keys(firstItemWithErr) && keys(firstItemWithErr)[0]
      err = firstItemWithErr && firstItemWithErr[firstKey]
    }
  } else {
    err = error
  }

  const passProps = omit(props, ['data-test-id'])

  return (
    <GroupWrapper data-test-id={props['data-test-id']}>
      {label && (
        <Label>
          {label} {required ? ' *' : ''}{' '}
          {touched && error && <Error>{err}</Error>}
        </Label>
      )}
      {data &&
        data.length > 0 &&
        data.map((item, i) => {
          const itemName = authors ? `${name}[${i}]` : `${name}[${i}].name`
          const itemValue = authors ? data[i] : data[i].name
          if (!data[i].id) data[i].id = uuid()
          const itemId = data[i].id

          return (
            <LineWrapper key={itemName}>
              <Field
                {...passProps}
                authors={authors}
                data-test-id={`${props['data-test-id']}`}
                error={Array.isArray(error) && error[i] && error[i].name}
                handleChange={handleChange}
                index={i}
                name={itemName}
                placeholder={placeholder}
                value={itemValue}
              />
              {!readOnly && (
                <Button onClick={() => handleRemove(itemId)}>Remove</Button>
              )}
            </LineWrapper>
          )
        })}

      {(!data || data.length === 0) && (
        <Field
          authors={authors}
          data-test-id={`${props['data-test-id']}`}
          error={Array.isArray(error) && error[0] && error[0].name}
          handleChange={handleChange}
          name={authors ? `${name}[0]` : `${name}[0].name`}
          value={authors ? data[0] : data[0].name}
          {...passProps}
        />
      )}

      {!readOnly && (
        <StyledButton
          data-test-id={`${props['data-test-id']}-add-button`}
          onClick={handleAdd}
          primary
        >
          Add author
        </StyledButton>
      )}
    </GroupWrapper>
  )
}

export default TextFieldGroup

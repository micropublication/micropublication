/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { isArray, clone } from 'lodash'
import { v4 as uuid } from 'uuid'

import { Button, List } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import { DiscussForm } from '../form'
import PanelTextEditor from './PanelTextEditor'

const DiscussionWrapper = styled.div`
  margin-bottom: calc(${th('gridUnit')} * 2);
  margin-left: calc(${th('gridUnit')} * 3);
`

const Editor = styled(PanelTextEditor)`
  box-sizing: border-box;
`

const transformEntries = entries =>
  entries.map(entry => ({
    id: entry.id || uuid(),
    label: entry.user.displayName,
    readOnly: true,
    value: entry.content,
  }))

/*
  HACK
  In order for the editor content to be reset, we need to destroy the xpub-edit
  instance and create a new one. We only need to reset the value on submit. So
  this is a counter that is incremented on every submit.
*/
let key = 0

const NewEntry = props => (
  <DiscussForm {...props}>
    {formProps => {
      const { handleSubmit, isValid, setStatus, status, values } = formProps

      // See note on key above
      if (status && status.hasReset) {
        key += 1
        setStatus({ hasReset: false })
      }

      return (
        <React.Fragment>
          <Editor
            key={key}
            name="content"
            placeholder="Make a comment"
            value={values.content}
            {...formProps}
          />

          <Button
            disabled={!isValid}
            onClick={handleSubmit}
            primary
            type="submit"
          >
            Send
          </Button>
        </React.Fragment>
      )
    }}
  </DiscussForm>
)

const Discuss = props => {
  const { data } = props

  let entries = clone(data)
  const hasEntries = isArray(entries) && entries.length > 0
  if (hasEntries) entries = transformEntries(entries)

  return (
    <DiscussionWrapper>
      {hasEntries && <List component={Editor} items={entries} />}
      <NewEntry {...props} />
    </DiscussionWrapper>
  )
}

export default Discuss

const axios = require('axios')
const config = require('config')
const cheerio = require('cheerio')

const { Manuscript, ManuscriptVersion } = require('@pubsweet/models')

const baseUrl = config.get('datacite.url')
const username = config.get('datacite.username')
const password = config.get('datacite.password')

const auth = {
  username,
  password,
}

const headers = {
  'content-type': 'application/vnd.api+json',
}

const makeFetchRequest = async (req, res) => {
  await axios({
    url: `${baseUrl}/${req.query.doi}`,
    method: 'get',
    headers,
    auth,
  })
    .then(response => res.send(response.data))
    .catch(error => {
      const { status, title: message } = error
      return res.status(status).send(message)
    })
}

const makeNewRequest = async (req, res) => {
  const { doi } = req.query

  const data = {
    data: {
      type: 'dois',
      attributes: {
        doi,
      },
    },
  }

  await axios({
    url: baseUrl,
    method: 'post',
    headers,
    data,
    auth,
  })
    .then(response => res.send(response.data))
    .catch(error => {
      const { status, data: errorMessages } = error.response
      return res.status(status).send(errorMessages.errors)
    })
}

const makeFetchLastRequest = async (req, res) => {
  await axios({
    url: `${baseUrl}/?client-id=caltech.micropub&page[size]=1&sort=-created`,
    method: 'get',
    headers,
    auth,
  }).then(response => res.send(response.data))
}

const makePublishRequest = async (req, res) => {
  const { id: manuscriptId } = req.query

  const version = await ManuscriptVersion.query()
    .where({ manuscriptId })
    .orderBy('created', 'desc')
    .first()

  const manuscript = await Manuscript.query().findById(manuscriptId)

  const { title, patternDescription, updated, authors } = version
  const { doi } = manuscript

  const creators = authors.map(author => {
    const { firstName, lastName } = author
    const creator = {
      givenName: firstName,
      familyName: lastName,
    }
    return creator
  })

  const updatedDate = new Date(Number(updated))

  // Grab the first paragraph for the abstract
  const $ = cheerio.load(patternDescription)
  const abstract = $('p')
    .first()
    .text()

  // Strip the HTML
  const strippedTitle = title.replace(/(<([^>]+)>)/gi, '')

  const baseDoi = doi.split('/')[1].replace(/\./g, '-')
  const url = `https://www.micropublication.org/journals/biology/${baseDoi}`

  const data = {
    data: {
      attributes: {
        event: 'publish',
        titles: { title: strippedTitle },
        publicationYear: updatedDate.getFullYear(),
        descriptions: { description: abstract, descriptionType: 'Abstract' },
        creators,
        types: { resourceTypeGeneral: 'DataPaper' },
        publisher: 'microPublication Biology',
        url,
      },
    },
  }

  await axios({
    url: `${baseUrl}/${doi}`,
    method: 'put',
    headers,
    data,
    auth,
  })
    .then(response => res.send(response.data))
    .catch(error => {
      const { status, data: errorMessages } = error.response
      return res.status(status).send(errorMessages.errors)
    })
}

const DataCiteApi = app => {
  /**
   * GET ENDPOINTS
   */
  app.get('/api/datacite/new', async (req, res) => {
    await makeNewRequest(req, res)
  })

  app.get('/api/datacite/fetch', async (req, res) => {
    await makeFetchRequest(req, res)
  })

  app.get('/api/datacite/fetchLast', async (req, res) => {
    await makeFetchLastRequest(req, res)
  })

  app.get('/api/datacite/publish', async (req, res) => {
    await makePublishRequest(req, res)
  })
}

module.exports = DataCiteApi

import config from 'config'

const { baseUrl } = config['pubsweet-client']
const apiUrl = `${baseUrl}/api/export`

const exportManuscriptToHTML = articleId => {
  window.location.assign(`${apiUrl}/${articleId}/html`)
}

const exportManuscriptToPrint = versionId => {
  window.open(`${apiUrl}/${versionId}/print`)
}

export { exportManuscriptToHTML, exportManuscriptToPrint }

import gql from 'graphql-tag'

const previewBaseFragment = gql`
  fragment PreviewBase on ManuscriptVersion {
    id
    created
    active
    submitted
    abstract
    acknowledgements
    authors {
      affiliations
      credit
      email
      firstName
      lastName
      submittingAuthor
      correspondingAuthor
      equalContribution
      WBId
      orcid
    }
    decision
    disclaimer
    funding
    image {
      name
      url
    }
    imageCaption
    imageTitle
    laboratory {
      name
      WBId
    }
    methods
    reagents
    patternDescription
    references {
      reference
      pubmedId
      doi
    }
    title

    # TO DO - SHOULD GENE EXPRESSION FORM DATA BE AVAILABLE TO REVIEWERS?
  }
`

const RESTRICTED_PREVIEW = gql`
  ${previewBaseFragment}

  query RestricedPreview($id: ID!) {
    manuscript(id: $id) {
      id
      versions {
        ...PreviewBase
      }
    }
  }
`

const REVIEWER_PREVIEW = gql`
  ${previewBaseFragment}

  query RestricedPreview($id: ID!) {
    manuscript(id: $id) {
      id
      versions(invitedToReviewOnly: true) {
        ...PreviewBase
      }
    }
  }
`

const FULL_PREVIEW = gql`
  ${previewBaseFragment}

  query FullPreview($id: ID!) {
    manuscript(id: $id) {
      id
      isDataTypeSelected
      isInitiallySubmitted
      chatThreads(type: author) {
        id
        chatType
        messages {
          id
          content
          timestamp
          user {
            displayName
          }
        }
      }
      versions {
        ...PreviewBase
        comments
        suggestedReviewer {
          name
          WBId
        }
      }
    }
  }
`

// full preview without the author chat
const SCIENCE_OFFICER_PREVIEW = gql`
  ${previewBaseFragment}

  query FullPreview($id: ID!) {
    manuscript(id: $id) {
      id
      isDataTypeSelected
      isInitiallySubmitted
      versions {
        ...PreviewBase
        comments
        suggestedReviewer {
          name
          WBId
        }
      }
    }
  }
`

const CURATOR_PREVIEW = SCIENCE_OFFICER_PREVIEW

const MANUSCRIPT_STATUS = gql`
  query ManuscriptStatus($id: ID!) {
    manuscript(id: $id) {
      id
      isInitiallySubmitted
      versions {
        id
        decision
        submitted
      }
    }
  }
`

const MANUSCRIPT_SUBMISSION_FORM = gql`
  query manuscript($id: ID!) {
    manuscript(id: $id) {
      id
      dataType
      versions {
        id
        decisionLetter
        submitted
        abstract
        acknowledgements
        authors {
          affiliations
          credit
          email
          firstName
          lastName
          submittingAuthor
          correspondingAuthor
          equalContribution
          WBId
          orcid
        }
        comments
        disclaimer
        funding
        image {
          name
          url
        }
        imageCaption
        imageTitle
        laboratory {
          name
          WBId
        }
        methods
        reagents
        patternDescription
        references {
          reference
          pubmedId
          doi
        }
        suggestedReviewer {
          name
          WBId
        }
        title
        geneExpression {
          antibodyUsed
          backboneVector {
            name
            WBId
          }
          coinjected
          constructComments
          constructionDetails
          detectionMethod
          dnaSequence {
            name
            WBId
          }
          expressionPattern {
            name
            WBId
          }
          fusionType {
            name
            WBId
          }
          genotype
          injectionConcentration
          inSituDetails
          integratedBy {
            name
            WBId
          }
          observeExpression {
            certainly {
              certainly {
                name
                WBId
              }
              during {
                name
                WBId
              }
              id
              subcellularLocalization {
                name
                WBId
              }
            }
            partially {
              partially {
                name
                WBId
              }
              during {
                name
                WBId
              }
              id
              subcellularLocalization {
                name
                WBId
              }
            }
            possibly {
              possibly {
                name
                WBId
              }
              during {
                name
                WBId
              }
              id
              subcellularLocalization {
                name
                WBId
              }
            }
            not {
              not {
                name
                WBId
              }
              during {
                name
                WBId
              }
              id
              subcellularLocalization {
                name
                WBId
              }
            }
          }
          reporter {
            name
            WBId
          }
          species {
            name
            WBId
          }
          strain
          transgeneName
          transgeneUsed {
            name
            WBId
          }
          utr {
            name
            WBId
          }
          variation {
            name
            WBId
          }
        }
      }
    }
  }
`

const EDITOR_PANEL = gql`
  query EditorPanel($id: ID!) {
    getGlobalTeams {
      id
      name
      role
      members {
        id
        user {
          id
          displayName
        }
      }
    }

    manuscript(id: $id) {
      chatThreads {
        id
        chatType
        messages {
          content
          timestamp
          user {
            displayName
          }
        }
        userId
      }
      # currentlyWith
      categories
      dataType
      dbReferenceId
      doi
      history {
        received
        sentForReview
        reviewReceived
        revisionReceived
        accepted
        published
      }
      id
      isDataTypeSelected
      isInitiallySubmitted
      pmId
      pmcId
      species
      submissionTypes
      teams {
        id
        role
        members {
          id
          user {
            id
            displayName
          }
        }
      }
      versions {
        id
        title
        created
        active
        isApprovedByScienceOfficer
        submitted
        authors {
          email
          firstName
          lastName
          submittingAuthor
        }
        curatorReviews {
          id
          content
          curator {
            id
            displayName
          }
          recommendation
          openAcknowledgement
          submitted
        }
        decision
        decisionLetter
        reviews {
          id
          content
          confidentialComments
          openAcknowledgement
          recommendation
          reviewer {
            id
            displayName
          }
          reviseQualifier
          askedToSeeRevision
          status {
            pending
            submitted
          }
        }
        teams {
          id
          role
          members {
            id
            status
            user {
              id
              displayName
            }
          }
        }
      }
    }
  }
`

const SCIENCE_OFFICER_PANEL = gql`
  query EditorPanel($id: ID!) {
    manuscript(id: $id) {
      chatThreads(type: scienceOfficer) {
        id
        chatType
        messages {
          content
          timestamp
          user {
            displayName
          }
        }
      }
      # currentlyWith
      dataType
      # doi
      id
      isDataTypeSelected
      isInitiallySubmitted
      teams {
        id
        role
        members {
          id
          user {
            id
            displayName
          }
        }
      }
      versions {
        id
        created
        active
        # isApprovedByScienceOfficer
        submitted
        authors {
          email
          firstName
          lastName
          submittingAuthor
        }
        decision
        decisionLetter
        reviews {
          id
          content
          confidentialComments
          openAcknowledgement
          recommendation
          reviewer {
            id
            displayName
          }
          reviseQualifier
          askedToSeeRevision
          status {
            pending
            submitted
          }
        }
        teams {
          id
          role
          members {
            id
            status
            user {
              id
              displayName
            }
          }
        }
      }
    }
  }
`

const REVIEWER_PANEL = gql`
  query ReviewerPanel($id: ID!) {
    manuscript(id: $id) {
      id
      chatThreads(currentUserOnly: true, type: reviewer) {
        id
        messages {
          id
          content
          timestamp
          user {
            id
            displayName
          }
        }
      }
      versions(invitedToReviewOnly: true) {
        id
        created
        decision
        reviews(currentUserOnly: true) {
          id
          confidentialComments
          content
          recommendation
          reviseQualifier
          openAcknowledgement
          askedToSeeRevision
          status {
            pending
            submitted
          }
        }
      }
    }
  }
`

const CURATOR_PANEL = gql`
  query ReviewerPanel($id: ID!) {
    manuscript(id: $id) {
      id
      chatThreads(currentUserOnly: true, type: curator) {
        id
        messages {
          id
          content
          timestamp
          user {
            id
            displayName
          }
        }
      }
      dbReferenceId
      doi
      versions {
        id
        created
        decision
        submitted
        curatorReviews(currentUserOnly: true) {
          id
          content
          recommendation
          openAcknowledgement
          submitted
        }
      }
    }
  }
`

const REINVITE_REVIEWER = gql`
  mutation ReinviteReviewer($input: ReinviteReviewerInput!) {
    reinviteReviewer(input: $input)
  }
`

const SAVE_CURATOR_REVIEW = gql`
  mutation SaveCuratorReview($id: ID!, $input: CuratorReviewInput!) {
    saveCuratorReview(id: $id, input: $input) {
      id
      content
      recommendation
      openAcknowledgement
    }
  }
`

const SUBMIT_CURATOR_REVIEW = gql`
  mutation SubmitCuratorReview($id: ID!, $input: CuratorReviewInput!) {
    submitCuratorReview(id: $id, input: $input) {
      id
      content
      recommendation
      openAcknowledgement
      submitted
    }
  }
`

const SAVE_FORM = gql`
  mutation saveSubmissionForm($input: SubmissionFormInput!) {
    saveSubmissionForm(input: $input)
  }
`

const SAVE_REVIEW = gql`
  mutation SaveReview($reviewId: ID!, $input: SubmitReviewInput!) {
    saveReview(reviewId: $reviewId, input: $input)
  }
`

const SEND_CHAT = gql`
  mutation SendChatMessage($input: SendChatMessageInput!) {
    sendChatMessage(input: $input)
  }
`

const SET_DATA_TYPE = gql`
  mutation SetDataType($manuscriptId: ID!, $input: SetDataTypeInput!) {
    setDataType(manuscriptId: $manuscriptId, input: $input)
  }
`

const SUBMIT_DECISION = gql`
  mutation SubmitDecision(
    $manuscriptVersionId: ID!
    $input: SubmitDecisionInput!
  ) {
    submitDecision(manuscriptVersionId: $manuscriptVersionId, input: $input)
  }
`

const SUBMIT_MANUSCRIPT = gql`
  mutation SubmitManuscript($input: SubmissionFormInput!) {
    submitManuscript(input: $input)
  }
`

const SUBMIT_REVIEW = gql`
  mutation SubmitReview($reviewId: ID!, $input: SubmitReviewInput!) {
    submitReview(reviewId: $reviewId, input: $input)
  }
`

const UPDATE_MANUSCRIPT_METADATA = gql`
  mutation UpdateManuscriptMetadata($manuscriptId: ID!, $data: MetadataInput!) {
    updateManuscriptMetadata(manuscriptId: $manuscriptId, data: $data)
  }
`

const UPDATE_MANUSCRIPT_TEAMS = gql`
  mutation UpdateManuscriptTeams($input: UpdateTeamMembershipInput!) {
    updateManuscriptTeamMembership(input: $input)
  }
`

const UPLOAD_FILE = gql`
  mutation UploadFile($file: Upload!) {
    upload(file: $file) {
      url
    }
  }
`

export {
  CURATOR_PANEL,
  CURATOR_PREVIEW,
  EDITOR_PANEL,
  MANUSCRIPT_SUBMISSION_FORM,
  MANUSCRIPT_STATUS,
  RESTRICTED_PREVIEW,
  REVIEWER_PANEL,
  REVIEWER_PREVIEW,
  FULL_PREVIEW,
  REINVITE_REVIEWER,
  SAVE_CURATOR_REVIEW,
  SAVE_FORM,
  SAVE_REVIEW,
  SCIENCE_OFFICER_PANEL,
  SCIENCE_OFFICER_PREVIEW,
  SEND_CHAT,
  SET_DATA_TYPE,
  SUBMIT_CURATOR_REVIEW,
  SUBMIT_DECISION,
  SUBMIT_MANUSCRIPT,
  SUBMIT_REVIEW,
  UPDATE_MANUSCRIPT_METADATA,
  UPDATE_MANUSCRIPT_TEAMS,
  UPLOAD_FILE,
}

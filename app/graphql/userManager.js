import gql from 'graphql-tag'

const USER_MANAGER_DATA = gql`
  query UserManagerData {
    users {
      admin
      displayName
      email
      givenNames
      id
      orcid
      surname
      username
    }
  }
`

const UPDATE_PERSONAL_INFORMATION = gql`
  mutation UpdatePersonalInformation($input: UpdatePersonalInformationInput!) {
    updatePersonalInformation(input: $input) {
      id
    }
  }
`

const UPDATE_USERNAME = gql`
  mutation UpdateUsername($input: UpdateUsernameInput!) {
    updateUsername(input: $input) {
      id
    }
  }
`

// eslint-disable-next-line import/prefer-default-export
export { USER_MANAGER_DATA, UPDATE_PERSONAL_INFORMATION, UPDATE_USERNAME }

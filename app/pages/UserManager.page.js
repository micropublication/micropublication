import React from 'react'
import { useMutation, useQuery } from '@apollo/react-hooks'
import { clone } from 'lodash'

import {
  USER_MANAGER_DATA,
  UPDATE_USERNAME,
  UPDATE_PERSONAL_INFORMATION,
} from '../graphql'
import { UserManager } from '../../ui'

const UserManagerPage = () => {
  const { data, loading } = useQuery(USER_MANAGER_DATA)

  const queryRefetch = {
    refetchQueries: [
      {
        query: USER_MANAGER_DATA,
      },
    ],
  }

  const [updateUsername] = useMutation(UPDATE_USERNAME, queryRefetch)

  const [updatePersonalInformation] = useMutation(
    UPDATE_PERSONAL_INFORMATION,
    queryRefetch,
  )

  const updateUserData = input => {
    const { userId, username, givenNames, surname, orcid } = input
    updateUsername({ variables: { input: { userId, username } } })
    return updatePersonalInformation({
      variables: { input: { givenNames, surname, orcid, userId } },
    })
  }

  /**
   * Prepare data for use by the UI
   */
  const users =
    data &&
    data.users.map(user => {
      const userWithFunctions = clone(user)
      userWithFunctions.updateUserData = updateUserData
      return userWithFunctions
    })

  return <UserManager loading={loading} users={users} />
}

export default UserManagerPage

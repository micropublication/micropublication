import { get } from 'lodash'

const getInvitedReviewersTeam = versionTeams => {
  const reviewerTeam = versionTeams.find(t => t.role === 'reviewer')
  const invitedReviewers = reviewerTeam.members.filter(member =>
    ['invited', 'acceptedInvitation', 'rejectedInvitation'].includes(
      member.status,
    ),
  )

  return invitedReviewers
}

const getManuscriptTeam = manuscriptTeams => {
  const manuscriptEditors = membersOfTeam(manuscriptTeams, 'editor')
  const manuscriptEditor = manuscriptEditors && manuscriptEditors[0]

  const manuscriptSectionEditors = membersOfTeam(
    manuscriptTeams,
    'sectionEditor',
  )
  const manuscriptSectionEditor =
    manuscriptSectionEditors && manuscriptSectionEditors[0]

  const manuscriptSOs = membersOfTeam(manuscriptTeams, 'scienceOfficer')
  const manuscriptSO = manuscriptSOs && manuscriptSOs[0]

  const manuscriptCurators = membersOfTeam(manuscriptTeams, 'curator')
  const manuscriptCurator = manuscriptCurators

  return {
    curator: manuscriptCurator,
    editor: manuscriptEditor,
    sectionEditor: manuscriptSectionEditor,
    scienceOfficer: manuscriptSO,
  }
}

const getReviewerCounts = versionTeams => {
  const reviewerTeam = getReviewerTeam(versionTeams)
  const invitedReviewers = getInvitedReviewersTeam(versionTeams)
  const acceptedReviewers = reviewerTeam.members.filter(
    member => member.status === 'acceptedInvitation',
  )
  const rejectedReviewers = reviewerTeam.members.filter(
    member => member.status === 'rejectedInvitation',
  )

  return {
    invited: invitedReviewers.length,
    accepted: acceptedReviewers.length,
    rejected: rejectedReviewers.length,
  }
}

const getReviewerTeam = versionTeams =>
  versionTeams.find(t => t.role === 'reviewer')

const getSubmittingAuthor = authors => {
  const submittingAuthor = authors.find(author => author.submittingAuthor)

  return {
    authorEmail: submittingAuthor.email,
    authorName: `${submittingAuthor.firstName} ${submittingAuthor.lastName}`,
  }
}

const membersOfTeam = (teams, role) => {
  if (!teams || !role) return null

  const team = teams.find(t => t.role === role)
  if (!team) return null

  return team.members.map(member => ({
    id: member.user.id,
    displayName: member.user.displayName,
  }))
}

const transformChatMessages = messages =>
  messages &&
  messages.map(message => {
    const { content, id, timestamp, user } = message
    const chatTime = new Date(Number(timestamp))

    return {
      id,
      content,
      displayName: `${user.displayName} ( ${chatTime.toLocaleString()} )`,
    }
  })

const transformCuratorReviews = reviews =>
  reviews.map(review => ({
    content: review.content,
    curatorId: get(review, 'curator.id'),
    curatorName: get(review, 'curator.displayName'),
    openAcknowledgement: review.openAcknowledgement,
    pending: !review.submitted,
    recommendation: review.recommendation,
    reviewerId: get(review, 'curator.id'),
    reviewerName: get(review, 'curator.displayName'),
    showRequestToSeeRevision: false,
    submitted: review.submitted,
  }))

const transformReviews = reviews =>
  reviews.map(review => ({
    askedToSeeRevision: review.askedToSeeRevision,
    confidentialComments: review.confidentialComments,
    content: review.content,
    openAcknowledgement: review.openAcknowledgement,
    pending: review.status.pending,
    recommendation: review.recommendation,
    reviewerId: review.reviewer.id,
    reviewerName: review.reviewer.displayName,
    reviseQualifier: review.reviseQualifier,
  }))

const getFromStorage = item => localStorage.getItem(item)

const saveToStorage = (item, key) => {
  if (!item || item === '<p></p>') {
    localStorage.removeItem(key)
  } else {
    localStorage.setItem(key, item)
  }
}

export {
  getFromStorage,
  getInvitedReviewersTeam,
  getManuscriptTeam,
  getReviewerCounts,
  getReviewerTeam,
  getSubmittingAuthor,
  membersOfTeam,
  saveToStorage,
  transformChatMessages,
  transformCuratorReviews,
  transformReviews,
}

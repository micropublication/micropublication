import {
  getInvitedReviewersTeam,
  getManuscriptTeam,
  getReviewerCounts,
  getSubmittingAuthor,
  membersOfTeam,
  transformChatMessages,
  transformCuratorReviews,
  transformReviews,
} from './common'

/**
 * HELPER FUNCTIONS
 */
const authorIdsOfAllVersions = versions => {
  if (!versions) return null

  const authorIds = []

  versions.forEach(version => {
    const authorTeam = version.teams.find(t => t.role === 'author')
    authorTeam.members.forEach(member => {
      if (!authorIds.includes(member.user.id)) authorIds.push(member.user.id)
    })
  })

  return authorIds
}

const disableAuthors = (userList, authorIds) =>
  userList.map(user => ({
    isDisabled: authorIds.includes(user.id),
    ...user,
  }))

/**
 * END HELPER FUNCTIONS
 */

export default (data, mutations) => {
  const { manuscript, getGlobalTeams: globalTeams } = data
  const {
    categories,
    chatThreads,
    dataType,
    dbReferenceId,
    doi,
    history,
    isInitiallySubmitted,
    isDataTypeSelected,
    pmId,
    pmcId,
    species,
    submissionTypes,
    versions: dataVersions,
  } = manuscript

  const {
    reinviteReviewerMutation,
    sendChatMutation,
    setDataTypeMutation,
    submitDecisionMutation,
    updateManuscriptTeamsMutation,
    updateMetadataMutation,
  } = mutations

  /**
   * Build global team member lists
   */

  const globalEditors = membersOfTeam(globalTeams, 'editors')
  const globalScienceOfficers = membersOfTeam(globalTeams, 'scienceOfficers')
  const globalCurators = membersOfTeam(globalTeams, 'globalCurator')
  const globalSectionEditors = membersOfTeam(globalTeams, 'globalSectionEditor')

  // pass through all members to disable authors
  // (eg. cannot be the editor of the manuscript you wrote)
  const authorIds = authorIdsOfAllVersions(dataVersions)

  const scienceOfficers = disableAuthors(globalScienceOfficers, authorIds)
  const curators = disableAuthors(globalCurators, authorIds)
  const editors = disableAuthors(globalEditors, authorIds)
  const sectionEditors = disableAuthors(globalSectionEditors, authorIds)

  /**
   * Manuscript-level teams
   */

  const manuscriptCuratorTeam = manuscript.teams.find(t => t.role === 'curator')
  const manuscriptEditorTeam = manuscript.teams.find(t => t.role === 'editor')
  const manuscriptSectionEditorTeam = manuscript.teams.find(
    t => t.role === 'sectionEditor',
  )
  const manuscriptSOTeam = manuscript.teams.find(
    t => t.role === 'scienceOfficer',
  )

  const {
    curator: assignedCurator,
    editor: assignedEditor,
    sectionEditor: assignedSectionEditor,
    scienceOfficer: assignedScienceOfficer,
  } = getManuscriptTeam(manuscript.teams)

  /**
   * Build chat data
   */

  const chatData = chatThreads.map(thread => ({
    id: thread.id,
    chatType: thread.chatType,
    reviewerId: thread.userId,
    messages: transformChatMessages(thread.messages),
  }))

  /**
   * Build version specific data
   */

  const filteredVersions = dataVersions.filter(
    v =>
      (!v.submitted && isInitiallySubmitted && !isDataTypeSelected) ||
      v.submitted,
  )

  const versions = filteredVersions.map((version, index) => {
    const {
      authors,
      created,
      curatorReviews,
      decision,
      decisionLetter,
      id,
      reviews,
      submitted,
      teams,
      title,
    } = version

    const latest = index === filteredVersions.length - 1

    const { authorEmail, authorName } = getSubmittingAuthor(authors)
    const invitedReviewers = getInvitedReviewersTeam(teams)
    const reviewerCounts = getReviewerCounts(teams)
    const versionReviews = transformReviews(reviews)
    const curatorReview = transformCuratorReviews(curatorReviews)

    const previousReviewers = []
    if (latest && filteredVersions.length > 1) {
      // loop backwards in the array so that you get latest reviews
      // of the same reviewer first
      let currentIndex = index - 1

      while (currentIndex >= 0) {
        filteredVersions[currentIndex].reviews.forEach(review => {
          if (
            // exclude reviewers already invited for this version
            !invitedReviewers.find(r => r.user.id === review.reviewer.id) &&
            // exclude reviewers already added to the array
            !previousReviewers.find(
              reviewer => reviewer.id === review.reviewer.id,
            ) &&
            // exclude reviewers that never submitted
            review.status.submitted
          ) {
            previousReviewers.push({
              id: review.reviewer.id,
              displayName: review.reviewer.displayName,
              recommendation: review.recommendation,
            })
          }
        })
        currentIndex -= 1
      }
    }

    return {
      id,
      created,
      latest,

      dataType,
      decision,
      decisionLetter,
      title,

      authorEmail,
      authorName,

      invitedReviewersCount: reviewerCounts.invited,
      acceptedReviewersCount: reviewerCounts.accepted,
      rejectedReviewersCount: reviewerCounts.rejected,

      curatorReview,
      reviews: versionReviews,
      previousReviewers,
      submitted,
    }
  })

  /**
   * Mutations
   */

  const reinviteReviewer = (versionId, reviewerId) =>
    reinviteReviewerMutation({
      variables: {
        input: {
          manuscriptVersionId: versionId,
          reviewerId,
        },
      },
    })

  const sendChatMessage = (content, chatThreadId) =>
    sendChatMutation({
      variables: {
        input: {
          content,
          chatThreadId,
        },
      },
    })

  const setDataType = value =>
    setDataTypeMutation({
      variables: {
        manuscriptId: manuscript.id,
        input: { dataType: value },
      },
    })

  const submitDecision = (versionId, input) =>
    submitDecisionMutation({
      variables: {
        manuscriptVersionId: versionId,
        input,
      },
    })

  const updateManuscriptTeams = values => {
    const curatorMembers = []
    if (values.curator) {
      values.curator.forEach(curator => {
        if (curator.id) {
          curatorMembers.push(curator.id)
        }
      })
    }

    const editorMembers = []
    if (values.editor && values.editor.id) {
      editorMembers.push(values.editor.id)
    }

    const sectionEditorMembers = []
    if (values.sectionEditor && values.sectionEditor.id) {
      sectionEditorMembers.push(values.sectionEditor.id)
    }

    const scienceOfficerMembers = []
    if (values.scienceOfficer && values.scienceOfficer.id) {
      scienceOfficerMembers.push(values.scienceOfficer.id)
    }

    return updateManuscriptTeamsMutation({
      variables: {
        input: {
          teams: [
            {
              teamId: manuscriptCuratorTeam.id,
              members: curatorMembers,
            },
            {
              teamId: manuscriptEditorTeam.id,
              members: editorMembers,
            },
            {
              teamId: manuscriptSectionEditorTeam.id,
              members: sectionEditorMembers,
            },
            {
              teamId: manuscriptSOTeam.id,
              members: scienceOfficerMembers,
            },
          ],
        },
      },
    })
  }

  const updateMetadata = metadata =>
    updateMetadataMutation({
      variables: {
        manuscriptId: manuscript.id,
        data: {
          categories: metadata.categories,
          dbReferenceId: metadata.dbReferenceId,
          doi: metadata.doi,
          pmId: metadata.pmId,
          pmcId: metadata.pmcId,
          species: metadata.species,
          submissionTypes: metadata.submissionTypes,
        },
      },
    })

  return {
    editors,
    sectionEditors,
    scienceOfficers,
    curators,

    assignedCurator,
    assignedEditor,
    assignedSectionEditor,
    assignedScienceOfficer,

    categories,
    dbReferenceId,
    doi,
    pmId,
    pmcId,
    species,
    submissionTypes,
    history,
    // isInitiallySubmitted,
    // isDataTypeSelected,

    chatData,
    sendChatMessage,

    versions,

    reinviteReviewer,
    setDataType,
    submitDecision,
    updateManuscriptTeams,
    updateMetadata,
  }
}

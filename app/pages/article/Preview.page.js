import React from 'react'
import PropTypes from 'prop-types'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { last } from 'lodash'

import { FULL_PREVIEW, SEND_CHAT } from '../../graphql'
import { Loader } from '../../../ui'
import { ArticlePreview } from '../../components/ui'
import { exportManuscriptToPrint } from '../../fetch/exportManuscript'
import {
  getFromStorage,
  transformChatMessages,
  saveToStorage,
} from '../_helpers/common'

const PreviewPage = props => {
  const { isAuthor, isEditor, manuscriptId } = props

  const { data, loading } = useQuery(FULL_PREVIEW, {
    variables: { id: manuscriptId },
  })

  const [sendChatMessage] = useMutation(SEND_CHAT, {
    refetchQueries: [
      {
        query: FULL_PREVIEW,
        variables: { id: manuscriptId },
      },
    ],
  })

  let previewData,
    authorChatThread,
    authorChatMessages,
    saveAuthorChat,
    getSavedAuthorChat,
    sendChatMessageFn

  if (data && data.manuscript) {
    previewData = last(data.manuscript.versions)

    authorChatThread = data.manuscript.chatThreads.find(
      thread => thread.chatType === 'author',
    )

    authorChatMessages =
      authorChatThread && transformChatMessages(authorChatThread.messages)

    sendChatMessageFn = content =>
      sendChatMessage({
        variables: {
          input: {
            chatThreadId: authorChatThread.id,
            content,
          },
        },
      })

    saveAuthorChat = message =>
      saveToStorage(message, `chat_${authorChatThread.id}`)
    getSavedAuthorChat = () => getFromStorage(`chat_${authorChatThread.id}`)
  }

  if (loading) return <Loader />

  // JUST SUBMITTED

  return (
    <ArticlePreview
      article={previewData}
      authorChatMessages={authorChatMessages}
      exportManuscript={exportManuscriptToPrint}
      getSavedAuthorChat={getSavedAuthorChat}
      isAuthor={isAuthor}
      isEditor={isEditor}
      manuscriptId={manuscriptId}
      // previousVersion
      saveAuthorChat={saveAuthorChat}
      sendAuthorChatMessage={sendChatMessageFn}
      showAdditionalData
      showHeader
    />
  )
}

PreviewPage.propTypes = {
  isAuthor: PropTypes.bool.isRequired,
  isEditor: PropTypes.bool.isRequired,
  manuscriptId: PropTypes.string.isRequired,
}

export default PreviewPage

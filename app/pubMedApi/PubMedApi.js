const request = require('request')
const { head } = require('lodash')
const config = require('config')

const baseUrl = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/'

const apiKey = config.has('pubmed.apiKey') ? config.get('pubmed.apiKey') : null

const makeRequest = (req, res) => {
  const reqUrl = `${baseUrl}esummary.fcgi`
  const { id } = req.query
  if (id === '') return res.status(400).send('Empty query')

  const qs = {
    db: 'pubmed',
    id,
    retmode: 'json',
    tool: 'micropublication',
    email: 'contact@micropublication.org',
  }

  if (apiKey) {
    qs.api_key = apiKey
  }

  return request(
    {
      qs,
      url: reqUrl,
    },
    (error, response, body) => {
      // eslint-disable-next-line no-console
      if (error) return console.error(error)
      const pubMedResult = JSON.parse(body)
      if (pubMedResult.error) {
        return res.status(429).send(pubMedResult.error)
      }
      // There should be only one
      const uid = pubMedResult.result[head(pubMedResult.result.uids)]
      if (uid && uid.error) {
        return res.status(400).send(uid.error)
      }
      if (!uid) {
        return res.status(400).send('PubMed Lookup Failed')
      }

      return res.send({ reference: makeReference(uid) })
    },
  )
}

const makeValidateRequest = (req, res) => {
  const validateUrl = `${baseUrl}esearch.fcgi`
  request(
    {
      qs: {
        db: 'pubmed',
        term: `${req.query.id}[UID]`,
        retmode: 'json',
      },
      url: validateUrl,
    },
    (error, response, body) => {
      // eslint-disable-next-line no-console
      if (error) return console.error(error)
      const json = JSON.parse(body)
      const result = json.esearchresult.count === '1'
      return res.send({ data: { result } })
    },
  )
}

const makeReference = uid => {
  const authors = uid.authors.map(author => author.name).join(', ')
  const year = head(uid.pubdate.split(' '))
  const { title, source, volume, pages } = uid

  return `${authors}. ${year}. ${title} ${source} ${volume}: ${pages}.`
}

const PubMedApi = app => {
  /**
   * GET ENDPOINTS
   */

  app.get('/api/pubmed/fetch/reference', (req, res) => {
    makeRequest(req, res)
  })

  /**
   * VALIDATION ENDPOINTS
   */

  app.get('/api/pubmed/validate/reference', (req, res) => {
    makeValidateRequest(req, res)
  })
}

module.exports = PubMedApi

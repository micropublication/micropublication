// order of models matters

module.exports = [
  '@pubsweet/model-team',
  '@pubsweet/model-user',
  '@pubsweet/component-send-email',
  './server/api',
  './server/models/chatMessage',
  './server/models/chatThread',
  './server/models/curatorReview',
  './server/models/identity',
  './server/models/manuscript',
  './server/models/manuscriptVersion',
  './server/models/review',
  './server/models/teamMember',
  './server/models/team',
  './server/models/user',
  './app/wbApi',
  './server/export',
  './app/pubMedApi',
  './app/dataCiteApi',
]

const { deferConfig } = require('config/defer')
// const path = require('path')
const winston = require('winston')
require('winston-daily-rotate-file')

const logger = new winston.Logger({
  transports: [
    new winston.transports.Console({
      colorize: true,
      handleExceptions: true,
      humanReadableUnhandledException: true,
      prettyPrint: true,
    }),
    // new winston.transports.DailyRotateFile({
    //   datePattern: 'DD-MM-YYYY',
    //   dirname: path.join(__dirname, '../logs/development'),
    //   filename: 'app-%DATE%.log',
    //   handleExceptions: true,
    //   humanReadableUnhandledException: true,
    //   json: true,
    //   maxFiles: '30d',
    //   zippedArchive: true,
    // }),
  ],
})

module.exports = {
  datacite: {
    url: 'https://api.test.datacite.org/dois',
  },
  'pubsweet-client': {
    baseUrl: deferConfig(
      cfg => `http://localhost:${cfg['pubsweet-server'].port}`,
    ),
    host: 'http://localhost',
    port: 4000,
  },
  'pubsweet-server': {
    db: {
      database: 'wb',
      password: 'pass',
      port: 5480,
      user: 'dev',
    },

    protocol: 'http',
    host: 'localhost',
    port: 3000,

    logger,
    secret: 'somesecret',
  },
}

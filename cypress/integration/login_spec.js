const { admin } = require('../support/credentials')

const enterLoginData = (username, password) => {
  cy.get('input[name="username"]').type(username)
  cy.get('input[name="password"]').type(password)

  cy.get('button[type="submit"]').click()
}

describe('Login', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  it('Does not login non-existing user', () => {
    const username = 'wrongUsername'
    const password = 'wrongPassword'
    enterLoginData(username, password)

    cy.contains('Wrong username or password').should('exist')
  })

  it('Logs in existing user', () => {
    const { username, password } = admin
    enterLoginData(username, password)

    cy.url().should('include', '/dashboard')
  })

  it('Logs out logged in user', () => {
    cy.login('admin')
    cy.visit('/dashboard')

    cy.contains('Logout').click()
    cy.contains('Login').should('exist')
  })
})

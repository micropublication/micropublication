const logger = require('@pubsweet/logger')
const { Manuscript, Team, TeamMember } = require('@pubsweet/models')

logger.info('ENV is', process.env.NODE_ENV)

const clear = async () => {
  try {
    await Team.query()
      .delete()
      .whereNot({ global: true })

    await TeamMember.query().delete()

    await Manuscript.query().delete()
  } catch (e) {
    logger.error(e)
  }
}

clear()

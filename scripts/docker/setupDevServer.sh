#!/bin/sh
set -x

# This is run through docker. Its CWD will be the root folder.
sh scripts/setupDb.sh
node scripts/seedUsers.js
node scripts/seedManuscripts.js

exec "$@"

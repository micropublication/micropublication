const range = require('lodash/range')
const { transaction } = require('objection')
const { internet, lorem, name } = require('faker')

const logger = require('@pubsweet/logger')
const {
  ChatThread,
  Manuscript,
  ManuscriptVersion,
  Team,
  TeamMember,
  User,
} = require('@pubsweet/models')

const createManuscript = async options => {
  try {
    let manuscript

    const { submitted } = options

    await transaction(Manuscript.knex(), async trx => {
      const manuscriptData = {
        history: {
          received: submitted,
          sentForReview: null,
          reviewReceived: null,
          revisionReceived: null,
          accepted: null,
          published: null,
        },
        isDataTypeSelected: submitted,
        isInitiallySubmitted: submitted,
      }

      if (submitted) manuscriptData.dataType = 'noDatatype'
      manuscript = await Manuscript.query(trx).insert(manuscriptData)

      // Create first version
      const version = await ManuscriptVersion.query(trx).insert({
        active: true,
        manuscriptId: manuscript.id,
        acknowledgements: lorem.words(6),
        title: lorem.words(8),
        abstract: lorem.sentences(5),
        comments: lorem.sentences(3),
        disclaimer: true,
        funding: lorem.words(3),
        imageCaption: lorem.sentences(2),
        imageTitle: lorem.words(4),
        laboratory: {
          name: lorem.words(3),
          WBId: lorem.word(),
        },
        methods: lorem.sentences(10),
        reagents: lorem.sentences(10),
        patternDescription: lorem.sentences(10),
        suggestedReviewer: {
          name: name.findName(),
          WBId: lorem.word(),
        },
        image: {
          url: '/sample-image.jpg',
          name: 'bird flying',
        },
        authors: [
          {
            affiliations: ['Caltech'],
            correspondingAuthor: true,
            credit: ['investigation'],
            email: internet.email(),
            equalContribution: false,
            firstName: name.firstName(),
            lastName: name.lastName(),
            orcid: '',
            submittingAuthor: true,
            WBId: '',
          },
        ],
        submitted,

        // references: arrayOfObjectsNullable,
        // dataTypeFormData: object,
        // isApprovedByScienceOfficer: booleanNullable,
        // active: booleanNullable,
        // isReviewerAutomationOn: boolean,
        // reviewerPool: arrayOfIds,

        // decision: string,
        // decisionLetter: stringNotEmpty,
      })

      // Create SO & author chat threads
      await ChatThread.query(trx).insert({
        chatType: 'scienceOfficer',
        manuscriptId: manuscript.id,
      })

      await ChatThread.query(trx).insert({
        chatType: 'author',
        manuscriptId: manuscript.id,
      })

      // TO DO -- change objectType to manuscript
      const teamData = [
        // Teams on the manuscript
        {
          name: `editor-${manuscript.id}`,
          objectId: manuscript.id,
          objectType: 'article',
          role: 'editor',
        },
        {
          name: `section-editor-${manuscript.id}`,
          objectId: manuscript.id,
          objectType: 'article',
          role: 'sectionEditor',
        },
        {
          name: `science-officer-${manuscript.id}`,
          objectId: manuscript.id,
          objectType: 'article',
          role: 'scienceOfficer',
        },
        {
          name: `curator-${manuscript.id}`,
          objectId: manuscript.id,
          objectType: 'article',
          role: 'curator',
        },
        // Teams on the version
        {
          name: `author-${version.id}`,
          objectId: version.id,
          objectType: 'manuscriptVersion',
          role: 'author',
        },
        {
          name: `reviewer-${version.id}`,
          objectId: version.id,
          objectType: 'manuscriptVersion',
          role: 'reviewer',
        },
      ]

      const teams = await Team.query(trx).insert(teamData)

      // Add author to author manuscript team
      const authorTeam = teams.find(t => t.role === 'author')

      const author = await User.query(trx).findOne({
        username: 'author',
      })

      await TeamMember.query(trx).insert({
        teamId: authorTeam.id,
        userId: author.id,
      })
    })

    return manuscript.id
  } catch (e) {
    logger.error(
      `Seed manuscript data: Create new manuscript failed! Rolling back...`,
    )
    throw new Error(e)
  }
}

const seed = async () => {
  console.log('') /* eslint-disable-line no-console */
  logger.info('### CREATING FAKE MANUSCRIPTS ###')
  logger.info('>>> Creating manuscripts...')

  await Promise.all(
    range(4).map(i =>
      createManuscript({
        submitted: i < 2,
      }),
    ),
  )
}

seed()

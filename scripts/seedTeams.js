#!/usr/bin/env node

const logger = require('@pubsweet/logger')
const { Team } = require('@pubsweet/models')

const names = {
  editors: 'Editors',
  scienceOfficers: 'Science Officers',
  globalSectionEditor: 'Section Editors',
  globalCurator: 'Curators',
}

const makeTeam = async type => {
  logger.info(`>>> Checking if ${type} team exists...`)

  const team = await Team.query().findOne({
    global: true,
    role: type,
  })

  if (team) {
    logger.info(`>>> ${type} team found`)
    return
  }

  logger.info(`>>> ${type} team not found. Creating...`)

  try {
    await Team.query().insert({
      global: true,
      name: names[type],
      role: type,
    })

    logger.info(`>>> ${type} team successfully created.`)
  } catch (e) {
    logger.error(e)
    process.kill(process.pid)
  }
}

const seed = async () => {
  console.log('') /* eslint-disable-line no-console */
  logger.info('### CREATING GLOBAL TEAMS ###')

  await makeTeam('editors')
  await makeTeam('scienceOfficers')
  await makeTeam('globalSectionEditor')
  await makeTeam('globalCurator')
}

seed()

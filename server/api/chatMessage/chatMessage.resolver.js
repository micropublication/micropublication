const logger = require('@pubsweet/logger')
const { ChatMessage, ChatThread } = require('@pubsweet/models')

const { notify } = require('../../services')

const baseMessage = 'Chat Message:'

const chatThreadMessages = async chatThread => {
  try {
    const messages = await ChatMessage.query()
      .where({
        chatThreadId: chatThread.id,
      })
      .orderBy('timestamp')

    return messages
  } catch (e) {
    logger.error(`${baseMessage} Chat thread Messages: Failed!`)
    throw new Error(e)
  }
}

const sendChatMessage = async (_, { input }, ctx) => {
  try {
    const { chatThreadId, content } = input
    const userId = ctx.user

    const thread = await ChatThread.query().findById(chatThreadId)

    await ChatMessage.query().insert({
      chatThreadId,
      content,
      userId,
    })

    notify('chat', {
      manuscriptId: thread.manuscriptId,
      messageContent: content,
      reviewerId: thread.userId,
      type: thread.chatType,
      userId,
    })

    return true
  } catch (e) {
    logger.error(`${baseMessage} Send chat message: Failed!`)
    throw new Error(e)
  }
}

module.exports = {
  chatThreadMessages,
  sendChatMessage,
}

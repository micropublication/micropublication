const logger = require('@pubsweet/logger')
const { ChatThread } = require('@pubsweet/models')

const baseMessage = 'Chat Thread:'

const manuscriptChatThreads = async (
  manuscript,
  { currentUserOnly, type },
  ctx,
) => {
  try {
    const userId = ctx.user
    const conditions = { manuscriptId: manuscript.id }
    if (type) conditions.chatType = type
    if (currentUserOnly) conditions.userId = userId

    const threads = await ChatThread.query().where(conditions)
    return threads
  } catch (e) {
    logger.error(`${baseMessage} Manuscript chat threads: Failed!`)
    throw new Error(e)
  }
}

module.exports = {
  manuscriptChatThreads,
}

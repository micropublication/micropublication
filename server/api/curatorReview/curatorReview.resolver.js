const notify = require('../../services/notify')

const logger = require('@pubsweet/logger')
const { CuratorReview, ManuscriptVersion, User } = require('@pubsweet/models')

const baseMessage = 'Curator Review:'

const curatorReview = async (_, { id }) => {
  try {
    return CuratorReview.query()
      .findById(id)
      .throwIfNotFound()
  } catch (e) {
    logger.error(`${baseMessage} Failed to find curator review with id ${id}`)
    throw new Error(e)
  }
}

const curatorReviews = async () => {
  try {
    return CuratorReview.query()
  } catch (e) {
    logger.error(`${baseMessage} Could not fetch curator reviews`)
    throw new Error(e)
  }
}

const curator = async parent => {
  const { curatorId } = parent

  try {
    return User.findById(curatorId)
  } catch (e) {
    logger.error(`${baseMessage} Could not find user with id ${curatorId}`)
    throw new Error(e)
  }
}

const curatorReviewManuscriptVersion = async parent => {
  const { manuscriptVersionId } = parent

  try {
    return ManuscriptVersion.query()
      .findById(manuscriptVersionId)
      .throwIfNotFound()
  } catch (e) {
    logger.error(
      `${baseMessage} Could not find manuscript version with id ${manuscriptVersionId}`,
    )
    throw new Error(e)
  }
}

const saveCuratorReview = async (_, { id, input }) =>
  CuratorReview.save(id, input)

const submitCuratorReview = async (_, { id, input }) => {
  notify('curationSubmitted', {
    reviewId: id,
  })

  return CuratorReview.submit(id, input)
}

module.exports = {
  curator,
  curatorReviewManuscriptVersion,
  curatorReview,
  curatorReviews,
  saveCuratorReview,
  submitCuratorReview,
}

extend type Query {
  manuscript(id: ID!): Manuscript!
  manuscripts(role: String!): [Manuscript]!
  userTeams(id: ID!): [Team]
}

extend type Mutation {
  createManuscript: ID!
  deleteManuscript(id: ID!): ID!
  handleInvitation(action: String!, articleVersionId: ID!): Boolean!
  updateManuscriptMetadata(manuscriptId: ID!, data: MetadataInput!): ID!
  setDataType(manuscriptId: ID!, input: SetDataTypeInput!): ID!
}

type Manuscript {
  categories: [String]
  currentlyWith: ID
  chatThreads(currentUserOnly: Boolean, type: ChatType): [ChatThread!]!
  dataType: String
  dbReferenceId: String
  doi: String
  history: History
  id: ID!
  isDataTypeSelected: Boolean
  isInitiallySubmitted: Boolean
  pmcId: String
  pmId: String
  reviewerStatus: String
  species: [String]
  submissionTypes: [String]
  teams: [Team!]
  versions(
    activeOnly: Boolean
    invitedToReviewOnly: Boolean
    last: Int
  ): [ManuscriptVersion!]
}

type History {
  received: String
  sentForReview: String
  reviewReceived: String
  revisionReceived: String
  accepted: String
  published: String
}

type Status {
  reviewers: ReviewerStatus
  scienceOfficer: ScienceOfficerStatus
  submission: SubmissionStatus
  version: Int
}

type ReviewerStatus {
  accepted: Boolean
  invited: Boolean
  submitted: Boolean
}

type ScienceOfficerStatus {
  approved: Boolean
  pending: Boolean
}

type SubmissionStatus {
  datatypeSelected: Boolean
  full: Boolean
  initial: Boolean
}

input MetadataInput {
  categories: [String!]
  dbReferenceId: String!
  doi: String!
  pmId: String!
  pmcId: String!
  species: [String!]
  submissionTypes: [String!]  
}

input SetDataTypeInput {
  dataType: String!
}

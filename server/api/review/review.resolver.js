const clone = require('lodash/clone')

const {
  Identity,
  Manuscript,
  ManuscriptVersion,
  Review,
  Team,
  TeamMember,
  User,
} = require('@pubsweet/models')
const logger = require('@pubsweet/logger')

const { REVIEWER_STATUSES } = require('../constants')
const { notify, stopJob } = require('../../services')

const reviewSubmittedStatus = {
  pending: false,
  submitted: true,
}

const reinviteReviewer = async (_, { input }, ctx) => {
  const { manuscriptVersionId, reviewerId } = input

  try {
    const manuscriptVersion = await ManuscriptVersion.query().findById(
      manuscriptVersionId,
    )
    const { manuscriptId } = manuscriptVersion

    const team = await Team.query().findOne({
      objectId: manuscriptVersionId,
      role: 'reviewer',
    })

    await TeamMember.query().insert({
      status: REVIEWER_STATUSES.invited,
      teamId: team.id,
      userId: reviewerId,
    })

    notify('reinviteReviewer', {
      manuscriptId,
      reviewerId,
    })
  } catch (e) {
    throw new Error(e)
  }
}

// const requestReviewerAttention = async (_, { input }, ctx) => {
//   const { manuscriptId, reviewerId } = input

//   notify('requestReviewerAttention', {
//     manuscriptId,
//     reviewerId,
//     userId: ctx.user,
//   })

//   return true
// }

const saveReview = async (_, { reviewId, input }, ctx) => {
  try {
    await Review.query()
      .patch(input)
      .findById(reviewId)

    return true
  } catch (e) {
    logger.error('Review: Save review: Failed!')
    throw new Error(e)
  }
}

const submitReview = async (_, { reviewId, input }, ctx) => {
  const userId = ctx.user
  const data = clone(input)
  data.status = reviewSubmittedStatus

  try {
    const updated = await Review.query().patchAndFetchById(reviewId, data)

    // If this is the first review submitted, set `reviewReceived`
    const manuscript = await Manuscript.query()
      .leftJoin(
        'manuscript_versions',
        'manuscripts.id',
        'manuscript_versions.manuscript_id',
      )
      .findOne({ 'manuscript_versions.id': updated.articleVersionId })

    if (!manuscript.history.reviewReceived) {
      manuscript.history.reviewReceived = new Date().toISOString()
      await manuscript.$query().patch({ history: manuscript.history })
    }

    const teamMember = await TeamMember.query()
      .leftJoin('teams', 'teams.id', 'team_id')
      .findOne({
        userId: updated.reviewerId,
        role: 'reviewer',
        objectId: updated.articleVersionId,
      })

    notify('reviewSubmitted', {
      reviewId,
      userId,
    })

    // stop jobs that are not valid after submit
    await stopJob(`review-reminder-${teamMember.id}`)
    await stopJob(`revoke-invitation-after-accept-${teamMember.id}`)

    return updated.id
  } catch (e) {
    logger.error('Review: Submit review: Failed!')
    throw new Error(e)
  }
}

const reviewer = async (review, __, ctx) => {
  try {
    const user = await User.findById(review.reviewerId)

    const identity = await Identity.query().findOne({
      isDefault: true,
      userId: user.id,
    })

    user.email = identity.email

    return user
  } catch (e) {
    throw new Error(e)
  }
}

const reviews = async (_, { articleId, reviewerId }, ctx) => {
  try {
    return Review.query()
      .leftJoin(
        'manuscript_versions',
        'reviews.article_version_id',
        'manuscript_versions.id',
      )
      .leftJoin(
        'manuscripts',
        'manuscript_versions.manuscript_id',
        'manuscripts.id',
      )
      .where({
        'manuscripts.id': articleId,
        reviewerId,
      })
      .orderBy('created')
  } catch (e) {
    throw new Error(e)
  }
}

const manuscriptVersionReviews = async (
  manuscriptVersion,
  { currentUserOnly },
  ctx,
) => {
  const { user: userId } = ctx
  const { id: manuscriptVersionId } = manuscriptVersion

  const where = { articleVersionId: manuscriptVersionId }
  if (currentUserOnly) where.reviewerId = userId

  try {
    return Review.query()
      .where(where)
      .orderBy('created')
  } catch (e) {
    logger.error('Manuscript version reviews: Query failed!')
    throw new Error(e)
  }
}

module.exports = {
  manuscriptVersionReviews,
  reinviteReviewer,
  // requestReviewerAttention,
  reviewer,
  reviews,
  saveReview,
  submitReview,
}

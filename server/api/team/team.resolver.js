const { transaction } = require('objection')
const uniq = require('lodash/uniq')

const {
  Identity,
  Manuscript,
  ManuscriptVersion,
  Team,
  TeamMember,
  User,
} = require('@pubsweet/models')

const { logger } = require('@coko/server')

const { REVIEWER_STATUSES } = require('../constants')
const { notify, schedule } = require('../../services')
const useTransaction = require('../../models/_helpers/useTransaction')

// NO AUTH
const addExternalReviewer = async (_, { manuscriptVersionId, input }, ctx) => {
  const { email, givenNames, surname } = input
  let user, teamMember

  try {
    await transaction(User.knex(), async trx => {
      const userIdentity = await Identity.query(trx).findOne({ email })

      if (userIdentity) {
        // TO DO -- Give feedback that user already exists
        user = await User.query(trx).findById(userIdentity.userId)
      } else {
        user = await User.query(trx).insert({
          agreedTc: false,
          givenNames,
          surname,
        })

        await Identity.query(trx).insert({
          email,
          isConfirmed: false,
          isDefault: true,
          userId: user.id,
        })
      }

      const reviewersTeam = await Team.query(trx).findOne({
        objectId: manuscriptVersionId,
        role: 'reviewer',
      })

      if (!reviewersTeam)
        throw new Error('Add external reviewer: Reviewers team does not exist')

      teamMember = await TeamMember.query(trx).findOne({
        teamId: reviewersTeam.id,
        userId: user.id,
      })

      if (teamMember) {
        throw new Error(
          'Add external reviewer: User has already been added to reviewer team',
        )
      } else {
        const version = await ManuscriptVersion.query(trx)
          .findById(manuscriptVersionId)
          .throwIfNotFound()

        const currentMembers = await TeamMember.query(trx).where({
          teamId: reviewersTeam.id,
        })

        if (
          currentMembers &&
          currentMembers.length > version.amountOfReviewers
        ) {
          throw new Error('Add external reviewer: No available reviewer slots')
        }

        teamMember = await TeamMember.query(trx).insert({
          status: REVIEWER_STATUSES.invited,
          teamId: reviewersTeam.id,
          userId: user.id,
        })

        // update reviewer pool
        await version.$query(trx).patch({
          reviewerPool: uniq([...version.reviewerPool, user.id]),
        })
      }
    })

    return teamMember.id
  } catch (e) {
    logger.error('Add external reviewer: Transaction failed! Rolling back...')
    throw new Error(e)
  }
}

// NO AUTH
const inviteReviewer = async (_, { manuscriptVersionId, input }, context) => {
  const { reviewerId } = input
  let result

  const reviewer = await TeamMember.query()
    .leftJoin('teams', 'team_members.team_id', 'teams.id')
    .findOne({
      objectId: manuscriptVersionId,
      role: 'reviewer',
      userId: reviewerId,
    })

  if (!reviewer)
    throw new Error(
      'Invite reviewer: User was never added to the reviewer pool',
    )

  let alreadyInvited = false
  const { invited, accepted, rejected } = REVIEWER_STATUSES
  const alreadyInvitedStatuses = [invited, accepted, rejected]
  if (alreadyInvitedStatuses.includes(reviewer.status)) alreadyInvited = true

  if (!alreadyInvited) {
    try {
      result = await reviewer.$query().patchAndFetch({
        status: REVIEWER_STATUSES.invited,
      })
    } catch (e) {
      throw new Error(e)
    }

    // If this is the first invitation to review, set `sentForReview`
    const manuscriptHistory = await ManuscriptVersion.query()
      .select('manuscripts.history')
      .leftJoin(
        'manuscripts',
        'manuscript_versions.manuscript_id',
        'manuscripts.id',
      )
      .findOne({ 'manuscript_versions.id': manuscriptVersionId })

    if (!manuscriptHistory.history.sentForReview) {
      manuscriptHistory.history.sentForReview = new Date().toISOString()
      await Manuscript.query().patch({ history: manuscriptHistory.history })
    }
  } else {
    result = reviewer
  }

  notify('reviewerInvited', {
    reviewerId,
    userId: context.user,
    versionId: manuscriptVersionId,
  })

  schedule('reviewerInvited', {
    teamMemberId: reviewer.id,
    userId: context.user,
    versionId: manuscriptVersionId,
  })

  return result
}

const reviewerStatus = async (manuscript, __, ctx) => {
  const { id: manuscriptId } = manuscript
  const { user: userId } = ctx

  const queryStatus = await Manuscript.query()
    .select('team_members.status')
    .leftJoin(
      'manuscript_versions',
      'manuscripts.id',
      'manuscript_versions.manuscript_id',
    )
    .leftJoin('teams', 'manuscript_versions.id', 'teams.object_id')
    .leftJoin('team_members', 'team_members.team_id', 'teams.id')
    .leftJoin('users', 'users.id', 'team_members.user_id')
    .findOne({
      'manuscripts.id': manuscriptId,
      'teams.role': 'reviewer',
      'users.id': userId,
    })
    .orderBy('manuscript_versions.created', 'desc')
    .limit(1)

  return queryStatus.status
}

const teams = async (_, __, ctx) => {
  const allTeams = await Team.query()
  return allTeams
}

const updateAssignedGlobalUser = async (currentUserId, input, type) => {
  /* 
    Find the editor / science officer team for this manuscript
    If there is an assigned editor / so already, remove them
    Assign the given user id
  */

  const { manuscriptId, userId } = input
  let newAssignedMember

  try {
    await transaction(Team.knex(), async trx => {
      const isAuthor = await TeamMember.query(trx)
        .leftJoin('teams', 'teams.id', 'team_members.team_id')
        .findOne({
          objectId: manuscriptId,
          role: 'author',
        })

      if (isAuthor)
        throw new Error(
          'Team: Update assigned global user: Cannot assign author of the manuscript to be an editor or science officer for it',
        )

      const assignmentTeam = await Team.query(trx).findOne({
        name: `${type}-${manuscriptId}`,
      })

      // There can be only one
      const currentAssignedMember = await TeamMember.query(trx).findOne({
        teamId: assignmentTeam.id,
      })

      if (currentAssignedMember) {
        await TeamMember.query(trx).deleteById(currentAssignedMember.id)
      }

      newAssignedMember = await TeamMember.query(trx).insert({
        teamId: assignmentTeam.id,
        userId,
      })

      // TO DO -- currentlyWith should be deprecated
      await Manuscript.query(trx)
        .where({ id: manuscriptId })
        .patch({ currentlyWith: userId })
    })

    if (currentUserId !== userId) {
      notify('currentlyWith', {
        currentlyWithId: userId,
        manuscriptId,
        userId: currentUserId,
      })
    }

    // TO DO -- this will not work if we introduce an option to simply
    // unassign a member (instead of assigning someone else)
    return newAssignedMember.userId
  } catch (e) {
    logger.error('Team: Update assigned global user: Failed! Rolling back...')
    throw new Error(e)
  }
}

// NO AUTH
const updateAssignedEditor = async (_, { input }, ctx) =>
  updateAssignedGlobalUser(ctx.user, input, 'editor')

// NO AUTH
const updateAssignedScienceOfficer = async (_, { input }, ctx) =>
  updateAssignedGlobalUser(ctx.user, input, 'science-officer')

const updateGlobalTeamMembership = async (_, { input }, ctx) => {
  const { teams: inputTeams } = input

  try {
    await useTransaction(async trx =>
      Promise.all(
        inputTeams.map(team =>
          Team.updateMembershipByTeamId(team.teamId, team.members, { trx }),
        ),
      ),
    )
    return true
  } catch (e) {
    logger.error('Team resolver: Update global team membership: Failed!')
    throw new Error(e)
  }
}

// TO DO -- refactor, same as updateGlobalTeamMembership
// AUTH NOTE: should section editors be able to assign another editor ???
const updateManuscriptTeamMembership = async (_, { input }, ctx) => {
  const { teams: inputTeams } = input

  try {
    await useTransaction(async trx =>
      Promise.all(
        inputTeams.map(team =>
          Team.updateMembershipByTeamId(team.teamId, team.members, { trx }),
        ),
      ),
    )

    return true
  } catch (e) {
    logger.error('Team resolver: Update manuscript team membership: Failed!')
    throw new Error(e)
  }
}

const teamsForObject = async (object, __, ctx) =>
  Team.query().where({
    objectId: object.id,
  })

const getGlobalTeams = async (_, __, ___) => {
  try {
    return Team.findAllGlobalTeams()
  } catch (e) {
    logger.error('Team resolver: Get global teams: Query failed!')
    throw new Error(e)
  }
}

// TO DO -- DEPRECATE THIS VERSION IN FAVOUR OF THE ABOVE
const globalTeams = async (_, __, ctx) => {
  let globalTs
  try {
    globalTs = await Team.query()
      .where('global', true)
      .eager('[members.[user]]')
  } catch (e) {
    logger.error(e)
    globalTs = []
  }
  return globalTs
}

const members = async (team, vars, ctx) =>
  TeamMember.query().where({
    teamId: team.id,
  })

const teamMemberUser = async (teamMember, vars, ctx) => {
  const user = await User.findById(teamMember.userId)
  const identity = await Identity.query().findOne({
    isDefault: true,
    userId: teamMember.userId,
  })

  user.email = identity.email

  return user
}

const revokeInvitation = async (_, args, ctx) => {
  const { manuscriptVersionId, reviewerId } = args

  try {
    return useTransaction(async trx => {
      const manuscriptVersion = await ManuscriptVersion.query(trx).findById(
        manuscriptVersionId,
      )

      const reviewer = await TeamMember.query(trx)
        .leftJoin('teams', 'team_members.team_id', 'teams.id')
        .findOne({
          objectId: manuscriptVersionId,
          role: 'reviewer',
          userId: reviewerId,
        })

      if (!reviewer)
        throw new Error('User was never added to the reviewer pool')

      if (reviewer.status === REVIEWER_STATUSES.revoked)
        throw new Error('Invitation has been revoked already')

      await reviewer.$query(trx).patchAndFetch({
        status: REVIEWER_STATUSES.revoked,
      })

      if (manuscriptVersion.isReviewerAutomationOn) {
        await manuscriptVersion.inviteMaxReviewers({ trx })
      }

      notify('revokeInvitation', {
        reviewerId,
        userId: ctx.user,
        versionId: manuscriptVersionId,
      })

      return manuscriptVersion
    })
  } catch (e) {
    logger.error(`Revoke invitation: ${e}`)
    throw new Error(e)
  }
}

module.exports = {
  addExternalReviewer,
  getGlobalTeams,
  globalTeams,
  inviteReviewer,
  members,
  reviewerStatus,
  revokeInvitation,
  teamMemberUser,
  teams,
  teamsForObject,
  updateAssignedEditor,
  updateAssignedScienceOfficer,
  updateGlobalTeamMembership,
  updateManuscriptTeamMembership,
}

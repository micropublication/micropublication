const clone = require('lodash/clone')
const config = require('config')
const crypto = require('crypto')
const moment = require('moment')
const { transaction } = require('objection')

const logger = require('@pubsweet/logger')
const {
  AuthorizationError,
  ConflictError,
  ValidationError,
} = require('@pubsweet/errors')
const {
  Identity,
  Manuscript,
  ManuscriptVersion,
  User,
} = require('@pubsweet/models')
const { createJWT } = require('@coko/server')

const { auth, notify } = require('../../services')

const chatMessageUser = async chatMessage => {
  try {
    const user = await User.findById(chatMessage.userId)
    return user
  } catch (e) {
    logger.error('User: Chat message user: Failed!')
    throw new Error(e)
  }
}

const currentUser = async (_, { input }, ctx) => {
  const userId = ctx.user
  if (!userId) throw new Error('Current User: No user id provided')

  const user = await User.findById(userId)

  const { createClientAuth } = auth
  user.auth = await createClientAuth(userId)

  const identity = await Identity.query().findOne({
    isDefault: true,
    userId: user.id,
  })

  user.orcid = identity.orcid
  user.email = identity.email

  return user
}

const displayName = async user => User.getDisplayName(user)

const login = async (_, { input }, ctx) => {
  const { username, password } = input
  let isValid = false
  let user

  try {
    user = await User.query().findOne({ username })
    if (!user) throw new AuthorizationError('Wrong username or password.')
    // throw new Error(`Login: No user with username ${username} exists`)

    isValid = await user.validPassword(password)
    if (!isValid) {
      throw new AuthorizationError('Wrong username or password.')
    }

    const identities = await Identity.query().where({ userId: user.id })
    const isConfirmed = identities.some(id => id.isConfirmed)
    if (!isConfirmed) throw new Error('Login: Identity not confirmed')

    return {
      token: createJWT(user),
    }
  } catch (e) {
    logger.error('Login: Failed!')
    throw new Error(e)
  }
}

const sendPasswordResetEmail = async (_, { email }, ctx) => {
  // // fail early if these configs are missing
  const baseUrl = config.get('pubsweet-server.baseUrl')

  const pathToPage = config.has('password-reset.pathToPage')
    ? config.get('password-reset.pathToPage')
    : '/password-reset'
  const tokenLength = config.has('password-reset.token-length')
    ? config.get('password-reset.token-length')
    : 32

  const identity = await Identity.query().findOne({
    isDefault: true,
    email: email.toLowerCase(),
  })

  if (!identity) {
    notify('requestResetPasswordEmailNotFound', { email })
    return true
  }

  const user = await User.query().findById(identity.userId)

  const resetToken = crypto.randomBytes(tokenLength).toString('hex')

  await user.$query().patch({
    passwordResetTimestamp: new Date(),
    passwordResetToken: resetToken,
  })

  const passwordResetURL = `${baseUrl}${pathToPage}/${resetToken}`

  logger.info(`Sending password reset email to ${identity.email}`)

  notify('requestResetPassword', {
    email: identity.email,
    link: passwordResetURL,
  })

  return true
}

const resendVerificationEmail = async (_, { token }) => {
  try {
    const identity = await Identity.query().findOne({
      confirmationToken: token,
    })

    if (!identity)
      throw new Error(
        'Resend Verification Email: Token does not correspond to an identity',
      )

    const confirmationToken = crypto.randomBytes(64).toString('hex')
    const confirmationTokenTimestamp = new Date()

    await identity.$query().patch({
      confirmationToken,
      confirmationTokenTimestamp,
    })

    notify('identityVerification', {
      confirmationToken,
      email: identity.email,
    })

    return true
  } catch (e) {
    logger.error('Resend Verification Email: Something went wrong')
    throw new Error(e)
  }
}

const resendVerificationEmailFromLogin = async (_, { username, password }) => {
  try {
    const user = await User.query().findOne({ username })
    if (!user)
      throw new Error(
        `Resend Verification Email From Login: No user with username ${username} found`,
      )

    const identity = await Identity.query().findOne({
      isDefault: true,
      userId: user.id,
    })

    if (!identity)
      throw new Error(
        `Resend Verification Email From Login: No default identity found for user with id ${user.id}`,
      )

    const confirmationToken = crypto.randomBytes(64).toString('hex')
    const confirmationTokenTimestamp = new Date()

    await identity.$query().patch({
      confirmationToken,
      confirmationTokenTimestamp,
    })

    notify('identityVerification', {
      confirmationToken,
      email: identity.email,
    })

    return true
  } catch (e) {
    logger.error('Resend Verification Email From Login: Something went wrong')
    throw new Error(e)
  }
}

const validatePasswordTokenExpiry = async (_, { token }) => {
  try {
    const user = await User.query().findOne({ passwordResetToken: token })
    if (!user) {
      logger.error(`Validate password token: No user found with token ${token}`)
      throw new Error('Something went wrong!')
    }

    if (
      moment()
        .subtract(24, 'hours')
        .isAfter(user.passwordResetTimestamp)
    ) {
      // throw new ValidationError('Your token has expired')
      return false
    }

    return true
  } catch (e) {
    logger.error('User: Validate password token expiry: Something went wrong')
    throw new Error(e)
  }
}

const resetPassword = async (_, { token, password }, ctx) => {
  const user = await User.query().findOne({ passwordResetToken: token })
  if (!user) {
    logger.error(`Validate password token: No user found with token ${token}`)
    throw new Error('Something went wrong!')
  }

  if (
    moment()
      .subtract(24, 'hours')
      .isAfter(user.passwordResetTimestamp)
  ) {
    throw new ValidationError('Your token has expired')
  }

  await user.$query().patch({
    password,
    passwordResetTimestamp: null,
    passwordResetToken: null,
  })

  notify('passwordUpdate', { userId: user.id })

  return true
}

/* eslint-disable-next-line consistent-return */
const signUp = async (_, { input }, ctx) => {
  const userInput = clone(input)
  const { email, givenNames, password, surname, username, orcid } = userInput

  const usernameExists = await User.query().findOne({ username })
  if (usernameExists) {
    logger.error('Sign up: Username already exists')
    throw new ConflictError('Username already exists')
  }

  const existingIdentity = await Identity.query().findOne({ email })

  if (existingIdentity) {
    const user = await User.query().findById(existingIdentity.userId)

    if (user.agreedTc) {
      logger.error('Sign up: A user with this email already exists')
      throw new ConflictError('A user with this email already exists')
    }

    // If not agreed to tc, user's been invited but is now signing up
    delete userInput.email
    delete userInput.orcid

    try {
      let updatedUser, confirmationToken

      await transaction(User.knex(), async trx => {
        updatedUser = await User.query(trx).patchAndFetchById(
          existingIdentity.userId,
          {
            ...userInput,
            agreedTc: true,
          },
        )

        confirmationToken = crypto.randomBytes(64).toString('hex')
        const confirmationTokenTimestamp = new Date()

        await existingIdentity.$query(trx).patch({
          confirmationToken,
          confirmationTokenTimestamp,
          orcid,
        })
      })

      notify('identityVerification', {
        confirmationToken,
        email,
      })

      return updatedUser
    } catch (e) {
      throw new Error(e)
    }
  }

  if (!existingIdentity) {
    try {
      let newUser
      const knex = User.knex()

      await transaction(knex, async trx => {
        newUser = await User.query(trx).insert({
          agreedTc: true,
          givenNames,
          password,
          surname,
          username,
        })

        const confirmationToken = crypto.randomBytes(64).toString('hex')
        const confirmationTokenTimestamp = new Date()

        await Identity.query(trx).insert({
          confirmationToken,
          confirmationTokenTimestamp,
          email,
          isConfirmed: false,
          isDefault: true,
          userId: newUser.id,
          orcid,
        })

        notify('identityVerification', {
          confirmationToken,
          email,
        })
      })

      return User.findById(newUser.id)
    } catch (e) {
      logger.error('Signup: User creation failed! Rolling back...')
      throw new Error(e)
    }
  }
}

// NO AUTH
const updatePassword = async (_, { input }, ctx) => {
  const userId = ctx.user
  const { currentPassword, newPassword } = input

  try {
    const u = await User.updatePassword(userId, currentPassword, newPassword)
    notify('passwordUpdate', { userId })
    return u.id
  } catch (e) {
    throw new Error(e)
  }
}

// NO AUTH
const updatePersonalInformation = async (_, { input }, ctx) => {
  const { givenNames, surname, orcid } = input
  const userId = input.userId || ctx.user

  await Identity.query()
    .patch({ orcid })
    .where({
      isDefault: true,
      userId,
    })

  return User.query().patchAndFetchById(userId, {
    givenNames,
    surname,
  })
}

// NO AUTH
const updateUsername = async (_, { input }, ctx) => {
  const { username } = input
  const userId = input.userId || ctx.user

  return User.query().patchAndFetchById(userId, { username })
}

const users = async (_, __, ctx) => {
  const all = await User.query()
  const withIdentity = all.map(async u => {
    const user = await User.findById(u.id)
    const identity = await Identity.query().findOne({ userId: u.id })
    delete identity.id
    delete identity.userId
    return { ...user, ...identity }
  })

  return withIdentity
}

const verifyEmail = async (_, { token }, ctx) => {
  try {
    const identity = await Identity.query().findOne({
      confirmationToken: token,
    })

    if (!identity) throw new Error('Verify email: Invalid token')

    if (identity.isConfirmed)
      throw new Error('Verify email: Identity has already been confirmed')

    if (!identity.confirmationTokenTimestamp) {
      throw new Error(
        'Verify email: Confirmation token does not have a corresponding timestamp',
      )
    }

    if (
      moment()
        // .subtract(5, 'seconds')
        .subtract(24, 'hours')
        .isAfter(identity.confirmationTokenTimestamp)
    ) {
      throw new Error('Verify email: Token expired')
    }

    await identity.$query().patch({
      // confirmationToken: null,
      // confirmationTokenTimestamp: null,
      isConfirmed: true,
    })

    return true
  } catch (e) {
    throw new Error(e)
  }
}

const searchForReviewer = async (_, args, ctx) => {
  const { searchQuery, manuscriptVersionId, limit } = args

  try {
    // We'll need the manuscript id as well (for building meta)
    const manuscriptVersion = await ManuscriptVersion.query()
      .findById(manuscriptVersionId)
      .throwIfNotFound()

    const { manuscriptId } = manuscriptVersion

    // Find users that match
    const search = `%${searchQuery}%`

    let query = User.query()
    if (limit && limit >= 0) query = query.limit(limit)

    const userResults = await query
      .where('givenNames', 'ilike', search)
      .orWhere('surname', 'ilike', search)

    // Build results with meta
    const results = await Promise.all(
      userResults.map(async user => {
        const isAuthor = await ManuscriptVersion.hasRole(
          user.id,
          manuscriptVersionId,
          'author',
        )

        const isReviewer = await ManuscriptVersion.hasRole(
          user.id,
          manuscriptVersionId,
          'reviewer',
        )

        const isEditor = await Manuscript.hasRole(
          user.id,
          manuscriptId,
          'editor',
        )

        const isSectionEditor = await Manuscript.hasRole(
          user.id,
          manuscriptId,
          'sectionEditor',
        )

        const isScienceOfficer = await Manuscript.hasRole(
          user.id,
          manuscriptId,
          'scienceOfficer',
        )

        const isCurator = await Manuscript.hasRole(
          user.id,
          manuscriptId,
          'curator',
        )

        const notSignedUp = !user.agreedTc

        return {
          user,
          meta: {
            isAuthorOfItem: !!isAuthor,
            isCuratorOfItem: !!isCurator,
            isEditorOfItem: !!isEditor,
            isReviewerOfItem: !!isReviewer,
            isSectionEditorOfItem: !!isSectionEditor,
            isScienceOfficerOfItem: !!isScienceOfficer,
            notSignedUp,
          },
        }
      }),
    )

    return results
  } catch (e) {
    logger.error(`User resolver: Search for reviewer: ${e}`)
    throw new Error(e)
  }
}

module.exports = {
  chatMessageUser,
  currentUser,
  displayName,
  login,
  resendVerificationEmail,
  resendVerificationEmailFromLogin,
  resetPassword,
  searchForReviewer,
  sendPasswordResetEmail,
  signUp,
  updatePassword,
  updatePersonalInformation,
  updateUsername,
  users,
  validatePasswordTokenExpiry,
  verifyEmail,
}

const Ajv = require('ajv')
const beautify = require('js-beautify').html
const uniq = require('lodash/uniq')
const flatten = require('lodash/flatten')

const logger = require('@pubsweet/logger')

const errorMessage = 'HTML file creation:'

/* Helpers */

const contributionAuthor = name => /* html */ `
  <span data-id="author-contributions-item-name">
    ${name}: 
  </span>
`

const contributionCredit = credit => {
  const data = credit
    .map(c => unCamelCase(c))
    .map(item => contributionCreditItem(item))
    .join(', ')

  return /* html */ `
    <span data-id="author-contributions-item-credit-section">
      ${data}
    </span>
  `
}

const contributionCreditItem = text => /* html */ `
  <span data-id="credit-section-item">
    ${text}
  </span>
`

const contributionItem = author => /* html */ `
  <span data-id="author-contributions-item">
    ${contributionAuthor(author.name)}
    ${contributionCredit(author.credit)}
  </span>
`

const getFieldValues = (list, field) => {
  const values = list.map(item => item[field])
  return values
}

const textSectionWithHeader = ({ data, label, name }) => /* html */ `
  <div data-id="${name}">
    <h2 data-id="${name}-header">
      ${label}
    </h2>

    <div data-id="${name}-content">
      ${data}
    </div>
  </div>
`

const unCamelCase = string =>
  string.replace(/([A-Z])/g, ' $1').replace(/^./, str => str.toUpperCase())

/* End Helpers */

/* Parts */
const authorContributionsEl = authors => {
  const data = authors.map(author => ({
    credit: author.credit,
    name: `${author.firstName} ${authors.lastName}`,
  }))

  const items = data.map(item => contributionItem(item)).join('; ')

  return /* html */ `
    <div data-id="author-contributions-section">
      ${items}
    </div>
  `
}

const authorsEl = authors => {
  // const names = getFieldValues(authors, 'lastName')
  const nameEls = authors
    .map(
      author =>
        /* html */ `<span data-id="author-name">${author.firstName} ${author.lastName}</span>`,
    )
    .join(', ')

  const affiliations = getFieldValues(authors, 'affiliations')
  const affiliationsEls = uniq(flatten(affiliations))
    .map(
      affiliation =>
        /* html */ `<div data-id="author-affiliation">${affiliation}</div>`,
    )
    .join('')

  return /* html */ `
    <div data-id="author-section">
      <div data-id="author-names">
        ${nameEls}
      </div>
      <div data-id="author-affiliations">
        ${affiliationsEls}
      </div>
    </div>
  `
}

const copyRight = /* html */ `
  <div data-id="copyright-section">
    <span data-id="copyright-header">
      Copyright
    </span>

    <span data-id="copyright-content"> 
      © 2019 by the authors. This is an open-access article distributed 
      under the terms of the Creative Commons Attribution 4.0 International 
      (CC BY 4.0) License, which permits unrestricted use, distribution, and 
      reproduction in any medium, provided the original author and source are 
      credited.
    </span>
  </div>
`

const descriptionEl = description =>
  textSectionWithHeader({
    data: description,
    label: 'Description',
    name: 'description',
  })

const fundingEl = funding =>
  textSectionWithHeader({
    data: funding,
    label: 'Funding',
    name: 'funding',
  })

const imageEl = (imageSrc, imageCaption, imageTitle) => /* html */ `
  <div data-id="image-section">
    <figure>
      <img data-id="image" src="${imageSrc}" />
      <figcaption>
        ${imageTitle} ${imageCaption}
      </figcaption>
    </figure>
  </div>
`

const methodsEl = methods =>
  textSectionWithHeader({
    data: methods,
    label: 'Methods',
    name: 'methods',
  })

const reagentsEl = reagents =>
  textSectionWithHeader({
    data: reagents,
    label: 'Reagents',
    name: 'reagents',
  })

const referencesEl = references =>
  textSectionWithHeader({
    data: references,
    label: 'References',
    name: 'references',
  })

const titleEl = title => /* html */ `
  <h1 data-id="title">
    ${title}
  </h1>
`
/* End Parts */

/* 
  Create complete HTML string
  reviewed by -- don't have name
  received / accepted / published online -- don't have dates!
  citation -- don't have author first/last names
*/
const createHTML = (manuscript, imageSrc) => `
  ${titleEl(manuscript.title)}
  ${authorsEl(manuscript.authors)}
  ${imageEl(imageSrc, manuscript.imageCaption, manuscript.imageTitle)}
  ${descriptionEl(manuscript.patternDescription)}
  ${methodsEl(manuscript.methods)}
  ${reagentsEl(manuscript.reagents)}
  ${referencesEl(manuscript.references)}
  ${fundingEl(manuscript.funding)}
  ${authorContributionsEl(manuscript.authors)}
  ${copyRight}
`

/* Validation */
/* eslint-disable sort-keys */
const stringNotEmpty = {
  type: 'string',
  minLength: 1,
}

const arrayOfStringsNotEmpty = {
  type: 'array',
  items: stringNotEmpty,
  minItems: 1,
}

const arrayNotEmpty = keys => ({
  ...keys,
  type: 'array',
  minItems: 1,
})

const author = {
  type: 'object',
  properties: {
    affiliations: arrayOfStringsNotEmpty,
    credit: arrayOfStringsNotEmpty,
    lastName: stringNotEmpty,
  },
  required: ['affiliations', 'credit', 'lastName'],
}

const authors = arrayNotEmpty({ items: author })

const schema = {
  type: 'object',
  properties: {
    authors,
    funding: stringNotEmpty,
    imageCaption: stringNotEmpty,
    imageTitle: stringNotEmpty,
    patternDescription: stringNotEmpty,
    title: stringNotEmpty,
  },
  required: [
    'authors',
    'funding',
    'imageCaption',
    'imageTitle',
    'patternDescription',
    'title',
  ],
}
/* eslint-enable sort-keys */

const validate = (manuscript, imageSrc) => {
  const errorText = `${errorMessage} Validate:`
  if (!imageSrc) {
    logger.error(`${errorText} No image src string provided`)
    return false
  }

  const ajv = new Ajv()
  const valid = ajv.validate(schema, manuscript)
  if (!valid) logger.error(`${errorText} ${ajv.errorsText()}`)

  return valid
}
/* End Validation */

const output = (manuscript, imageSrc) => {
  const valid = validate(manuscript, imageSrc)
  if (!valid) return null

  const html = createHTML(manuscript, imageSrc)
  return beautify(html)
}

module.exports = output

// import HTMLExport from '../services/HTMLExport'

const fs = require('fs')
const path = require('path')
const { promisify } = require('util')
const JSZip = require('jszip')
const config = require('config')

const logger = require('@pubsweet/logger')
const { ManuscriptVersion } = require('@pubsweet/models')

const HTMLExport = require('./HTML')
const PrintExport = require('./print')

const readFile = promisify(fs.readFile)
const writeFile = promisify(fs.writeFile)

const exportEndpoints = app => {
  app.get('/api/export/:manuscriptId/html', async (req, res) => {
    const { manuscriptId } = req.params

    // Get manuscript version
    let version
    try {
      version = await ManuscriptVersion.query().findOne({
        active: true,
        manuscriptId,
      })
    } catch (e) {
      logger.error(e)
      throw new Error(e)
    }

    // Define names and paths for files
    const htmlFileName = `${manuscriptId}.html`
    const htmlFilePath = path.join(__dirname, '_tempFileStorage', htmlFileName)

    const imageFileName = version.image.url.substring(1)
    const imageFilePath = path.join(
      __dirname,
      '..',
      '..',
      'uploads',
      imageFileName,
    )

    const zip = new JSZip()
    const zipFileName = `${manuscriptId}.zip`
    const zipFilePath = path.join(__dirname, '_tempFileStorage', zipFileName)

    const deleteFiles = () => {
      const filesToDelete = [htmlFilePath, zipFilePath]

      filesToDelete.forEach(file => {
        if (fs.existsSync(file)) {
          fs.unlink(file, err => {
            if (err) {
              logger.error(err)
              return
            }

            logger.info(`${file} deleted from temp folder`)
          })
        }
      })
    }

    const createZipFile = callback => {
      zip
        .generateNodeStream({
          streamFiles: true,
          type: 'nodebuffer',
        })
        .pipe(fs.createWriteStream(zipFilePath))
        .on('finish', () => {
          logger.info(`Zip file created for manuscript ${manuscriptId}`)
          callback()
        })
    }

    // Make HTML file
    const html = HTMLExport(version, imageFileName)
    await writeFile(htmlFilePath, html)
    logger.info(`HTML file created for manuscript ${manuscriptId}`)

    // Add HTML and image to zip
    await Promise.all([
      readFile(htmlFilePath).then(data => zip.file('index.html', data)),
      readFile(imageFilePath).then(data => zip.file(imageFileName, data)),
    ])
      .then(() => {
        // Send zip file back to client for download
        createZipFile(() => {
          res.download(zipFilePath, `microPublication_${manuscriptId}.zip`)
        })
      })
      .catch(e => {
        logger.error(e)
        deleteFiles()
      })

    // Delete HTML and zip files when done
    res.on('finish', deleteFiles)
  })

  app.get('/api/export/:manuscriptVersionId/print', async (req, res) => {
    const { manuscriptVersionId } = req.params
    const serverUrl =
      config.has('pubsweet-server.baseUrl') &&
      config.get('pubsweet-server.baseUrl')

    // Get manuscript version
    let version
    try {
      version = await ManuscriptVersion.query().findOne({
        id: manuscriptVersionId,
      })
    } catch (e) {
      logger.error(e)
      throw new Error(e)
    }

    const html = PrintExport(
      version,
      `${serverUrl}/uploads${version.image.url}`,
    )
    res.send(html)
  })
}

module.exports = exportEndpoints

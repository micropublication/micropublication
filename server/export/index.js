const exportEndpoints = require('./export')

module.exports = {
  server: () => app => exportEndpoints(app),
}

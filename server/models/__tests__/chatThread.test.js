const { v4: uuid } = require('uuid')

const {
  ChatMessage,
  ChatThread,
  Manuscript,
  User,
} = require('@pubsweet/models')

const clearDb = require('./helpers/clearDb')

describe('ChatThread Model', () => {
  beforeAll(() => clearDb())
  afterEach(() => clearDb())

  afterAll(() => {
    const knex = ChatThread.knex()
    knex.destroy()
  })

  test('creates a new thread', async () => {
    const manuscript = await Manuscript.query().insert({})

    const thread = await ChatThread.query().insert({
      manuscriptId: manuscript.id,
      chatType: 'scienceOfficer',
    })

    expect(thread.chatType).toEqual('scienceOfficer')
    expect(thread.manuscriptId).toEqual(manuscript.id)
  })

  test('does not create new thread without a manuscript id', async () => {
    const createThread = () =>
      ChatThread.query().insert({
        chatType: 'reviewer',
      })

    await expect(createThread()).rejects.toThrow()
  })

  test('does not create a new thread with an invalid manuscript id', async () => {
    const manuscriptId = uuid()

    const createThread = () =>
      ChatThread.query().insert({
        manuscriptId,
        chatType: 'reviewer',
      })

    await expect(createThread()).rejects.toThrow()
  })

  test('does not create a new thread without a valid chat type', async () => {
    const manuscript = await Manuscript.query().insert({})

    const createThread = () =>
      ChatThread.query().insert({
        manuscriptId: manuscript.id,
        chatType: 'wrong',
      })

    await expect(createThread()).rejects.toThrow()
  })

  test('can retrieve the thread messages', async () => {
    const manuscript = await Manuscript.query().insert({})
    const userOne = await User.query().insert({})
    const userTwo = await User.query().insert({})
    const userThree = await User.query().insert({})
    const thread = await ChatThread.query().insert({
      chatType: 'author',
      manuscriptId: manuscript.id,
    })

    const messages = [
      {
        chatThreadId: thread.id,
        userId: userOne.id,
        content: 'this is it',
      },
      {
        chatThreadId: thread.id,
        userId: userTwo.id,
        content: 'is it?',
      },
      {
        chatThreadId: thread.id,
        userId: userThree.id,
        content: 'think so',
      },
    ]

    await ChatMessage.query().insert(messages)

    const result = await ChatThread.query()
      .findById(thread.id)
      .eager('messages')

    expect(result.messages).toHaveLength(3)
    expect(result.messages[0].userId).toEqual(messages[0].userId)
    expect(result.messages[1].content).toEqual(messages[1].content)
    expect(result.messages[2].chatThreadId).toEqual(messages[2].chatThreadId)
  })

  test('can only have one author chat per manuscript', async () => {
    const manuscript = await Manuscript.query().insert({})

    const createThread = async () =>
      ChatThread.query().insert({
        chatType: 'author',
        manuscriptId: manuscript.id,
      })

    // First should be fine, second should make the constraint throw
    await createThread()
    await expect(createThread()).rejects.toThrow()
  })

  test('can only have one science officer chat per manuscript', async () => {
    const manuscript = await Manuscript.query().insert({})

    const createThread = async () =>
      ChatThread.query().insert({
        chatType: 'scienceOfficer',
        manuscriptId: manuscript.id,
      })

    // First should be fine, second should make the constraint throw
    await createThread()
    await expect(createThread()).rejects.toThrow()
  })

  test('can only have one reviewer chat per manuscript per reviewer', async () => {
    const manuscript = await Manuscript.query().insert({})
    const userOne = await User.query().insert({})
    const userTwo = await User.query().insert({})

    const createThread = async reviewerId =>
      ChatThread.query().insert({
        chatType: 'reviewer',
        manuscriptId: manuscript.id,
        userId: reviewerId,
      })

    // Different reviewer ids for the same manuscript should be fine
    await createThread(userOne.id)
    await createThread(userTwo.id)

    const threads = await ChatThread.query()
    expect(threads).toHaveLength(2)

    // But the same reviewer id on the same manuscript should make the constraint throw
    await expect(createThread(userOne.id)).rejects.toThrow()
  })

  test('can only have one curator chat per manuscript per curator', async () => {
    const manuscript = await Manuscript.query().insert({})
    const userOne = await User.query().insert({})
    const userTwo = await User.query().insert({})

    const createThread = async curatorId =>
      ChatThread.query().insert({
        chatType: 'curator',
        manuscriptId: manuscript.id,
        userId: curatorId,
      })

    // Different curator ids for the same manuscript should be fine
    await createThread(userOne.id)
    await createThread(userTwo.id)

    const threads = await ChatThread.query()
    expect(threads).toHaveLength(2)

    // But the same curator id on the same manuscript should make the constraint throw
    await expect(createThread(userOne.id)).rejects.toThrow()
  })

  test('user id must not be null for reviewer chats', async () => {
    const manuscript = await Manuscript.query().insert({})

    const createThread = async () =>
      ChatThread.query().insert({
        chatType: 'reviewer',
        manuscriptId: manuscript.id,
      })

    await expect(createThread()).rejects.toThrow()
  })

  test('user id must not be null for curator chats', async () => {
    const manuscript = await Manuscript.query().insert({})

    const createThread = async () =>
      ChatThread.query().insert({
        chatType: 'curator',
        manuscriptId: manuscript.id,
      })

    await expect(createThread()).rejects.toThrow()
  })

  test('reviewer id must be null for author and science officer chats', async () => {
    const manuscript = await Manuscript.query().insert({})
    const user = await User.query().insert({})

    const createAuthorThread = async () =>
      ChatThread.query().insert({
        chatType: 'author',
        manuscriptId: manuscript.id,
        reviewerId: user.id,
      })

    const createSOThread = async () =>
      ChatThread.query().insert({
        chatType: 'scienceOfficer',
        manuscriptId: manuscript.id,
        reviewerId: user.id,
      })

    await expect(createAuthorThread()).rejects.toThrow()
    await expect(createSOThread()).rejects.toThrow()
  })

  test('does not create a new reviewer thread with an invalid reviewer id', async () => {
    const manuscript = await Manuscript.query().insert({})
    const userId = uuid()

    const createThread = async () =>
      ChatThread.query().insert({
        chatType: 'reviewer',
        manuscriptId: manuscript.id,
        reviewerId: userId,
      })

    await expect(createThread()).rejects.toThrow()
  })

  test('creates curator thread', async () => {
    const curator = await User.query().insert({})
    const manuscript = await Manuscript.query().insert({})
    let threads

    const findThreads = async () =>
      ChatThread.query().where({
        manuscriptId: manuscript.id,
        userId: curator.id,
        chatType: 'curator',
      })

    await ChatThread.createCuratorThread(manuscript.id, curator.id)

    threads = await findThreads()
    expect(threads).toHaveLength(1)

    // this time it already exists, so call should be ignored
    await ChatThread.createCuratorThread(manuscript.id, curator.id)

    threads = await findThreads()
    expect(threads).toHaveLength(1) // still 1
  })
})

const { v4: uuid } = require('uuid')

const {
  CuratorReview,
  Manuscript,
  ManuscriptVersion,
  User,
} = require('@pubsweet/models')

const clearDb = require('./helpers/clearDb')

const createReview = async () => {
  const user = await User.query().insert({})
  const manuscript = await Manuscript.query().insert({})
  const version = await ManuscriptVersion.query().insert({
    manuscriptId: manuscript.id,
  })

  return CuratorReview.query().insert({
    manuscriptVersionId: version.id,
    curatorId: user.id,
  })
}

const reviewData = {
  content: 'lorem',
  recommendation: 'revise',
  openAcknowledgement: true,
}

describe('Curator Review Model', () => {
  beforeAll(() => clearDb())
  afterEach(() => clearDb())

  afterAll(() => {
    const knex = CuratorReview.knex()
    knex.destroy()
  })

  test('Saves curator review', async () => {
    const review = await createReview()
    const saved = await CuratorReview.save(review.id, reviewData)

    expect(saved.content).toEqual('lorem')
    expect(saved.recommendation).toEqual('revise')
    expect(saved.openAcknowledgement).toEqual(true)
    expect(saved.submitted).toBeNull()
  })

  test('Does not save if review id does not exist', async () => {
    const savePromise = () => CuratorReview.save(uuid(), reviewData)
    await expect(savePromise()).rejects.toThrow()
  })

  test('Submits curator review', async () => {
    const review = await createReview()
    const submitted = await CuratorReview.submit(review.id, reviewData)

    expect(submitted.content).toEqual('lorem')
    expect(submitted.recommendation).toEqual('revise')
    expect(submitted.openAcknowledgement).toEqual(true)
    expect(submitted.submitted).toBeInstanceOf(Date)
  })

  test('Does not submit if review id does not exist', async () => {
    const submitPromise = () => CuratorReview.submit(uuid(), reviewData)
    await expect(submitPromise()).rejects.toThrow()
  })

  test('Does not submit if review is already submitted', async () => {
    const review = await createReview()
    await CuratorReview.submit(review.id, reviewData)

    const resubmitPromise = () => CuratorReview.submit(review.id, reviewData)
    await expect(resubmitPromise()).rejects.toThrow()
  })

  test('creates reviews for all versions of a manuscript', async () => {
    const curator = await User.query().insert({})
    const manuscript = await Manuscript.query().insert({})
    const versions = await ManuscriptVersion.query().insert([
      { manuscriptId: manuscript.id },
      { manuscriptId: manuscript.id },
    ])

    await CuratorReview.createReviewsForManuscript(manuscript.id, curator.id)

    const curatorReviews = await CuratorReview.query().where({
      curatorId: curator.id,
    })

    const reviewForFirstVersion = curatorReviews.find(
      r => r.manuscriptVersionId === versions[0].id,
    )

    const reviewForSecondVersion = curatorReviews.find(
      r => r.manuscriptVersionId === versions[1].id,
    )

    expect(curatorReviews).toHaveLength(2)
    expect(reviewForFirstVersion).toBeDefined()
    expect(reviewForSecondVersion).toBeDefined()
  })
})

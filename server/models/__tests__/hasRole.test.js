const { Model } = require('objection')

const { Manuscript, Team, User } = require('@pubsweet/models')

const hasRole = require('../_helpers/hasRole')
const clearDb = require('./helpers/clearDb')

describe('Has role helper', () => {
  beforeAll(() => clearDb())
  afterEach(() => clearDb())

  afterAll(() => {
    const knex = Model.knex()
    knex.destroy()
  })

  test('finds whether a user has a role on an object', async () => {
    const user = await User.query().insert({})
    const manuscript = await Manuscript.query().insert({})
    const team = await Team.query().insert({
      role: 'editor',
      objectId: manuscript.id,
      objectType: 'manuscript',
    })

    await Team.addMember(team.id, user.id)

    const result = await hasRole(user.id, manuscript.id, 'editor', false)
    expect(result).toBe(true)
  })

  test('finds whether a user has a global role', async () => {
    const user = await User.query().insert({})
    const team = await Team.query().insert({
      role: 'editors',
      global: true,
    })

    await Team.addMember(team.id, user.id)

    const result = await hasRole(user.id, null, 'editors', true)
    expect(result).toBe(true)
  })
})

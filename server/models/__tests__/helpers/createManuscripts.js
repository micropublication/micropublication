const { lorem } = require('faker')
const range = require('lodash/range')

const Manuscript = require('../../manuscript/manuscript.model')
const ManuscriptVersion = require('../../manuscriptVersion/manuscriptVersion.model')

const createManuscript = async () => {
  const manuscript = await Manuscript.query().insert({})

  await ManuscriptVersion.query().insert({
    manuscriptId: manuscript.id,
    title: lorem.words(10),
  })

  return manuscript
}

const createManuscripts = async n =>
  Promise.all(range(n).map(() => createManuscript()))

module.exports = {
  createManuscript,
  createManuscripts,
}

const clearDb = require('./clearDb')
const { createManuscript, createManuscripts } = require('./createManuscripts')
const { createUser, createUsers } = require('./createUsers')

module.exports = {
  clearDb,
  createManuscript,
  createManuscripts,
  createUser,
  createUsers,
}

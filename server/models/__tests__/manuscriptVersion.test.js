// const { v4: uuid } = require('uuid')

const {
  CuratorReview,
  Manuscript,
  ManuscriptVersion,
  Team,
  TeamMember,
  User,
} = require('@pubsweet/models')

const { clearDb, createManuscript, createUsers } = require('./helpers')

const { REVIEWER_STATUSES } = require('../../api/constants')

describe('Manuscript Version Model', () => {
  beforeAll(() => clearDb())
  afterEach(() => clearDb())

  afterAll(() => {
    const knex = ManuscriptVersion.knex()
    knex.destroy()
  })

  test('Creates new version', async () => {
    const manuscript = await Manuscript.query().insert({})
    const firstVersion = await ManuscriptVersion.query().insert({
      manuscriptId: manuscript.id,
    })

    const firstAuthorTeam = await Team.query().insert({
      name: `author-${firstVersion.id}`,
      objectId: firstVersion.id,
      objectType: 'manuscriptVersion',
      role: 'author',
    })

    // first reviewer team
    await Team.query().insert({
      name: `reviewer-${firstVersion.id}`,
      objectId: firstVersion.id,
      objectType: 'manuscriptVersion',
      role: 'reviewer',
    })

    // assign an author to first version
    const author = await User.query().insert({})
    await TeamMember.query().insert({
      teamId: firstAuthorTeam.id,
      userId: author.id,
    })

    // create curator team for manuscript and assign a curator
    const curatorTeam = await Team.query().insert({
      role: 'curator',
      objectId: manuscript.id,
      objectType: 'article',
    })
    const curator = await User.query().insert({})
    await TeamMember.query().insert({
      teamId: curatorTeam.id,
      userId: curator.id,
    })

    // There should now be a curator review for the first version
    const firstCuratorReview = await CuratorReview.query().findOne({
      manuscriptVersionId: firstVersion.id,
    })
    expect(firstCuratorReview).toBeDefined()

    await firstVersion.makeNewVersion()

    const versions = await ManuscriptVersion.query().where({
      manuscriptId: manuscript.id,
    })

    // new version should have been created
    expect(versions).toHaveLength(2)

    const secondVersion = versions.find(v => v.id !== firstVersion.id)

    const secondVersionTeams = await Team.query().where({
      objectId: secondVersion.id,
    })

    const secondAuthorTeam = secondVersionTeams.find(v => v.role === 'author')
    const secondReviewerTeam = secondVersionTeams.find(
      v => v.role === 'reviewer',
    )

    // new version should already have author and reviewer teams
    expect(secondVersionTeams).toHaveLength(2)
    expect(secondAuthorTeam).toBeDefined()
    expect(secondReviewerTeam).toBeDefined()

    const secondVersionAuthors = await TeamMember.query().where({
      teamId: secondAuthorTeam.id,
    })

    // new author team should have copied over authors from previous version
    expect(secondVersionAuthors).toHaveLength(1)
    expect(secondVersionAuthors[0].userId).toEqual(author.id)

    // there should now be a curator review for the second version
    const secondCuratorReview = await CuratorReview.query().findOne({
      manuscriptVersionId: secondVersion.id,
    })
    expect(secondCuratorReview).toBeDefined()
  })

  test('finds if a user has a role on a manuscript version', async () => {
    const user = await User.query().insert({})
    const manuscript = await Manuscript.query().insert({})
    const manuscriptVersion = await ManuscriptVersion.query().insert({
      manuscriptId: manuscript.id,
    })
    const team = await Team.query().insert({
      role: 'author',
      objectId: manuscriptVersion.id,
      objectType: 'manuscriptVersion',
    })

    await Team.addMember(team.id, user.id)

    const value = await manuscriptVersion.hasRole(user.id, 'author')
    expect(value).toBe(true)

    const valueStatic = await ManuscriptVersion.hasRole(
      user.id,
      manuscriptVersion.id,
      'author',
    )
    expect(valueStatic).toBe(true)
  })

  test('invites all allowed reviewers from pool', async () => {
    const users = await createUsers(4)

    const manuscript = await createManuscript()

    const version = await ManuscriptVersion.query()
      .patch({
        amountOfReviewers: 2,
      })
      .findOne({
        manuscriptId: manuscript.id,
      })
      .returning('*')

    const reviewerTeam = await Team.query().insert({
      role: 'reviewer',
      global: false,
      objectId: version.id,
      objectType: 'manuscriptVersion',
    })

    await version.$query().patch({
      reviewerPool: users.map(user => user.id),
    })

    const poolMembers = await TeamMember.query().insert(
      users.map(user => ({
        userId: user.id,
        teamId: reviewerTeam.id,
      })),
    )

    const findInvited = async () =>
      TeamMember.query().where({
        teamId: reviewerTeam.id,
        status: REVIEWER_STATUSES.invited,
      })

    let invited = await findInvited()
    expect(invited).toHaveLength(0)

    await version.inviteMaxReviewers()
    invited = await findInvited()
    expect(invited).toHaveLength(2)

    expect(invited).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ id: poolMembers[0].id }),
        expect.objectContaining({ id: poolMembers[1].id }),
      ]),
    )

    await poolMembers[1].$query().patch({
      status: REVIEWER_STATUSES.revoked,
    })

    await version.inviteMaxReviewers()
    invited = await findInvited()
    expect(invited).toHaveLength(2)

    expect(invited).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ id: poolMembers[0].id }),
        expect.objectContaining({ id: poolMembers[2].id }),
      ]),
    )
  })
})

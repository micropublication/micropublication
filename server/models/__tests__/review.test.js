const {
  Manuscript,
  ManuscriptVersion,
  Review,
  User,
} = require('@pubsweet/models')

const clearDb = require('./helpers/clearDb')

describe('ReviewModel', () => {
  beforeAll(() => clearDb())
  afterEach(() => clearDb())

  afterAll(() => {
    const knex = Review.knex()
    knex.destroy()
  })

  test('only one review per user per manuscript version', async () => {
    const manuscript = await Manuscript.query().insert({})
    const version = await ManuscriptVersion.query().insert({
      manuscriptId: manuscript.id,
    })
    const reviewer = await User.query().insert({})

    // first review should be fine
    await Review.query().insert({
      articleVersionId: version.id,
      reviewerId: reviewer.id,
    })

    // second review of the same user for the same version should fail
    const createDuplicate = () =>
      Review.query().insert({
        articleVersionId: version.id,
        reviewerId: reviewer.id,
      })

    await expect(createDuplicate()).rejects.toThrow()
  })
})

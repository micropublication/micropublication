const { Model } = require('objection')

const { Team } = require('@pubsweet/models')

const useTransaction = require('../_helpers/useTransaction')
const clearDb = require('./helpers/clearDb')

const createValidTeams = async trx =>
  Team.query(trx).insert([
    {
      role: 'globalCurator',
      global: true,
    },
    {
      role: 'globalSectionEditor',
      global: true,
    },
  ])

const createInvalidTeams = async trx => {
  // works
  await Team.query(trx).insert({
    role: 'globalCurator',
    global: true,
  })

  // fails
  await Team.query(trx).insert({
    role: 'globalCurator',
    global: false,
  })
}

describe('Use transaction', () => {
  beforeAll(() => clearDb())
  afterEach(() => clearDb())

  afterAll(() => {
    const knex = Model.knex()
    knex.destroy()
  })

  // No transaction. First one is created, even though the second one failed.
  test('does not use any transaction by default', async () => {
    const options = { passedTrxOnly: true }
    const withoutTrx = () => useTransaction(createInvalidTeams, options)
    await expect(withoutTrx()).rejects.toThrow()

    const teams = await Team.query()
    expect(teams.length).toEqual(1)
  })

  // Transaction used. Second one fails, first is rolled back as a result.
  test('can optionally use transaction by default', async () => {
    const withTrx = () => useTransaction(createInvalidTeams)
    await expect(withTrx()).rejects.toThrow()

    const teams = await Team.query()
    expect(teams.length).toEqual(0)

    const withTrxValid = () => useTransaction(createValidTeams)
    await withTrxValid()

    const teamsNow = await Team.query()
    expect(teamsNow.length).toEqual(2)
  })

  test('uses passed transaction if provided', async () => {
    const nesting = () =>
      useTransaction(async trx => {
        await Team.query(trx).insert({
          role: 'globalCurator',
          global: true,
        })

        // this will make the whole transaction fail
        await useTransaction(
          async nestedTrx =>
            Team.query(trx).insert({
              role: 'author',
              global: true, // ivalid option
            }),
          { trx },
        )
      })

    // Nothing will be created, as the inner `useTransaction` failed
    await expect(nesting()).rejects.toThrow()

    const teams = await Team.query()
    expect(teams.length).toEqual(0)

    await useTransaction(async trx => {
      await Team.query(trx).insert({
        role: 'globalCurator',
        global: true,
      })

      // uses trx passed from parent
      await useTransaction(
        async nestedTrx =>
          Team.query(trx).insert({
            role: 'globalSectionEditor',
            global: true,
          }),
        { trx },
      )
    })

    const newTeams = await Team.query()
    expect(newTeams.length).toEqual(2)
  })
})

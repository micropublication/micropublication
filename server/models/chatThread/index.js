const model = require('./chatThread.model')

module.exports = {
  model,
  modelName: 'ChatThread',
}

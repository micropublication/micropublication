CREATE UNIQUE INDEX only_one_author_chat_per_manuscript
ON chat_threads (manuscript_id, chat_type)
WHERE chat_type = 'author';

CREATE UNIQUE INDEX only_one_science_officer_chat_per_manuscript
ON chat_threads (manuscript_id, chat_type)
WHERE chat_type = 'scienceOfficer';

CREATE UNIQUE INDEX only_one_reviewer_chat_per_manuscript_per_reviewer
ON chat_threads (manuscript_id, chat_type, reviewer_id);

ALTER TABLE ONLY chat_threads
ADD CONSTRAINT reviewer_id_not_null_if_type_is_reviewer_null_otherwise
CHECK
(
  (chat_type = 'reviewer' AND reviewer_id IS NOT NULL)
  OR
  (chat_type != 'reviewer' AND reviewer_id IS NULL)
);

const logger = require('@pubsweet/logger')

/**
 * Make the reviewer id column more generic by renaming it to user id.
 * This will allow other roles to be used without having to add a new column.
 * (eg. curator id column)
 *
 * The same logic as with reviewer ids will apply:
 * If you add a user id, the idea is that you will have a unique thread
 * per user per manuscript.
 * So for the same manuscript, with 2 reviewers, you'll get two independent
 * threads, one for each reviewer. Same goes for curators.
 *
 * This is different to author threads, where there is a single author thread
 * per manuscript, not one for every author.
 */

exports.up = knex => {
  try {
    return knex.schema.table('chat_threads', table => {
      table.renameColumn('reviewer_id', 'user_id')
    })
  } catch (e) {
    logger.error(
      'Chat Thread: Rename reviewer id to user id: Migration failed!',
    )
    throw new Error(e)
  }
}

exports.down = knex => {
  try {
    knex.schema.table('chat_threads', table => {
      table.renameColumn('user_id', 'reviewer_id')
    })
  } catch (e) {
    logger.error(
      'Chat Thread: Rename reviewer id to user id: Reversing migration failed!',
    )
    throw new Error(e)
  }
}

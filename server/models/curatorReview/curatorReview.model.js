const { BaseModel, logger } = require('@coko/server')
const { Team } = require('@pubsweet/models')

// When running tests, pubsweet/models has not loaded this yet, so import explicitly
const { model: ManuscriptVersion } = require('../manuscriptVersion')

const {
  boolean,
  dateNullable,
  id,
  stringNullable,
} = require('../_helpers/types')

const baseMessage = 'Curator Model:'

class CuratorReview extends BaseModel {
  static get tableName() {
    return 'curatorReviews'
  }

  constructor(properties) {
    super(properties)
    this.type = 'curatorReview'
  }

  static get schema() {
    return {
      type: 'object',
      required: ['curatorId', 'manuscriptVersionId'],
      properties: {
        manuscriptVersionId: id,
        curatorId: id,

        content: stringNullable,
        recommendation: stringNullable,
        openAcknowledgement: boolean,

        submitted: dateNullable,
      },
    }
  }

  static async save(curatorReviewId, data, options = {}) {
    try {
      const { content, recommendation, openAcknowledgement } = data

      return this.query(options.trx)
        .patchAndFetchById(curatorReviewId, {
          content,
          recommendation,
          openAcknowledgement,
        })
        .throwIfNotFound()
    } catch (e) {
      logger.error(`${baseMessage} Save failed for review ${curatorReviewId}!`)
      throw new Error(e)
    }
  }

  static async submit(curatorReviewId, data, options = {}) {
    try {
      const { content, recommendation, openAcknowledgement } = data
      const review = await CuratorReview.query(options.trx)
        .findById(curatorReviewId)
        .throwIfNotFound()

      if (review.submitted) {
        throw new Error('Curator review already submitted!')
      }

      return this.query(options.trx).patchAndFetchById(curatorReviewId, {
        content,
        recommendation,
        openAcknowledgement,
        submitted: new Date(),
      })
    } catch (e) {
      logger.error(
        `${baseMessage} Submit failed for review ${curatorReviewId}!`,
      )
      throw new Error(e)
    }
  }

  /**
   * Creates a curator review for each version of this manuscript for this curator
   */
  static async createReviewsForManuscript(
    manuscriptId,
    curatorId,
    options = {},
  ) {
    const { trx } = options

    const versions = await ManuscriptVersion.query(trx).where({
      manuscriptId,
    })

    if (!versions || versions.length === 0) {
      throw new Error(
        `${baseMessage} Create curator reviews: No versions found for manuscript ${manuscriptId}`,
      )
    }

    await Promise.all(
      versions.map(async version => {
        const data = {
          curatorId,
          manuscriptVersionId: version.id,
        }

        const curatorReview = await this.query(trx).findOne(data)
        if (!curatorReview) await this.query(trx).insert(data)
      }),
    )
  }

  async $beforeDelete() {
    await Team.deleteAssociated(this.data.type, this.id)
  }
}

module.exports = CuratorReview

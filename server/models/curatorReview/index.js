const model = require('./curatorReview.model')

module.exports = {
  model,
  modelName: 'CuratorReview',
}

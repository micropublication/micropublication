CREATE TABLE curator_reviews (
    id UUID PRIMARY KEY,
    created TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp,
    updated TIMESTAMP WITH TIME ZONE,
    type TEXT NOT NULL,

    manuscript_version_id UUID NOT NULL references manuscript_versions,
    curator_id UUID NOT NULL references users,

    content TEXT,
    recommendation TEXT,
    open_acknowledgement BOOLEAN default FALSE,

    submitted TIMESTAMP WITH TIME ZONE
);

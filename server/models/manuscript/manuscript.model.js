const { BaseModel } = require('@coko/server')
const { Team } = require('@pubsweet/models')

const {
  arrayOfStrings,
  boolean,
  id,
  object,
  string,
} = require('../_helpers/types')

const hasRoleHelper = require('../_helpers/hasRole')

class Manuscript extends BaseModel {
  static get tableName() {
    return 'manuscripts'
  }

  constructor(properties) {
    super(properties)
    this.type = 'manuscript'
  }

  static get schema() {
    return {
      properties: {
        categories: arrayOfStrings,
        currentlyWith: id,
        data: object,
        dataType: string,
        dbReferenceId: string,
        doi: string,
        history: object,
        isDataTypeSelected: boolean,
        isInitiallySubmitted: boolean,
        pmcId: string,
        pmId: string,
        species: arrayOfStrings,
        submissionTypes: arrayOfStrings,
      },
    }
  }

  async $beforeDelete() {
    await Team.deleteAssociated(this.data.type, this.id)
  }

  static async hasRole(userId, manuscriptId, role) {
    return hasRoleHelper(userId, manuscriptId, role)
  }

  async hasRole(userId, role) {
    return hasRoleHelper(userId, this.id, role)
  }
}

module.exports = Manuscript

const { transaction } = require('objection')

const {
  Manuscript,
  ManuscriptVersion,
  Review,
  Team,
} = require('@pubsweet/models')
const logger = require('@pubsweet/logger')

/* 
  For each manuscript
    Make its first version
    Move relevant data to the version
    If manuscript's decision status is revise, make its second version as well
    Move relevant teams to the version
    Make related reviews point to the version
*/

const deriveDecision = decisionObject => {
  const { accepted, rejected, revise } = decisionObject
  if (accepted) return 'accept'
  if (rejected) return 'reject'
  if (revise) return 'revise'
  return null
}

const camelToKebab = str =>
  str
    .replace(/([a-z])([A-Z])/g, '$1 $2')
    .split(' ')
    .map(i => i.toLowerCase())
    .join('-')

exports.up = async knex => {
  try {
    await transaction(Manuscript.knex(), async trx => {
      const manuscripts = await Manuscript.query(trx)

      await Promise.all(
        manuscripts.map(async manuscript => {
          const {
            acknowledgements,
            authors,
            comments,
            decisionLetter,
            disclaimer,
            funding,
            geneExpression,
            id: manuscriptId,
            image,
            imageCaption,
            laboratory,
            methods,
            patternDescription,
            references,
            status,
            suggestedReviewer,
            title,
          } = manuscript

          const manuscriptVersionData = {
            acknowledgements,
            authors,
            comments,
            disclaimer,
            dataTypeFormData: {
              type: 'geneExpression',
              data: geneExpression,
            },
            // decisionLetter,
            funding,
            image,
            imageCaption,
            laboratory,
            methods,
            patternDescription,
            references,
            manuscriptId,
            suggestedReviewer,
            title,
          }

          const { decision } = status
          const decisionValue = deriveDecision(decision)

          if (decisionValue) manuscriptVersionData.decision = decisionValue
          if (decisionLetter)
            manuscriptVersionData.decisionLetter = decisionLetter

          // IF IT IS ACCEPTED / REJECTED, IS IT ACTIVE ??
          if (decisionValue !== 'revise') manuscriptVersionData.active = true

          const manuscriptVersion = await ManuscriptVersion.query(trx).insert(
            manuscriptVersionData,
          )

          const manuscriptTeams = await Team.query(trx)
            .whereIn('role', [
              'author',
              'reviewers',
              'reviewersInvited',
              'reviewersRejected',
              'reviewersAccepted',
            ])
            .andWhere({
              global: false,
              objectId: manuscriptId,
            })

          await Promise.all(
            manuscriptTeams.map(async team => {
              await team.$query(trx).patch({
                name: `${camelToKebab(team.role)}-${manuscriptVersion.id}`,
                objectId: manuscriptVersion.id,
                objectType: 'manuscriptVersion',
              })
            }),
          )

          if (decisionValue === 'revise') {
            await manuscriptVersion.makeNewVersion(trx)
          }

          const manuscriptReviews = await Review.query(trx).where({
            articleVersionId: manuscriptId,
          })

          await Promise.all(
            manuscriptReviews.map(async review => {
              await review.$query(trx).patch({
                articleVersionId: manuscriptVersion.id,
              })
            }),
          )
        }),
      )
    })
  } catch (error) {
    logger.error('Move existing manuscripts: Migration failed! Rolling back...')
    throw new Error(error)
  }
}

const { transaction } = require('objection')
const clone = require('lodash/clone')

const { Manuscript, ManuscriptVersion } = require('@pubsweet/models')
const logger = require('@pubsweet/logger')

exports.up = async knex => {
  try {
    await transaction(Manuscript.knex(), async trx => {
      const manuscripts = await Manuscript.query(trx)

      await Promise.all(
        manuscripts.map(async manuscript => {
          const status = clone(manuscript.status)
          const { scienceOfficer, submission } = status
          delete status.decision

          const isApprovedByScienceOfficer = scienceOfficer.approved
          const isInitiallySubmitted = submission.initial
          const isDataTypeSelected = submission.datatypeSelected
          const submitted = submission.full

          await manuscript.$query(trx).patch({
            isDataTypeSelected,
            isInitiallySubmitted,
          })

          const versions = await ManuscriptVersion.query(trx).where({
            manuscriptId: manuscript.id,
          })

          await Promise.all(
            versions.map(async version => {
              let submittedValue = true

              if (!submitted) submittedValue = false

              if (versions.length > 1 && versions.indexOf(version) !== 0) {
                const previousVersion = versions[versions.indexOf(version) - 1]
                if (previousVersion.decision === 'revise') {
                  submittedValue = false
                }
              }

              await version.$query(trx).patch({
                isApprovedByScienceOfficer,
                submitted: submittedValue,
              })
            }),
          )
        }),
      )
    })
  } catch (e) {
    logger.error(
      'Move manuscript status from json: Transaction failed! Rolling back...',
    )
    throw new Error(e)
  }
}

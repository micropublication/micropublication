const { transaction } = require('objection')

const { Manuscript, Team } = require('@pubsweet/models')
const logger = require('@pubsweet/logger')

exports.up = async knex => {
  try {
    await transaction(Manuscript.knex(), async trx => {
      const manuscripts = await Manuscript.query(trx)
      await Promise.all(
        manuscripts.map(async manuscript => {
          await Team.query(trx).insert({
            name: `section-editor-${manuscript.id}`,
            objectId: manuscript.id,
            objectType: 'article',
            role: 'sectionEditor',
          })
        }),
      )
    })
  } catch (e) {
    logger.error(
      'Add section editor team to manuscripts: Transaction failed! Rolling back...',
    )
    throw new Error(e)
  }
}

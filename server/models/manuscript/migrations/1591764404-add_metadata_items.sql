alter table only manuscripts

add column categories jsonb,
add column species jsonb,
add column submission_types jsonb,
add column pm_id text,
add column pmc_id text
;

const clone = require('lodash/clone')
const omit = require('lodash/omit')
const { transaction } = require('objection')

const { BaseModel, logger } = require('@coko/server')

const Team = require('../team/team.model')
const TeamMember = require('../teamMember/teamMember.model')
const hasRoleHelper = require('../_helpers/hasRole')
const useTransaction = require('../_helpers/useTransaction')
const { REVIEWER_STATUSES } = require('../../api/constants')

const baseMessage = 'Manuscript Version Model:'

const {
  arrayOfIds,
  arrayOfObjectsNullable,
  boolean,
  booleanNullable,
  id,
  integerPositive,
  object,
  objectNullable,
  string,
  stringNotEmpty,
  stringNullable,
} = require('../_helpers/types')

class ManuscriptVersion extends BaseModel {
  constructor(properties) {
    super(properties)
    this.type = 'manuscriptVersion'
  }

  static get tableName() {
    return 'manuscriptVersions'
  }

  static get schema() {
    return {
      type: 'object',
      required: ['manuscriptId'],
      properties: {
        manuscriptId: id,
        active: booleanNullable,
        submitted: boolean,
        isApprovedByScienceOfficer: booleanNullable,

        amountOfReviewers: integerPositive,
        isReviewerAutomationOn: boolean,
        reviewerPool: arrayOfIds,

        abstract: stringNullable,
        acknowledgements: stringNullable,
        authors: arrayOfObjectsNullable,
        comments: stringNullable,
        disclaimer: booleanNullable,
        funding: stringNullable,
        image: objectNullable,
        imageCaption: stringNullable,
        imageTitle: stringNullable,
        laboratory: objectNullable,
        methods: stringNullable,
        reagents: stringNullable,
        patternDescription: stringNullable,
        references: arrayOfObjectsNullable,
        suggestedReviewer: objectNullable,
        title: stringNullable,

        dataTypeFormData: object,

        decision: string,
        decisionLetter: stringNotEmpty,
      },
    }
  }

  /* 
    Transaction (trx) is optional.
    If no transaction is given then one is created.
    If one is given, then all queries will run in the context of the given
    transaction.
  */
  async makeNewVersion(trx) {
    const newVersionData = omit(clone(this), [
      'id',
      'created',
      'updated',
      'decision',
      'decisionLetter',
      'submitted',
      'isApprovedByScienceOfficer',
    ])

    newVersionData.active = true

    const makeChanges = async trxFinal => {
      await this.$query(trxFinal).patch({
        active: null,
      })

      // create new version
      const newVersion = await ManuscriptVersion.query(trxFinal).insert(
        newVersionData,
      )

      // add author and reviewer teams to the new version
      const teamsToCreate = [
        {
          name: `author-${newVersion.id}`,
          objectId: newVersion.id,
          objectType: 'manuscriptVersion',
          role: 'author',
        },
        {
          name: `reviewer-${newVersion.id}`,
          objectId: newVersion.id,
          objectType: 'manuscriptVersion',
          role: 'reviewer',
        },
      ]

      const newVersionTeams = await Team.query(trxFinal).insert(teamsToCreate)

      // copy over authors from previous version to new one
      const authorTeam = await Team.query(trxFinal).findOne({
        objectId: this.id,
        role: 'author',
      })

      const authorMembers = await TeamMember.query(trxFinal).where({
        teamId: authorTeam.id,
      })

      const newAuthorTeam = newVersionTeams.find(t => t.role === 'author')

      const membersToCreate = authorMembers.map(a => ({
        teamId: newAuthorTeam.id,
        userId: a.userId,
      }))

      await TeamMember.query(trxFinal).insert(membersToCreate)

      // if there is an assigned curator, create a new curator review for this version
      const curatorMembers = await TeamMember.query(trxFinal)
        .leftJoin('teams', 'team_members.teamId', 'teams.id')
        .where({
          'teams.role': 'curator',
          'teams.objectId': newVersion.manuscriptId,
        })

      // Undefined in pubsweet/models when run with jest
      // eslint-disable-next-line global-require
      const { CuratorReview } = require('@pubsweet/models')

      await Promise.all(
        curatorMembers.map(async curatorMember => {
          await CuratorReview.query(trxFinal).insert({
            curatorId: curatorMember.userId,
            manuscriptVersionId: newVersion.id,
          })
        }),
      )

      return newVersion
    }

    if (!trx) {
      try {
        const changes = await transaction(
          ManuscriptVersion.knex(),
          async trxNew => makeChanges(trxNew),
        )
        return changes
      } catch (e) {
        logger.error(
          `${baseMessage} Make new version: Transaction failed! Rolling back...`,
        )
        throw new Error(e)
      }
    } else {
      return makeChanges(trx)
    }
  }

  static async hasRole(userId, manuscriptVersionId, role) {
    return hasRoleHelper(userId, manuscriptVersionId, role)
  }

  async hasRole(userId, role) {
    return hasRoleHelper(userId, this.id, role)
  }

  /**
   * Finds how many reviewers can still be invited for this version (defined by
   * how many are alredy invited and what the max reviewer number is) and
   * invites them.
   *
   * Returns the updated team members.
   */
  async inviteMaxReviewers(options = {}) {
    // avoid circular dependency
    /* eslint-disable-next-line global-require */
    const { notify } = require('../../services')

    const MAX_REVIEWERS = this.amountOfReviewers

    return useTransaction(
      async trx => {
        const reviewerTeam = await Team.query(trx).findOne({
          objectId: this.id,
          role: 'reviewer',
        })

        const reviewerTeamMembers = await Promise.all(
          this.reviewerPool.map(reviewerId =>
            TeamMember.query(trx).findOne({
              teamId: reviewerTeam.id,
              userId: reviewerId,
            }),
          ),
        )

        const invitedAlready = reviewerTeamMembers.filter(m =>
          m.hasActiveInvitation(),
        )

        if (invitedAlready.length >= MAX_REVIEWERS) {
          logger.info(
            `${baseMessage} inviteMaxReviewers: Already at reviewer capacity. Not inviting anyone.`,
          )
          return []
        }

        const numberOfReviewersToInvite = MAX_REVIEWERS - invitedAlready.length

        const membersToInvite = reviewerTeamMembers
          .filter(m => m.canInvite())
          .slice(0, numberOfReviewersToInvite)

        const invitedMembers = await Promise.all(
          membersToInvite.map(member =>
            member.$query(trx).patch({
              status: REVIEWER_STATUSES.invited,
            }),
          ),
        )

        membersToInvite.forEach(member => {
          notify('reviewerInvited', {
            reviewerId: member.userId,
            versionId: this.id,
          })
        })

        return invitedMembers
      },
      { trx: options.trx },
    )
  }
}

module.exports = ManuscriptVersion

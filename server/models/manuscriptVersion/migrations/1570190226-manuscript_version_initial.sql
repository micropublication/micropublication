create type decision as enum ('accept', 'reject', 'revise');

create table manuscript_versions(
  id uuid primary key not null,
  created timestamp with time zone not null default current_timestamp,
  updated timestamp with time zone,
  type text not null,

  manuscript_id uuid not null references manuscripts,
  active boolean, -- true or null, to allow the constraint to work
  is_approved_by_science_officer boolean,

  acknowledgements text,
  authors jsonb,
  comments text,
  disclaimer boolean,
  funding text,
  image jsonb,
  image_caption text,
  laboratory jsonb,
  methods text,
  pattern_description text,
  "references" text,
  suggested_reviewer jsonb,
  title text,

  data_type_form_data jsonb,

  decision decision,
  decision_letter text,

  constraint only_one_active_version_per_manuscript unique(manuscript_id, active)
);

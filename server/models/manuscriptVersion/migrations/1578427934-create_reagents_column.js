exports.up = knex =>
  knex.schema.hasColumn('manuscript_versions', 'reagents').then(exists =>
    knex.schema.table('manuscript_versions', table => {
      if (!exists) {
        table.text('reagents')
      }
    }),
  )

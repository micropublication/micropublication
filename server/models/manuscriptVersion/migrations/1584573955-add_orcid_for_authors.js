const { transaction } = require('objection')
const { cloneDeep } = require('lodash')

const { ManuscriptVersion } = require('@pubsweet/models')
const logger = require('@pubsweet/logger')

exports.up = async knex => {
  try {
    await transaction(ManuscriptVersion.knex(), async trx => {
      const manuscriptVersions = await ManuscriptVersion.query(trx)

      await Promise.all(
        manuscriptVersions.map(async version => {
          const { authors } = version
          if (authors) {
            const modAuthors = authors.map(a => {
              const modAuthor = cloneDeep(a)
              modAuthor.orcid = ''
              return modAuthor
            })

            await version.$query(trx).patch({ authors: modAuthors })
          }
        }),
      )
    })
  } catch (error) {
    logger.error(
      'Add author orcid to manuscript versions: Migration failed! Rolling back...',
    )
    throw new Error(error)
  }
}

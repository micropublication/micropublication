alter table only manuscript_versions
add column reviewer_pool jsonb not null default '[]'::jsonb
;
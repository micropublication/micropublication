ALTER TABLE ONLY manuscript_versions
ADD COLUMN amount_of_reviewers SMALLINT NOT NULL DEFAULT 1
;
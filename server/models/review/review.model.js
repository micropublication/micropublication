const { BaseModel } = require('@coko/server')
const { Team } = require('@pubsweet/models')

class Review extends BaseModel {
  static get tableName() {
    return 'reviews'
  }

  constructor(properties) {
    super(properties)
    this.type = 'review'
  }

  static get schema() {
    return {
      type: 'object',
      required: ['articleVersionId', 'reviewerId'],
      properties: {
        articleVersionId: { type: 'string', format: 'uuid' },
        status: { type: 'object' },
        events: { type: 'object' },
        content: { type: 'string' },
        reviewerId: { type: 'string', format: 'uuid' },
        recommendation: { type: 'string' },
        openAcknowledgement: { type: 'boolean' },
        askedToSeeRevision: { type: 'boolean' },
        confidentialComments: { type: 'string' },
        reviseQualifier: { type: 'string' },
      },
    }
  }

  async $beforeDelete() {
    await Team.deleteAssociated(this.data.type, this.id)
  }
}

Review.type = 'review'
module.exports = Review

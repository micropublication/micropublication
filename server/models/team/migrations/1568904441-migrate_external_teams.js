const { Team, TeamMember, User } = require('@pubsweet/models')
const logger = require('@pubsweet/logger')

// Removes the word "external" and fixes camelCase
// eg. externalReviewersInvited -> reviewersInvited
const getCorrespondingType = type =>
  type
    .replace(/([a-z])([A-Z])/g, '$1 $2')
    .split(' ')
    .slice(1)
    .map((i, pos) => {
      if (pos === 0) return i.toLowerCase()
      return i
    })
    .join('')

const baseMessage = 'Migrate external teams:'
const tableDoesNotExistMessage =
  'Most likely table "external_team" does not exist'

exports.up = async knex => {
  let externalTeams = []

  try {
    // Only get external teams that actually have members
    externalTeams = await knex('external_team').where(
      knex.raw(`jsonb_array_length("members"::jsonb) > 0`),
    )
  } catch (e) {
    logger.warn(
      `${baseMessage} Failed to get external teams! ${tableDoesNotExistMessage}`,
      // e,
    )
  }

  await Promise.all(
    externalTeams.map(async team => {
      const type = getCorrespondingType(team.teamType)
      const objectId = team.manuscriptId
      const { members } = team

      try {
        const targetTeam = await Team.query().findOne({
          objectId,
          role: type,
        })

        if (!targetTeam)
          throw new Error(`${baseMessage} Target team does not exist!`)

        const data = members.map(member => ({
          teamId: targetTeam.id,
          userId: member,
        }))

        await Promise.all(
          data.map(async memberData => {
            const exists = await TeamMember.query().findOne(memberData)
            const userExists = await User.query().findById(memberData.userId)

            if (!exists && userExists && targetTeam)
              await TeamMember.query().insert(memberData)
          }),
        )

        await knex('external_team')
          .where({
            id: team.id,
          })
          .del()
      } catch (error) {
        logger.error(`${baseMessage} Team member insertion failed!`)
        throw new Error(error)
      }
    }),
  )
}

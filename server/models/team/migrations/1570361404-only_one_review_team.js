const { transaction } = require('objection')

const { Team, TeamMember } = require('@pubsweet/models')
const logger = require('@pubsweet/logger')

const baseMessage = 'Only one reviewer team migration:'

/* 
  Current implementation uses 4 different teams for reviewers.
  reviewer, reviewersInvited, reviewersAccepted, reviewersRejected
  Invited is a subset of reviewer.
  And accepted / rejected is a subset of invited (but mutually exclusive)

  All this creates a lot of unnecessary code (and a lot of unnecessary rows in
  the teamMember table - eg. for each accepted invitation, there are 3 rows:
  they're a member of reviewer, reviewerInvited and reviewerAccepted), 
  so this migration switches the above to a single "reviewer" team, with
  statuses: "notInvited", "invited", "acceptedInvitation", "rejectedInvitation".
*/

exports.up = async knex => {
  try {
    await transaction(Team.knex(), async trx => {
      // For each member of a reviewers team, set member status as 'notInvited'
      await TeamMember.query(trx)
        .whereIn('teamId', builder => {
          builder
            .select('id')
            .from('teams')
            .where({ role: 'reviewers' })
        })
        .patch({
          status: 'notInvited',
        })

      const baseQuery = () =>
        TeamMember.query(trx)
          .select('team_members.*', 'teams.objectId', 'teams.role')
          .leftJoin('teams', 'team_members.team_id', 'teams.id')

      const moveMembers = async membersToMove =>
        Promise.all(
          membersToMove.map(async member => {
            // Find reviewer team member, change its status, and clean up.
            const reviewersMember = await baseQuery().findOne({
              role: 'reviewers',
              objectId: member.objectId,
              userId: member.userId,
            })

            let status
            switch (member.role) {
              case 'reviewersInvited':
                status = 'invited'
                break

              case 'reviewersAccepted':
                status = 'acceptedInvitation'
                break

              case 'reviewersRejected':
                status = 'rejectedInvitation'
                break

              default:
                break
            }

            await reviewersMember.$query(trx).patch({ status })
            await TeamMember.query(trx).deleteById(member.id)
          }),
        )

      const invitedMembers = await baseQuery().where({
        role: 'reviewersInvited',
      })
      const acceptedMembers = await baseQuery().where({
        role: 'reviewersAccepted',
      })
      const rejectedMembers = await baseQuery().where({
        role: 'reviewersRejected',
      })

      /* 
        Execute these 3 separately and in order (as opposed to dumping them all
        together), to avoid 'accepted' / 'rejected' statuses being overwritten
        by 'invited' statuses.
      */
      await moveMembers(invitedMembers)
      await moveMembers(acceptedMembers)
      await moveMembers(rejectedMembers)

      await Team.query(trx)
        .delete()
        .whereIn('role', [
          'reviewersInvited',
          'reviewersAccepted',
          'reviewersRejected',
        ])

      await Team.query(trx)
        .where({ role: 'reviewers' })
        .patch({ role: 'reviewer' })
    })
  } catch (e) {
    logger.error(`${baseMessage} Migration failed! Rolling back...`)
    throw new Error(e)
  }
}

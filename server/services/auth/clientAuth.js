const { TeamMember, User } = require('@pubsweet/models')
const uniq = require('lodash/uniq')

const {
  TEAMS: ROLES,
  GLOBAL_TEAMS: TEAMS,
  REVIEWER_STATUSES,
} = require('../../api/constants')

const createClientAuth = async userId => {
  const user = await User.query().findById(userId)

  /* 
    Is user member of any global team
  */
  const globalMembership = await TeamMember.teamFromMemberQuery().where({
    global: true,
    userId,
  })

  const isGlobal = globalMembership.length > 0 || user.admin
  const isGlobalEditor = !!globalMembership.find(t => t.role === TEAMS.EDITORS)
  const isGlobalScienceOfficer = !!globalMembership.find(
    t => t.role === TEAMS.SCIENCE_OFFICERS,
  )
  const isGlobalCurator = !!globalMembership.find(
    t => t.role === TEAMS.GLOBAL_CURATOR,
  )
  const isGlobalSectionEditor = !!globalMembership.find(
    t => t.role === TEAMS.GLOBAL_SECTION_EDITOR,
  )

  /* 
    Manuscripts that the user is an author of
  */
  const isAuthor = await TeamMember.manuscriptIdsWithRole(ROLES.AUTHOR, userId)

  /*
    Manuscripts that the user is assigned to as a science officer
  */
  const isAssignedScienceOfficer = await TeamMember.manuscriptIdsWithRole(
    ROLES.SCIENCE_OFFICER,
    userId,
  )

  /*
    Manuscripts that the user is assigned to as a section editor
  */
  const isAssignedSectionEditor = await TeamMember.manuscriptIdsWithRole(
    ROLES.SECTION_EDITOR,
    userId,
  )

  /*
    Manuscripts that the user is assigned to as an editor
  */
  const isAssignedEditor = await TeamMember.manuscriptIdsWithRole(
    ROLES.EDITOR,
    userId,
  )

  /*
    Manuscripts that the user is assigned to as a curator
  */
  const isAssignedCurator = await TeamMember.manuscriptIdsWithRole(
    ROLES.CURATOR,
    userId,
  )

  /*
    Manuscripts (not versions) that the user has accepted an invitation
    to review
  */
  const acceptedReviewInvitationManuscripts = await TeamMember.manuscriptIdsWithRole(
    ROLES.REVIEWER,
    userId,
  )

  const isAcceptedReviewerForManuscript = uniq(
    acceptedReviewInvitationManuscripts,
  )

  const isAcceptedReviewerForVersion = await TeamMember.manuscriptVersionIdsWithRole(
    ROLES.REVIEWER,
    userId,
    REVIEWER_STATUSES.accepted,
  )

  return {
    isAcceptedReviewerForManuscript,
    isAcceptedReviewerForVersion,
    isAssignedCurator,
    isAssignedEditor,
    isAssignedSectionEditor,
    isAssignedScienceOfficer,
    isAuthor,
    isGlobal,
    isGlobalCurator,
    isGlobalEditor,
    isGlobalScienceOfficer,
    isGlobalSectionEditor,
  }
}

module.exports = createClientAuth

const { boss, db, logger } = require('@coko/server')

const reviewerInvited = require('./reviewerInvited')
const reviewerAccepted = require('./reviewerAccepted')

const mapper = {
  reviewerAccepted,
  reviewerInvited,
}

const schedule = (event, context) => {
  try {
    mapper[event](context)
  } catch (e) {
    logger.error(e)
  }
}

const stopJob = async jobName => {
  try {
    const query = `SELECT x.* FROM pgboss.job x WHERE name='${jobName}'`
    const res = await db.raw(query)

    if (!res || !res.rows || !res.rows[0]) {
      throw new Error('Job query went wrong:', res)
    }

    const job = res.rows[0]

    await boss.cancel(job.id)
  } catch (e) {
    logger.error('Stop job error:', e)
  }
}

module.exports = { schedule, stopJob }

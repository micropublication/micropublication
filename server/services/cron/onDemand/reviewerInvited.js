const { remindReviewerJob, revokeInvitationJob } = require('./_jobs')

// This will run on reviewer invitation
const runReviewerInvitedJobs = async context => {
  const { teamMemberId, userId, versionId } = context

  /**
   * If the reviewer hasn't accepted the invite after 5 days, send reminder email.
   * If the reviewer hasn't accepted the invite after 7 days, revoke invitation.
   * If the reviewer responds to the invitation, these jobs need to be removed.
   */

  /* REMINDER */

  const remindName = `reviewer-invitation-reminder-${teamMemberId}`

  await remindReviewerJob({
    name: remindName,
    isAccepted: false,
    teamMemberId,
    userId,
    versionId,
  })

  /* REVOKE INVITATION */

  const revokeName = `revoke-invitation-${teamMemberId}`

  await revokeInvitationJob({
    name: revokeName,
    teamMemberId,
    versionId,
  })
}

module.exports = runReviewerInvitedJobs

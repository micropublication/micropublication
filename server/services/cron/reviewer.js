const moment = require('moment')

const { cron } = require('@coko/server')

const notify = require('../notify')

const {
  TeamMember,
  Team,
  ManuscriptVersion,
  Review,
} = require('@pubsweet/models')

/**
 * Automates reminders to reviewers and editors when reviewers fail to take action.
 * - If the reviewer hasn't accepted the invite after 5 days, another invite notification is sent.
 * - If the reviewer hasn't accepted the invite after 7 days, they will be uninvited (currently just a notification to the editors telling them to un-invite).
 * - If the reviewer has accepted but didn't leave a review for 3 days after acceptance, a reminder will be sent to the reviewer.
 * - If the reviewer has accepted but didn't leave a review for 7 days after acceptance, they will be uninvited (again, just a notification to editors for now).
 */

const checkReviewers = async () => {
  const teams = await Team.query().where({ role: 'reviewer' })

  await teams.forEach(async team => {
    const { objectId: versionId, objectType } = team

    if (objectType !== 'manuscriptVersion') return
    const version = await ManuscriptVersion.query().where({ id: versionId })
    if (version.decision) {
      return
    }

    const reviews = await Review.query().where({ articleVersionId: versionId })
    if (reviews.some(review => review.status.submitted)) {
      return
    }

    const invitedReviewers = await TeamMember.query().where({
      status: 'invited',
      teamId: team.id,
    })

    invitedReviewers.forEach(reviewer => {
      const { updated, userId } = reviewer
      const isAccepted = false

      if (
        moment()
          .subtract(8, 'days')
          .isAfter(moment(updated))
      ) {
        return
      }

      if (
        moment(updated).isBetween(
          moment().subtract(6, 'days'),
          moment().subtract(5, 'days'),
        )
      ) {
        notify('reviewerInvited', {
          reviewerId: userId,
          versionId,
        })
        notify('remindReviewer', { userId, isAccepted, versionId })
      }
      if (
        moment(updated).isBetween(
          moment().subtract(8, 'days'),
          moment().subtract(7, 'days'),
        )
      ) {
        // TODO: Update team member to be unininvited
        notify('uninviteReviewer', { userId, isAccepted, versionId })
      }
    })

    const acceptedReviewers = await TeamMember.query().where({
      status: 'acceptedInvitation',
      teamId: team.id,
    })

    acceptedReviewers.forEach(reviewer => {
      const { updated, userId } = reviewer
      const isAccepted = true

      if (
        moment()
          .subtract(8, 'days')
          .isAfter(moment(updated))
      ) {
        return
      }

      if (
        moment(updated).isBetween(
          moment().subtract(4, 'days'),
          moment().subtract(3, 'days'),
        )
      ) {
        notify('remindReviewer', { userId, isAccepted, versionId })
      }
      if (
        moment(updated).isBetween(
          moment().subtract(8, 'days'),
          moment().subtract(7, 'days'),
        )
      ) {
        // TODO:  Update team member to be unininvited
        notify('uninviteReviewer', { userId, isAccepted, versionId })
      }
    })
  })
}

cron.schedule('0 0 * * *', () => {
  checkReviewers()
})

// /* Self-invoking function using pg-boss's schedule functions */

// ;(async () => {
//   /* eslint-disable-next-line global-require */
//   const { boss } = require('@coko/server')

//   let count = 0
//   let schedules

//   await boss.schedule('test', '* * * * *', { and: 'how' })

//   await boss.subscribe('test', async job => {
//     count += 1
//     console.log(job.data, 'count', count)

//     if (count >= 2) {
//       await boss.unschedule('test')
//       await boss.unsubscribe('test')
//       schedules = await boss.getSchedules()
//       console.log('new schedules', schedules)
//     }
//   })

//   schedules = await boss.getSchedules()
//   console.log('schedules', schedules)
// })()

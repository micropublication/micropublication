const manuscript = {
  authors: [
    {
      affiliations: 'University of Minnesota',
      credit: ['formalAnalysis', 'software'],
      email: null,
      name: 'Yanna Cen',
      submittingAuthor: null,
      WBId: 'WBPerson18694',
    },
    {
      affiliations: 'University of Chicago',
      credit: ['dataCuration'],
      email: null,
      name: 'Alec Barret',
      submittingAuthor: null,
      WBId: 'WBPerson15466',
    },
    {
      affiliations: 'University of Chicago',
      credit: ['software'],
      email: 'john@john.com',
      name: 'John A Bryden',
      submittingAuthor: true,
      WBId: 'WBPerson6903',
    },
  ],
  dataType: 'noDatatype',
  doi: '',
  funding: 'blah',
  geneExpression: {
    antibodyUsed: 'an antibody',
    backboneVector: {
      name: '',
      WBId: '',
    },
    coinjected: '',
    constructComments: '',
    constructionDetails: '',
    detectionMethod: 'antibody',
    dnaSequence: [
      {
        name: '',
        WBId: '',
      },
    ],
    expressionPattern: {
      name: 'some expression',
      WBId: '1',
    },
    fusionType: {
      name: '',
      WBId: '',
    },
    genotype: '',
    injectionConcentration: '',
    inSituDetails: '',
    integratedBy: {
      name: '',
      WBId: '',
    },
    observeExpression: {
      certainly: [
        {
          certainly: {
            name: '',
            WBId: '',
          },
          during: {
            name: '',
            WBId: '',
          },
          id: '2f6eabf1-9173-4804-83c7-11b24363cfbf',
          subcellularLocalization: {
            name: '',
            WBId: '',
          },
        },
      ],
      not: [
        {
          during: {
            name: 'soma',
            WBId: '67',
          },
          id: '24fb8b53-94f8-45c5-9c64-a85d0cb907c3',
          not: {
            name: '',
            WBId: '',
          },
          subcellularLocalization: {
            name: '',
            WBId: '',
          },
        },
      ],
      partially: [
        {
          during: {
            name: '',
            WBId: '',
          },
          id: '50ccad5c-ede1-4662-a75b-4b4b6529bd9e',
          partially: {
            name: '',
            WBId: '',
          },
          subcellularLocalization: {
            name: '',
            WBId: '',
          },
        },
      ],
      possibly: [
        {
          during: {
            name: '',
            WBId: '',
          },
          id: 'd41e58bb-ff2f-4c01-9f6a-ed85efdf86f0',
          possibly: {
            name: '',
            WBId: '',
          },
          subcellularLocalization: {
            name: '',
            WBId: '',
          },
        },
      ],
    },
    reporter: {
      name: '',
      WBId: '',
    },
    species: {
      name: 'Caenorhabditis brenneri',
      WBId: 'Caenorhabditis brenneri',
    },
    strain: '',
    transgeneName: '',
    transgeneUsed: [
      {
        name: 'Some',
        WBId: '',
      },
      {
        name: 'Some',
        WBId: '',
      },
    ],
    utr: {
      name: '',
      WBId: '',
    },
    variation: {
      name: '',
      WBId: '',
    },
  },
  id: 'cd1774ca-a1eb-40b0-ae62-492437ed8e81',
  image: { url: '/e2c1615bed8d94d4e69a5ae1c886aa70.png' },
  imageCaption: '<p>fdsfsd</p>',
  laboratory: { name: 'Jean-Claude Labbe', WBId: 'UM' },
  methods: '<p>Yello</p>',
  patternDescription: '<p>Hello there</p>',
  references: '<p>fdsfdsfsd</p>',
  title: '<p>titlez <em>mymy</em> <strong>here</strong><sup>there</sup></p>',
}

module.exports = manuscript

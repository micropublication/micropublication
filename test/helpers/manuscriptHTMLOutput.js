module.exports = /* html */ `
  <h1 data-id="title">
    <p>titlez <em>mymy</em> <strong>here</strong><sup>there</sup></p>
  </h1>

  <div data-id="author-section">
    <div data-id="author-names">
      <span data-id="author-name">Yanna Cen</span>, 
      <span data-id="author-name">Alec Barret</span>, 
      <span data-id="author-name">John A Bryden</span>
    </div>
    
    <div data-id="author-affiliations">
      <div data-id="author-affiliation">University of Minnesota</div>
      <div data-id="author-affiliation">University of Chicago</div>
    </div>
  </div>

  <div data-id="image-section">
    <figure>
      <img data-id="image" src="e2c1615bed8d94d4e69a5ae1c886aa70.png" />
      <figcaption>
        Figure 1. <p>fdsfsd</p>
      </figcaption>
    </figure>
  </div>

  <div data-id="description">
    <h2 data-id="description-header">
      Description
    </h2>

    <div data-id="description-content">
      <p>Hello there</p>
    </div>
  </div>

  <div data-id="methods">
    <h2 data-id="methods-header">
      Methods
    </h2>

    <div data-id="methods-content">
      <p>Yello</p>
    </div>
  </div>

  <div data-id="references">
    <h2 data-id="references-header">
      References
    </h2>

    <div data-id="references-content">
      <p>fdsfdsfsd</p>
    </div>
  </div>

  <div data-id="funding">
    <h2 data-id="funding-header">
      Funding
    </h2>

    <div data-id="funding-content">
      blah
    </div>
  </div>


  <div data-id="author-contributions-section">
    <span data-id="author-contributions-item">
        <span data-id="author-contributions-item-name">
            Yanna Cen:
        </span>

        <span data-id="author-contributions-item-credit-section">
            <span data-id="credit-section-item">
                Formal Analysis
            </span>
            ,
            <span data-id="credit-section-item">
                Software
            </span>
        </span>
    </span>
    ;
    <span data-id="author-contributions-item">
        <span data-id="author-contributions-item-name">
            Alec Barret:
        </span>

        <span data-id="author-contributions-item-credit-section">
            <span data-id="credit-section-item">
                Data Curation
            </span>
        </span>
    </span>
    ;
    <span data-id="author-contributions-item">
        <span data-id="author-contributions-item-name">
            John A Bryden:
        </span>

        <span data-id="author-contributions-item-credit-section">
            <span data-id="credit-section-item">
                Software
            </span>
        </span>
    </span>
  </div>


  <div data-id="copyright-section">
    <span data-id="copyright-header">
      Copyright
    </span>

    <span data-id="copyright-content">
      © 2019 by the authors. This is an open-access article distributed
      under the terms of the Creative Commons Attribution 4.0 International
      (CC BY 4.0) License, which permits unrestricted use, distribution, and
      reproduction in any medium, provided the original author and source are
      credited.
    </span>
  </div> 

`

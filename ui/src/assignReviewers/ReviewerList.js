import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd'

import { th } from '../_helpers'
import ReviewerRow from './ReviewerRow'

const portal = document.createElement('div')
// portal.classList.add('')
if (!document.body) throw new Error('body not ready for portal creation!')
document.body.appendChild(portal)

const Wrapper = styled.div`
  border: 1px solid gray;
  padding: 16px;

  > div:not(:last-child) {
    margin-bottom: 8px;
  }
`

const EmptyMessage = styled.div`
  color: ${th('colorTextPlaceholder')};
  font-size: ${th('fontSizeBaseSmall')};
  font-style: italic;
`

const Row = props => {
  const {
    reviewer,
    canInvite,
    onClickInvite,
    onClickRemove,
    onClickRevokeInvitation,
    provided,
    snapshot,
    showReviewerEmails,
  } = props

  const {
    displayName,
    email,
    invited,
    isSignedUp,
    acceptedInvitation,
    rejectedInvitation,
    invitationRevoked,
    id,
    reviewSubmitted,
  } = reviewer

  const { innerRef, draggableProps, dragHandleProps } = provided
  const { isDragging } = snapshot

  const handleClickInvite = () => {
    onClickInvite(id)
  }

  const handleClickRevoke = () => {
    onClickRevokeInvitation(id)
  }

  const handleClickRemove = () => {
    onClickRemove(id)
  }

  const child = (
    <ReviewerRow
      acceptedInvitation={acceptedInvitation}
      canInvite={canInvite}
      displayName={displayName}
      email={email}
      innerRef={innerRef}
      invitationRevoked={invitationRevoked}
      invited={invited}
      isDragging={isDragging}
      isSignedUp={isSignedUp}
      onClickInvite={handleClickInvite}
      onClickRemove={handleClickRemove}
      onClickRevokeInvitation={handleClickRevoke}
      rejectedInvitation={rejectedInvitation}
      reviewerId={id}
      reviewSubmitted={reviewSubmitted}
      showEmail={showReviewerEmails}
      {...draggableProps}
      {...dragHandleProps}
    />
  )

  if (!isDragging) return child
  return ReactDOM.createPortal(child, portal)
}

const ReviewerList = props => {
  const {
    canInviteMore,
    className,
    onClickInvite,
    onClickRemoveRow,
    onClickRevokeInvitation,
    onReorder,
    reviewers,
    showReviewerEmails,
  } = props

  const onDragEnd = result => {
    const { source, destination } = result
    if (!destination) return
    const list = reviewers.slice(0) // shallow clone of reviewers

    const [removed] = list.splice(source.index, 1)
    list.splice(destination.index, 0, removed)

    onReorder(list)
  }

  if (reviewers.length === 0)
    return (
      <Wrapper className={className}>
        <EmptyMessage>No reviewers have been added to the list</EmptyMessage>
      </Wrapper>
    )

  return (
    <DragDropContext onDragEnd={onDragEnd}>
      <Droppable droppableId="reviewer-list">
        {providedDroppable => (
          <Wrapper
            className={className}
            ref={providedDroppable.innerRef}
            {...providedDroppable.droppableProps}
          >
            {reviewers.map((reviewer, index) => (
              <Draggable
                draggableId={reviewer.id}
                index={index}
                key={reviewer.id}
              >
                {(providedDraggable, snapshotDraggable) => (
                  <Row
                    canInvite={canInviteMore}
                    onClickInvite={onClickInvite}
                    onClickRemove={onClickRemoveRow}
                    onClickRevokeInvitation={onClickRevokeInvitation}
                    provided={providedDraggable}
                    reviewer={reviewer}
                    showReviewerEmails={showReviewerEmails}
                    snapshot={snapshotDraggable}
                  />
                )}
              </Draggable>
            ))}
            {providedDroppable.placeholder}
          </Wrapper>
        )}
      </Droppable>
    </DragDropContext>
  )
}

ReviewerList.propTypes = {
  /** Whether more reviewers can be invited */
  canInviteMore: PropTypes.bool.isRequired,
  /** Function to run when "invite" is clicked on a row from the pool */
  onClickInvite: PropTypes.func.isRequired,
  /** Function to run when the "X" button is clicked on a row from the pool */
  onClickRemoveRow: PropTypes.func.isRequired,
  /** Function to run when "revoke invitation" is clicked on a row from the pool */
  onClickRevokeInvitation: PropTypes.func.isRequired,
  /** Function to run on reorder. Receives the new list as an argument. */
  onReorder: PropTypes.func.isRequired,
  /** List of data for reviewer. List is controlled via props, not state. Object shape defined in ReviewerRow component */
  reviewers: PropTypes.arrayOf(PropTypes.object),
  /** Show reviewer emails in every row */
  showReviewerEmails: PropTypes.bool,
}

ReviewerList.defaultProps = {
  reviewers: [],
  showReviewerEmails: false,
}

export default ReviewerList

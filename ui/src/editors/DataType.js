import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import * as yup from 'yup'

import { grid } from '../_helpers'
import { Button, Form, Select } from '../common'
import ValueList from './ValueList'

const Wrapper = styled.div`
  max-width: ${grid(35)};
`

// add margin if there is no value and the form will be displayed
const StyledValueList = styled(ValueList)`
  margin-bottom: ${props =>
    (props.data && props.data[0] && props.data[0].value && '0') || grid(1)};
`

const SubmitButton = styled(Button)`
  margin-top: ${grid(2)};
`

const options = [
  {
    label: 'Gene expression results',
    value: 'geneExpression',
    isDisabled: true,
  },
  {
    label: 'No Datatype',
    value: 'noDatatype',
  },
]

const initialValues = {
  dataType: '',
}

const validations = yup.object().shape({
  dataType: yup.string().required('You need to select a datatype'),
})

const findSelected = value => options.find(option => option.value === value)

const Datatype = props => {
  const {
    className,
    dataType,
    onClickSetDataType,
    showDataTypeSelection,
  } = props

  const valueListData = [
    {
      label: 'DATATYPE',
      status: 'primary',
      value: findSelected(dataType) && findSelected(dataType).label,
    },
  ]

  const handleSubmit = values => {
    const selected = findSelected(values.dataType)
    onClickSetDataType(selected)
  }

  return (
    <Wrapper className={className}>
      <StyledValueList data={valueListData} missingValueText="not set" />

      {!dataType && showDataTypeSelection && (
        <Form
          initialValues={initialValues}
          onSubmit={handleSubmit}
          validationSchema={validations}
        >
          {formProps => {
            const { isValid, setFieldValue, values } = formProps

            const onChange = valueObject => {
              if (!valueObject) setFieldValue('dataType', '')
              else setFieldValue('dataType', valueObject.value)
            }

            const selectValue = findSelected(values.dataType)

            return (
              <>
                <Select
                  isClearable
                  isSearchable={false}
                  name="dataType"
                  onChange={onChange}
                  options={options}
                  placeholder="Select datatype"
                  value={selectValue}
                />

                <SubmitButton disabled={!isValid} primary type="submit">
                  Set datatype
                </SubmitButton>
              </>
            )
          }}
        </Form>
      )}
    </Wrapper>
  )
}

Datatype.propTypes = {
  dataType: PropTypes.string,
  onClickSetDataType: PropTypes.func,
  showDataTypeSelection: PropTypes.bool,
}

Datatype.defaultProps = {
  dataType: null,
  onClickSetDataType: null,
  showDataTypeSelection: false,
}

export default Datatype

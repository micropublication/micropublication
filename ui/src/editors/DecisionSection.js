import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import styled, { css, ThemeContext, withTheme } from 'styled-components'
import * as yup from 'yup'
import { get } from 'lodash'

import { stripHTML } from '../_helpers'
import { Button, Form, PanelTextEditor, Radio, Status } from '../common'

const Wrapper = styled.div``

const Editor = styled(PanelTextEditor)`
  div[contenteditable] {
    ${props =>
      props.readOnly &&
      css`
        border-bottom: 0;
      `};
  }
`

const validations = yup.object().shape({
  decision: yup.string().required('You need to make a decision'),
  decisionLetter: yup
    .string()
    .test(
      'decision-letter-not-empty',
      'You need to write a decision letter',
      val => {
        if (!val) return false
        return stripHTML(val).length > 0
      },
    ),
})

const makeOptions = theme => [
  {
    color: theme.colorSuccess,
    label: 'Accept and send proofs',
    value: 'accept',
  },
  {
    color: theme.colorError,
    label: 'Decline',
    value: 'decline',
  },
  {
    color: theme.colorWarning,
    label: 'Revise',
    value: 'revise',
  },
  {
    color: theme.colorError,
    label: 'Reject',
    value: 'reject',
  },
  {
    color: theme.colorPrimary,
    label: 'Publish',
    value: 'publish',
  },
]

const DecisionSection = props => {
  const {
    articleTitle,
    articleUrl,
    authorName,
    getSavedDecision,
    decision,
    decisionLetter,
    finalizeDoi,
    proofLink,
    reviewExists,
    reviseQualifier,
    saveDecision,
    submitDecision,
    submitted,
  } = props

  const theme = useContext(ThemeContext)
  const radioOptions = makeOptions(theme)

  const savedDecision = getSavedDecision()

  const initialValues = {
    decision: decision || get(savedDecision, 'decision') || '',
    decisionLetter:
      decisionLetter || get(savedDecision, 'decisionLetter') || '',
    key: '',
  }

  const handleSubmit = (values, formikBag) => {
    submitDecision({
      decision: values.decision,
      decisionLetter: values.decisionLetter,
    })
    if (values.decision === 'publish') {
      finalizeDoi()
    }
  }

  const proofsLetter = `
    <p>
    Dear ${authorName},
    </p>
    <p>
    We are happy to let you know that your article has been accepted for publication. Congratulations!
    </p>
    <p>
    Please take a careful look at the pre-production proofs of your article: <a href="${proofLink}">proof download</a>.
    </p>
    <p>
    Please make sure there are no typos, errors or omissions in your article, including your title, author names, affiliations, 
    reagents, etc. in addition to your reported results. These are little things that if wrong will still require a separate 
    corrigendum article if they need correction after publication.
    </p>
    <p>
    Please make any change or approve the current version by following this <a href="${articleUrl}">link</a>.
    </p>
    <p>
    You will still get a chance to see the final version before publication.<br />
    Please return your corrections within 72 hours. If you are unable to return your corrections within 72 hours, let us know.<br />
    Do not hesitate to contact us if you have any questions.
    </p>
    <p>
    We look forward to publishing your work.
    </p>
    <p>
    Best wishes,
    </p>
    <p>
    The microPublication Editorial Team
    </p>
  `

  const rejectLetter = `
    <p>
    Dear ${authorName},
    </p>
    <p>
    We regret to inform you that your article "${stripHTML(
      articleTitle,
    )}" has been rejected following the reviewer's comments, see below.
    </p>
    <p>
    We are sorry not to have better news for you, however, we thank you for giving us the opportunity to consider your manuscript.
    </p>
    <p>
    Best wishes,
    </p>
    <p>
    The microPublication Editorial Team
    </p>
  `

  const reviseQualifierLetters = {
    'accept, with minor modifications to figure and/or text': `
      Your article ${stripHTML(articleTitle)}" has been accepted, with minor 
      modifications.
  `,

    'accept, with major modifications to figure and/or text': `
    Your article "${stripHTML(articleTitle)}" has been accepted, with major 
    modifications.
  `,

    'accept, with either an additional data/experiment or include a caveat concerning the conclusion given the missing information, as well as any additional minor or major modifications': `
    Your article "${stripHTML(articleTitle)}" has been accepted, with 
    additional data, which may entail further experiments. If gathering more 
    data or doing more experiments is not possible, please include a caveat 
    that the relevant conclusion is preliminary.
  `,

    'accept, with the addition of missing essential data; in the absence of this data, then reject': `
    Your article "${stripHTML(articleTitle)}" has been conditionally accepted 
    as it requires the addition of missing essential data.  If gathering more 
    data or doing more experiments is not possible, we will, unfortunately, 
    need to reject your article. 
  `,
  }

  const reviseLetter = reviewExists
    ? `
    <p>
    Dear ${authorName},
    </p>
    <p>
    ${reviseQualifierLetters[reviseQualifier]}
    </p>
    <p>
    ** list revision requests here **
    </p>
    <p>
    We kindly ask you to address each point and summarize your changes in the 
    'Comments to Editor' section on the platform. In order to expedite the 
    processing of your revised manuscript, please be as specific as possible 
    in your responses.
    </p>
    <p>
    The microPublication Editorial Team
    </p>
  `
    : `
    <p>
    Dear ${authorName},
    </p>
    <p>
    Thank you for submitting your manuscript "${stripHTML(
      articleTitle,
    )}" to microPublication Biology.
    </p>
    <p>
    Before sending your article out for peer-review we kindly ask you to address the following in the <a href="${articleUrl}">submission platform</a>:
    </p>
  `

  const declineLetter = `
      <p>
      Dear ${authorName},
      </p>
      <p>
      We regret to inform you we need to decline your manuscript "${stripHTML(
        articleTitle,
      )}".
      </p>
      <p>
      Sincerely,
      </p>
      <p>
      The microPublication  Editorial Team
      </p>
  `

  return (
    <Wrapper>
      <Form
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={validations}
      >
        {formProps => {
          const {
            errors,
            setFieldTouched,
            setFieldValue,
            touched,
            values,
          } = formProps

          saveDecision &&
            saveDecision({
              decision: values.decision,
              decisionLetter: values.decisionLetter,
            })

          const decisionStatus =
            decision === 'accept' ? 'accept and send proofs' : decision

          const defaultLetter = value => {
            setFieldValue('key', value)
            if (value === 'accept') {
              setFieldValue('decisionLetter', proofsLetter)
            } else if (value === 'publish') {
              setFieldValue(
                'decisionLetter',
                "E-Mail is currently disabled when the publish decision is selected. Don't forget to generate the DOI.",
              )
            } else if (value === 'revise') {
              setFieldValue('decisionLetter', reviseLetter)
            } else if (value === 'reject') {
              setFieldValue('decisionLetter', rejectLetter)
            } else if (value === 'decline') {
              setFieldValue('decisionLetter', declineLetter)
            }
          }

          return (
            <>
              {submitted && (
                <>
                  <Status status="primary">decision submitted</Status>: &nbsp;
                  <Status status={decision}>{decisionStatus}</Status>
                </>
              )}

              {!submitted && (
                <Radio
                  error={errors.decision}
                  handleChange={defaultLetter}
                  inline
                  label="Decision"
                  name="decision"
                  options={radioOptions}
                  readOnly={submitted}
                  required={!submitted}
                  setFieldValue={setFieldValue}
                  touched={touched}
                  values={values}
                />
              )}

              <Editor
                error={errors.decisionLetter}
                key={values.key}
                label="Decision letter"
                link
                name="decisionLetter"
                placeholder="Make some comments to the author"
                readOnly={submitted}
                required={!submitted}
                setFieldTouched={setFieldTouched}
                setFieldValue={setFieldValue}
                touched={touched}
                value={values.decisionLetter}
              />

              {!submitted && (
                <Button primary type="submit">
                  Send to Author
                </Button>
              )}
            </>
          )
        }}
      </Form>
    </Wrapper>
  )
}

DecisionSection.propTypes = {
  /** Title of article */
  articleTitle: PropTypes.string,
  /** Url for the article */
  articleUrl: PropTypes.string,
  /** Name of submitting author */
  authorName: PropTypes.string,
  /** Decision made by the editors */
  decision: PropTypes.oneOf(['accept', 'decline', 'reject', 'revise']),
  /** Decision letter text content */
  decisionLetter: PropTypes.string,
  /** Function to publish the DOI metadata at DataCite */
  finalizeDoi: PropTypes.func,
  /** Fetch saved (but not submitted) decision */
  getSavedDecision: PropTypes.func,
  /** Link to proof */
  proofLink: PropTypes.string,
  /** Whether a review has been submitted */
  reviewExists: PropTypes.bool,
  /** The qualifer for review revise decision */
  reviseQualifier: PropTypes.string,
  /** Save decision without submitting */
  saveDecision: PropTypes.func,
  /** Submit decision */
  submitDecision: PropTypes.func,
  /** Whether this decision is submitted or not */
  submitted: PropTypes.bool,
}

DecisionSection.defaultProps = {
  articleTitle: null,
  articleUrl: null,
  authorName: null,
  decision: null,
  decisionLetter: null,
  finalizeDoi: null,
  getSavedDecision: null,
  proofLink: null,
  reviewExists: false,
  reviseQualifier: null,
  saveDecision: null,
  submitDecision: null,
  submitted: false,
}

export default withTheme(DecisionSection)

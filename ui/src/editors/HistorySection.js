import React from 'react'
import PropTypes from 'prop-types'

import ValueList from './ValueList'

const HistorySection = props => {
  const { history } = props
  const received = history.received
    ? new Date(history.received).toLocaleDateString()
    : null
  const sentForReview = history.sentForReview
    ? new Date(history.sentForReview).toLocaleDateString()
    : null
  const reviewReceived = history.reviewReceived
    ? new Date(history.reviewReceived).toLocaleDateString()
    : null
  const revisionReceived = history.revisionReceived
    ? new Date(history.revisionReceived).toLocaleDateString()
    : null
  const accepted = history.accepted
    ? new Date(history.accepted).toLocaleDateString()
    : null
  const published = history.published
    ? new Date(history.published).toLocaleDateString()
    : null

  const data = [
    {
      label: 'Received',
      status: 'primary',
      value: received,
    },
    {
      label: 'Sent For Review',
      status: 'primary',
      value: sentForReview,
    },
    {
      label: 'Review Received',
      status: 'primary',
      value: reviewReceived,
    },
    {
      label: 'Revision Received',
      status: 'primary',
      value: revisionReceived,
    },
    {
      label: 'Accepted',
      status: 'primary',
      value: accepted,
    },
    {
      label: 'Published',
      status: 'primary',
      value: published,
    },
  ]

  return <ValueList data={data} />
}

HistorySection.propTypes = {
  history: PropTypes.shape({
    received: PropTypes.string,
    sentForReview: PropTypes.string,
    reviewReceived: PropTypes.string,
    revisionReceived: PropTypes.string,
    accepted: PropTypes.string,
    published: PropTypes.string,
  }),
}

HistorySection.defaultProps = {
  history: null,
}

export default HistorySection

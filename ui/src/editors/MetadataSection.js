import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import * as yup from 'yup'

import { grid, th } from '../_helpers'
import { Button, Form, Select, TextField } from '../common'
import ValueList from './ValueList'

const Wrapper = styled.div``

const DoiWrapper = styled.div`
  display: flex;
  flex-direction: row;
`

const MetadataValues = styled(ValueList)`
  margin-bottom: ${grid(2)};
`

const StyledSelect = styled(Select)`
  margin-bottom: ${grid(2)};
`

const Label = styled.label`
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  padding: 10px 10px 0 0;
`

const FieldWrapper = styled.div`
  display: flex;
  flex-direction: row;
`

const StyledField = styled(TextField)`
  margin-right: calc(${th('gridUnit')} * 3);
  width: calc(${th('gridUnit')} * 35);
`

const categoryOptions = [
  {
    label: 'Phenotype Data',
    value: 'phenotype data',
  },
  {
    label: 'Expression Data',
    value: 'expression data',
  },
  {
    label: 'Genotype Data',
    value: 'genotype data',
  },
  {
    label: 'Methods',
    value: 'methods',
  },
  {
    label: 'Software',
    value: 'software',
  },
  {
    label: 'Database Updates',
    value: 'database updates',
  },
  {
    label: 'Genetic Screens',
    value: 'genetic screens',
  },
  {
    label: 'Integrations',
    value: 'integrations',
  },
  {
    label: 'Interaction Data',
    value: 'interaction data',
  },
  {
    label: 'Models of Human Disease',
    value: 'models of human disease',
  },
  {
    label: 'Phylogenetic Data',
    value: 'phylogenetic data',
  },

  {
    label: 'Science and Society',
    value: 'science and society',
  },
]

const speciesOptions = [
  {
    label: 'Arabidopsis',
    value: 'arabidopsis',
  },
  { label: 'C. elegans', value: 'c. elegans' },
  {
    label: 'Drosophila',
    value: 'drosophila',
  },
  {
    label: 'Mouse',
    value: 'mouse',
  },
  {
    label: 'S. cerevisiae',
    value: 's. cerevisiae',
  },
  {
    label: 'S. pombe',
    value: 's. pombe',
  },
  {
    label: 'Universal',
    value: 'universal',
  },
  {
    label: 'Xenopus',
    value: 'xenopus',
  },
  {
    label: 'Zebrafish',
    value: 'zebrafish',
  },
]

const submissionTypesOptions = [
  {
    label: 'New Finding',
    value: 'new finding',
  },
  {
    label: 'New Methods',
    value: 'new methods',
  },
  {
    label: 'Materials and Reagents',
    value: 'materials and reagents',
  },
  {
    label: 'Negative Result',
    value: 'negative result',
  },
  {
    label: 'Software',
    value: 'software',
  },
  {
    label: 'Commodity Validation',
    value: 'commodity validation',
  },
  {
    label: 'Corrigendum',
    value: 'corrigendum',
  },
  {
    label: 'Data Updates',
    value: 'data updates',
  },
  {
    label: 'Erratum',
    value: 'erratum',
  },
  {
    label: 'Findings Previously Not Shown',
    value: 'findings previously not shown',
  },
  {
    label: 'Replication Successful',
    value: 'replication successful',
  },
  {
    label: 'Replication Unsuccessful',
    value: 'replication unsuccessful',
  },
  {
    label: 'Retracted',
    value: 'retracted',
  },
  {
    label: 'Retraction',
    value: 'retraction',
  },
]

const validations = yup.object().shape({
  dbReferenceId: yup.string(),
  doi: yup.string(),
  categories: yup.array().of(yup.string()),
  pmcId: yup.string(),
  pmId: yup.string(),
  speciesOptions: yup.array().of(yup.string()),
  submissionTypes: yup.array().of(yup.string()),
})

const MetadataSection = props => {
  const {
    categories,
    createDoi,
    dbReferenceId,
    doi,
    pmcId,
    pmId,
    species,
    submissionTypes,
    updateMetadata,
  } = props

  const [showForm, setShowForm] = useState(false)
  const [fetchDoi, setFetchDoi] = useState(false)

  /**
   * Either value list
   */
  const data = [
    { label: 'Species', status: 'primary', value: species },
    {
      label: 'Categories',
      status: 'primary',
      value: categories,
    },
    {
      label: 'Submission Types',
      status: 'primary',
      value: submissionTypes,
    },
    {
      label: 'DOI',
      status: 'primary',
      value: doi,
    },
    { label: 'PMC ID', status: 'primary', value: pmcId },
    { label: 'PubMed ID', status: 'primary', value: pmId },
    {
      label: 'DB Reference ID',
      status: 'primary',
      value: dbReferenceId,
    },
  ]

  if (!showForm)
    return (
      <Wrapper>
        <MetadataValues data={data} />

        <div>
          <Button onClick={() => setShowForm(true)} primary>
            Edit
          </Button>
        </div>
      </Wrapper>
    )

  /**
   * Or form
   */

  const handleSubmit = (values, formikBag) => {
    updateMetadata(values).then(() => setShowForm(false))
  }

  const initialValues = {
    dbReferenceId: dbReferenceId || '',
    doi: doi || '',
    categories: categories || [],
    pmId: pmId || '',
    pmcId: pmcId || '',
    species: species || [],
    submissionTypes: submissionTypes || [],
  }

  return (
    <Form
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={validations}
    >
      {formProps => {
        const {
          errors,
          handleBlur,
          handleChange,
          setFieldError,
          setFieldValue,
          setFieldTouched,
          touched,
          values,
        } = formProps

        const currentValues = (items, options) =>
          items
            ? items.map(value => options.find(option => option.value === value))
            : []

        const currentCategoriesValues = currentValues(
          values.categories,
          categoryOptions,
        )

        const currentSpeciesValues = currentValues(
          values.species,
          speciesOptions,
        )

        const currentSubmissionTypesValues = currentValues(
          values.submissionTypes,
          submissionTypesOptions,
        )

        const handleSelect = (type, newValues) => {
          const selectValues = newValues.map(item => item.value)
          setFieldValue(type, selectValues)
        }

        const handleCategories = newValues =>
          handleSelect('categories', newValues)
        const handleSpecies = newValues => handleSelect('species', newValues)
        const handleSubmissionTypes = newValues =>
          handleSelect('submissionTypes', newValues)

        const generateDoi = async () => {
          setFetchDoi(true)
          await createDoi()
            .then(newDoi => {
              setFieldValue('doi', newDoi)
              values.doi = newDoi
              updateMetadata(values)
            })
            .catch(error => {
              setFieldError('doi', `Generate DOI Failed: ${error}`)
              setFieldTouched('doi', true, false)
            })
            .finally(() => {
              setFetchDoi(false)
            })
        }

        return (
          <Wrapper>
            <Label>Species</Label>
            <StyledSelect
              closeMenuOnSelect={false}
              isMulti
              name="species"
              onBlur={handleBlur}
              onChange={handleSpecies}
              options={speciesOptions}
              placeholder="Select Species"
              value={currentSpeciesValues}
            />

            <Label>Categories</Label>
            <StyledSelect
              closeMenuOnSelect={false}
              isMulti
              name="categories"
              onBlur={handleBlur}
              onChange={handleCategories}
              options={categoryOptions}
              placeholder="Select Categories"
              value={currentCategoriesValues}
            />

            <Label>Submission Types</Label>
            <StyledSelect
              closeMenuOnSelect={false}
              isMulti
              name="submissionTypes"
              onBlur={handleBlur}
              onChange={handleSubmissionTypes}
              options={submissionTypesOptions}
              placeholder="Select Submission Types"
              value={currentSubmissionTypesValues}
            />

            <DoiWrapper>
              <TextField
                error={errors.doi}
                handleBlur={handleBlur}
                handleChange={handleChange}
                label="DOI"
                name="doi"
                touched={touched}
                value={values.doi}
              />
              <Button loading={fetchDoi} onClick={() => generateDoi()}>
                Generate DOI
              </Button>
            </DoiWrapper>

            <FieldWrapper>
              <StyledField
                error={errors.pmcId}
                handleBlur={handleBlur}
                handleChange={handleChange}
                label="PMC ID"
                name="pmcId"
                touched={touched}
                value={values.pmcId}
              />
              <StyledField
                error={errors.pmId}
                handleBlur={handleBlur}
                handleChange={handleChange}
                label="PubMed ID"
                name="pmId"
                touched={touched}
                value={values.pmId}
              />
            </FieldWrapper>

            <TextField
              error={errors.dbReferenceId}
              handleBlur={handleBlur}
              handleChange={handleChange}
              label="DB Reference ID"
              name="dbReferenceId"
              touched={touched}
              value={values.dbReferenceId}
            />

            <Button onClick={() => setShowForm(false)}>Cancel</Button>

            <Button primary type="submit">
              Save
            </Button>
          </Wrapper>
        )
      }}
    </Form>
  )
}

MetadataSection.propTypes = {
  categories: PropTypes.arrayOf(PropTypes.string),
  createDoi: PropTypes.func.isRequired,
  dbReferenceId: PropTypes.string,
  doi: PropTypes.string,
  pmcId: PropTypes.string,
  pmId: PropTypes.string,
  species: PropTypes.arrayOf(PropTypes.string),
  submissionTypes: PropTypes.arrayOf(PropTypes.string),
  updateMetadata: PropTypes.func.isRequired,
}

MetadataSection.defaultProps = {
  categories: null,
  dbReferenceId: null,
  doi: null,
  pmcId: null,
  pmId: null,
  species: null,
  submissionTypes: null,
}

export default MetadataSection

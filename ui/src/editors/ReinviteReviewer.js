import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { grid, th } from '../_helpers'
import { DiscreetButton } from '../common'
import RecommendationDot from './RecommendationDot'

const RowWrapper = styled.div`
  align-items: center;
  border-bottom: 1px solid ${th('colorFurniture')};
  cursor: default;
  display: flex;
  justify-content: space-between;
  margin: ${grid(1)} 0;
  transition: border-color 0.1s ease-in;

  &:hover {
    border-color: ${th('colorPrimary')};
  }
`

const LeftSide = styled.div`
  display: flex;
`

const Dot = styled(RecommendationDot)`
  margin-right: ${grid(1)};
`

const Name = styled.span`
  transition: all 0.1s ease-in;
`

const ReinviteReviewer = props => {
  const { displayName, id, onClickInvite, recommendation } = props

  return (
    <RowWrapper>
      <LeftSide>
        <Dot recommendation={recommendation} />
        <Name>{displayName}</Name>
      </LeftSide>

      <DiscreetButton onClick={() => onClickInvite(id)}>
        reinvite
      </DiscreetButton>
    </RowWrapper>
  )
}

ReinviteReviewer.propTypes = {
  displayName: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  onClickInvite: PropTypes.func.isRequired,
  recommendation: PropTypes.oneOf(['accept', 'reject', 'revise']).isRequired,
}

export default ReinviteReviewer

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { grid } from '../_helpers'
import ReinviteReviewer from './ReinviteReviewer'

const Header = styled.div`
  font-weight: bold;
  margin-bottom: ${grid(1)};
`

const ReinviteReviewerList = props => {
  const { onClickInvite, reviewers } = props

  return (
    <div>
      <Header>Reviewers from older versions</Header>

      {(!reviewers || reviewers.length === 0) && (
        <span>
          There are either no reviewers from previous versions or all previous
          reviewers are already invited.
        </span>
      )}

      {reviewers.map(reviewer => (
        <ReinviteReviewer
          displayName={reviewer.displayName}
          id={reviewer.id}
          onClickInvite={() => onClickInvite(reviewer.id)}
          recommendation={reviewer.recommendation}
        />
      ))}
    </div>
  )
}

ReinviteReviewerList.propTypes = {
  onClickInvite: PropTypes.func.isRequired,
  reviewers: PropTypes.arrayOf(
    PropTypes.shape({
      displayName: PropTypes.string,
      id: PropTypes.string,
      recommendation: PropTypes.oneOf(['accept', 'reject', 'revise']),
    }),
  ),
}

ReinviteReviewerList.defaultProps = {
  reviewers: [],
}

export default ReinviteReviewerList

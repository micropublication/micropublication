import React from 'react'

import InfoModal from './InfoModal'

const SubmissionSuccessModal = props => (
  <InfoModal headerText="successful submission" {...props}>
    Your microPublication was successfully submitted!
  </InfoModal>
)

export default SubmissionSuccessModal

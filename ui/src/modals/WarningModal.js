import React from 'react'

import DialogModal from './DialogModal'

const WarningModal = props => <DialogModal {...props} />

export default WarningModal

import React from 'react'
import PropTypes from 'prop-types'
import styled, { withTheme } from 'styled-components'
import { debounce, get, isEqual } from 'lodash'
import * as yup from 'yup'

import {
  Button,
  Checkbox,
  DiscreetButton,
  Form,
  Radio,
  Status,
  TextEditor,
} from '../common'
import { stripHTML } from '../_helpers'

const Wrapper = styled.div``

const ChatWrapper = styled.div`
  /* align-items: flex-end; */
  display: flex;
  justify-content: flex-end;
`

const Editor = styled(TextEditor)`
  width: 100%;
`

const StyledRadio = styled(Radio)`
  width: 100%;
`

const validations = yup.object().shape({
  content: yup
    .string()
    .test('content-not-empty', 'Cannot leave review empty', val => {
      if (!val) return false
      return stripHTML(val).length > 0
    }),
  recommendation: yup.string().required('You need to make a recommendation'),
  openAcknowledgement: yup.boolean(),
  askedToSeeRevision: yup.boolean(),
  confidentialComments: yup.string(),
})

/* eslint-disable react/prop-types */
class AutoSave extends React.Component {
  componentWillReceiveProps(nextProps, nextContext) {
    if (!isEqual(nextProps.values, this.props.values)) {
      this.save()
    }
  }

  save = debounce(() => {
    const { values } = this.props
    Promise.resolve(this.props.onSave(values))
  }, 500)

  render() {
    return null
  }
}
/* eslint-enable react/prop-types */

const ReviewerPanel = props => {
  const {
    canStillReview,
    dbReferenceId,
    decision,
    doi,
    makeOptions,
    onClickChat,
    openAcknowledgementLabel,
    review,
    reviewTextAreaLabel,
    reviewTextAreaPlaceholder,
    save,
    showConfidentialCommentsEditor,
    showRequestToSeeRevision,
    showReviewerGuidelines,
    submit,
    submitted,
    theme,
  } = props
  const decisionExists = !!decision

  if (!canStillReview && !review.content)
    return (
      <Wrapper>
        <Status status="error">
          You did not submit a review for this version
        </Status>
      </Wrapper>
    )

  const recommendation =
    get(review, 'recommendation') === 'revise'
      ? get(review, 'reviseQualifier')
      : get(review, 'recommendation')

  const initialValues = {
    content: get(review, 'content') || '',
    confidentialComments: get(review, 'confidentialComments') || '',
    recommendation: recommendation || '',
    openAcknowledgement: get(review, 'openAcknowledgement') || false,
  }

  if (showRequestToSeeRevision) {
    initialValues.askedToSeeRevision =
      get(review, 'askedToSeeRevision') || false
  }

  const handleSubmit = values => submit(values)
  const radioOptions = makeOptions(theme)

  return (
    <Wrapper>
      <ChatWrapper>
        {showReviewerGuidelines && (
          <DiscreetButton onClick={showReviewerGuidelines}>
            reviewer guidelines
          </DiscreetButton>
        )}
        <DiscreetButton onClick={onClickChat}>
          chat with the editors
        </DiscreetButton>
      </ChatWrapper>

      {submitted && (
        <div>
          <Status status="primary">Submitted</Status>
        </div>
      )}

      {dbReferenceId && (
        <div>
          <Status>
            db reference id: <Status status="primary">{dbReferenceId}</Status>
          </Status>
        </div>
      )}

      {doi && (
        <div>
          <Status>
            doi: <Status status="primary">{doi}</Status>
          </Status>
        </div>
      )}

      {submitted && decisionExists && (
        <Status>
          editor decision: <Status status={decision}>{decision}</Status>
        </Status>
      )}

      <Form
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={validations}
      >
        {formProps => {
          const {
            errors,
            handleChange,
            setFieldTouched,
            setFieldValue,
            touched,
            values,
          } = formProps

          const setRecommendation = (_, value) => {
            if (!['reject', 'accept'].includes(value)) {
              setFieldValue('reviseQualifier', value)
              setFieldValue('recommendation', 'revise')
            } else {
              setFieldValue('reviseQualifier', '')
              setFieldValue('recommendation', value)
            }
          }

          return (
            <>
              <StyledRadio
                error={errors.recommendation}
                label="Recommendation to the Editors"
                name="recommendation"
                options={radioOptions}
                readOnly={submitted}
                required
                setFieldValue={setRecommendation}
                theme={theme}
                touched={touched}
                value={get(values, 'reviseQualifier') || values.recommendation}
              />

              <Editor
                bold
                error={errors.content}
                italic
                key={submitted}
                label={reviewTextAreaLabel}
                name="content"
                placeholder={reviewTextAreaPlaceholder}
                readOnly={submitted}
                required
                setFieldTouched={setFieldTouched}
                setFieldValue={setFieldValue}
                subscript
                superscript
                touched={touched}
                value={values.content}
              />

              {showConfidentialCommentsEditor && (
                <Editor
                  bold
                  error={errors.confidentialComments}
                  italic
                  key={submitted}
                  label="Confidential comments to the editor"
                  name="confidentialComments"
                  placeholder="Comments could include concerns about the data quality, issues related to plagiarism, inappropriate figure manipulation, etc. These comments will not be shown to the author(s)."
                  readOnly={submitted}
                  setFieldTouched={setFieldTouched}
                  setFieldValue={setFieldValue}
                  subscript
                  superscript
                  touched={touched}
                  value={values.confidentialComments}
                />
              )}

              <Checkbox
                checkBoxText={openAcknowledgementLabel}
                checked={values.openAcknowledgement}
                name="openAcknowledgement"
                onChange={handleChange}
                readOnly={submitted}
                setFieldTouched={setFieldTouched}
                touched={touched}
              />

              {showRequestToSeeRevision && (
                <Checkbox
                  checkBoxText="I would like to see the revised article"
                  checked={values.askedToSeeRevision}
                  name="askedToSeeRevision"
                  onChange={handleChange}
                  readOnly={submitted}
                  setFieldTouched={setFieldTouched}
                  touched={touched}
                />
              )}

              {!submitted && (
                <Button primary type="submit">
                  Submit
                </Button>
              )}

              <AutoSave onSave={save} values={values} />
            </>
          )
        }}
      </Form>
    </Wrapper>
  )
}

const validDecisions = ['accept', 'reject', 'revise']

ReviewerPanel.propTypes = {
  canStillReview: PropTypes.bool.isRequired,
  dbReferenceId: PropTypes.string,
  decision: PropTypes.oneOf(validDecisions),
  doi: PropTypes.string,
  onClickChat: PropTypes.func.isRequired,
  openAcknowledgementLabel: PropTypes.string,
  makeOptions: PropTypes.func,
  review: PropTypes.shape({
    content: PropTypes.string,
    recommendation: PropTypes.oneOf(validDecisions),
    reviseQualifier: PropTypes.string,
    openAcknowledgement: PropTypes.bool,
    askedToSeeRevision: PropTypes.bool,
  }),
  reviewTextAreaLabel: PropTypes.string,
  reviewTextAreaPlaceholder: PropTypes.string,
  save: PropTypes.func,
  showConfidentialCommentsEditor: PropTypes.bool,
  showRequestToSeeRevision: PropTypes.bool,
  showReviewerGuidelines: PropTypes.func,
  submit: PropTypes.func,
  submitted: PropTypes.bool,
}

ReviewerPanel.defaultProps = {
  dbReferenceId: null,
  decision: null,
  doi: null,
  openAcknowledgementLabel:
    'I would like to be openly acknowledged as the reviewer',
  makeOptions: null,
  review: null,
  reviewTextAreaLabel:
    'In the space below please explain acceptance, rejection, or detail the modifications that should be made. If modifications are requested, enumerate each point the author(s) should address.',
  reviewTextAreaPlaceholder: 'Write your review',
  save: null,
  showConfidentialCommentsEditor: true,
  showRequestToSeeRevision: true,
  showReviewerGuidelines: null,
  submit: null,
  submitted: false,
}

export default withTheme(ReviewerPanel)

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { List } from '@pubsweet/ui/dist/molecules'
import { th } from '@pubsweet/ui-toolkit'

import { Loader, PageHeader as DefaultPageHeader } from '../common'
import UserSectionItem from './UserSectionItem'

const PageWrapper = styled.div`
  margin: 0 auto;
  max-width: 800px;
`

const PageHeader = styled(DefaultPageHeader)`
  margin-block-end: ${th('gridUnit')};
`

const UserManager = props => {
  const { loading, users } = props

  if (loading) return <Loader />

  return (
    <PageWrapper>
      <PageHeader>User Manager</PageHeader>
      {users && <List component={UserSectionItem} items={users} />}
    </PageWrapper>
  )
}

UserManager.propTypes = {
  /** Whether the data is still being fetched */
  loading: PropTypes.bool,
  /** The list of users to display */
  users: PropTypes.arrayOf(PropTypes.object.isRequired).isRequired,
}

UserManager.defaultProps = {
  loading: false,
}

export default UserManager

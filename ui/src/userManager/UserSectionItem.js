import React, { useState } from 'react'
import PropTypes from 'prop-types'
import * as yup from 'yup'

import { Accordion, Button, Form, TextField } from '../common'
import ValueList from '../editors/ValueList'

const validations = yup.object().shape({
  username: yup.string().required('Username cannot be empty'),
  givenNames: yup.string(),
  surname: yup.string().required('Surname cannot be empty'),
  orcid: yup.string(),
})

const UserSectionItem = props => {
  const {
    admin,
    displayName,
    email,
    givenNames,
    orcid,
    surname,
    updateUserData,
    username,
    id: userId,
  } = props

  const isAdmin = admin ? 'Admin' : 'Not Admin'

  const data = [
    {
      label: 'Username',
      status: 'primary',
      value: username,
    },
    {
      label: 'Given Names',
      status: 'primary',
      value: givenNames,
    },
    {
      label: 'Surname',
      status: 'primary',
      value: surname,
    },
    {
      label: 'Admin',
      status: 'primary',
      value: isAdmin,
    },
    {
      label: 'E-Mail',
      status: 'primary',
      value: email,
    },
    {
      label: 'ORCiD',
      status: 'primary',
      value: orcid,
    },
  ]

  const [showForm, setShowForm] = useState(false)

  const initialValues = {
    username: username || '',
    givenNames: givenNames || '',
    surname: surname || '',
    orcid: orcid || '',
  }

  const handleSubmit = (values, formikBag) => {
    updateUserData({ userId, ...values }).then(() => setShowForm(false))
    setShowForm(false)
  }

  return (
    <Accordion label={displayName}>
      {!showForm && (
        <div>
          <ValueList data={data} />
          <Button onClick={() => setShowForm(true)} primary>
            Edit
          </Button>
        </div>
      )}

      {showForm && (
        <Form
          initialValues={initialValues}
          onSubmit={handleSubmit}
          validationSchema={validations}
        >
          {formProps => {
            const {
              errors,
              handleBlur,
              handleChange,
              touched,
              values,
            } = formProps

            return (
              <div>
                <TextField
                  error={errors.username}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  label="Username"
                  name="username"
                  touched={touched}
                  value={values.username}
                />

                <TextField
                  error={errors.givenNames}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  label="Given Names"
                  name="givenNames"
                  touched={touched}
                  value={values.givenNames}
                />

                <TextField
                  error={errors.surname}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  label="Surname"
                  name="surname"
                  touched={touched}
                  value={values.surname}
                />

                <TextField
                  error={errors.orcid}
                  handleBlur={handleBlur}
                  handleChange={handleChange}
                  label="ORCiD"
                  name="orcid"
                  touched={touched}
                  value={values.orcid}
                />

                <Button onClick={() => setShowForm(false)}>Cancel</Button>
                <Button primary type="submit">
                  Save
                </Button>
              </div>
            )
          }}
        </Form>
      )}
    </Accordion>
  )
}

UserSectionItem.propTypes = {
  admin: PropTypes.bool.isRequired,
  email: PropTypes.string.isRequired,
  displayName: PropTypes.string.isRequired,
  givenNames: PropTypes.string,
  orcid: PropTypes.string,
  surname: PropTypes.string,
  updateUserData: PropTypes.func.isRequired,
  username: PropTypes.string,
  id: PropTypes.string.isRequired,
}

UserSectionItem.defaultProps = {
  givenNames: '',
  orcid: '',
  surname: '',
  username: '',
}

export default UserSectionItem

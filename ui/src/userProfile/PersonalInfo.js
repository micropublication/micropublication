import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { isEmpty } from 'lodash'
import * as yup from 'yup'

import { Button, Form, TextField } from '../common'

const Wrapper = styled.div``

const PersonalInfo = props => {
  const { className, givenNames, surname, update, orcid } = props

  const initialValues = {
    givenNames,
    orcid,
    surname,
  }

  const validations = yup.object().shape({
    givenNames: yup.string().required('Given names are required'),
    orcid: yup.string().nullable(),
    surname: yup.string().required('Surname is required'),
  })

  return (
    <Wrapper className={className}>
      <Form
        initialValues={initialValues}
        onSubmit={update}
        validationSchema={validations}
      >
        {formProps => {
          const {
            errors,
            handleBlur,
            handleChange,
            touched,
            values,
          } = formProps

          const isValid = isEmpty(errors)
          const disabled =
            (values.givenNames === initialValues.givenNames &&
              values.surname === initialValues.surname &&
              values.orcid === initialValues.orcid) ||
            !isValid

          return (
            <>
              <TextField
                error={errors.givenNames}
                handleBlur={handleBlur}
                handleChange={handleChange}
                label="Given Names"
                name="givenNames"
                touched={touched}
                value={values.givenNames}
              />

              <TextField
                error={errors.surname}
                handleBlur={handleBlur}
                handleChange={handleChange}
                label="Surname"
                name="surname"
                touched={touched}
                value={values.surname}
              />

              <TextField
                handleBlur={handleBlur}
                handleChange={handleChange}
                label="ORCID iD"
                name="orcid"
                touched={touched}
                value={values.orcid}
              />

              <Button disabled={disabled} primary type="submit">
                Update
              </Button>
            </>
          )
        }}
      </Form>
    </Wrapper>
  )
}

PersonalInfo.propTypes = {
  givenNames: PropTypes.string.isRequired,
  surname: PropTypes.string.isRequired,
  orcid: PropTypes.string,
  update: PropTypes.func.isRequired,
}

PersonalInfo.defaultProps = {
  orcid: null,
}

export default PersonalInfo

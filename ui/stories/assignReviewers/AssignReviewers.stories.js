import React, { useState } from 'react'
import styled from 'styled-components'
import { range } from 'lodash'
import { internet, name, random } from 'faker'

import AssignReviewers from '../../src/assignReviewers/AssignReviewers'
import { Note } from '../../src/common'
import { grid } from '../../src/_helpers'

const Wrapper = styled.div`
  margin-bottom: 100px;
`

const StyledNote = styled(Note)`
  margin-bottom: ${grid(2)};
`

const Separator = styled.div`
  border-bottom: 2px solid gray;
  margin: 32px 0;
`

const ButtonsWrapper = styled.div`
  > button {
    margin-right: 4px;
  }
`

const makeReviewers = n =>
  range(n).map(() => ({
    displayName: name.findName(),
    email: internet.email(),
    id: random.uuid(),
    invited: false,
    invitationRevoked: false,
    isSignedUp: true,
    acceptedInvitation: false,
    rejectedInvitation: false,
    reviewSubmitted: false,
  }))

const suggestedReviewer = name.findName()

const isActive = r => r.invited && !r.invitationRevoked && !r.rejectedInvitation

const isAvailable = r => !r.invited
// || !r.invitationRevoked || !r.rejectedInvitation

export const Base = () => {
  const [reviewers, setReviewers] = useState(makeReviewers(40))
  const [pool, setPool] = useState(makeReviewers(8))
  const [automation, setAutomation] = useState(false)
  const [amountOfReviewers, setAmountOfReviewers] = useState(2)

  const onReorder = newList => setPool(newList)

  const handleClickReviewer = optionClicked => {
    const reviewer = optionToReviewerData(optionClicked)
    setPool([...pool, reviewer])
    setReviewers(reviewers.filter(r => r.id !== reviewer.id))
  }

  const handleClickRemoveRow = rowId => {
    const item = pool.find(r => r.id === rowId)
    setPool(pool.filter(r => r.id !== rowId))
    setReviewers([item, ...reviewers])
  }

  const findAvailableSlots = () => {
    const active = pool.filter(r => isActive(r))
    const reviewerSlotsLeft = amountOfReviewers - active.length

    if (reviewerSlotsLeft < 0) return 0
    return reviewerSlotsLeft
  }

  const canInviteMore = () => {
    const available = findAvailableSlots()
    return available > 0
  }

  const runAutomation = () => {
    const reviewerSlotsLeft = findAvailableSlots()

    // invite as many as allowed
    const notInvited = pool.filter(r => isAvailable(r))

    const reviewerIdsToInvite = notInvited
      .slice(0, reviewerSlotsLeft)
      .map(r => r.id)

    const poolClone = [...pool]

    reviewerIdsToInvite.forEach(id => {
      const obj = poolClone.find(i => i.id === id)
      const index = poolClone.indexOf(obj)
      obj.invited = true
      poolClone[index] = obj
    })

    setPool(poolClone)
  }

  const handleAmountOfReviewersChange = value => {
    setAmountOfReviewers(value)
  }

  const handleClickInvite = reviewerId => {
    if (!canInviteMore()) return

    const poolClone = [...pool]
    const reviewer = poolClone.find(r => r.id === reviewerId)

    // reinvited
    if (reviewer.invited && reviewer.invitationRevoked) {
      reviewer.invitationRevoked = false
    } else {
      reviewer.invited = true
    }

    setPool(poolClone)
  }

  const handleClickRevokeInvitation = reviewerId => {
    const poolClone = [...pool]
    const reviewer = poolClone.find(r => r.id === reviewerId)
    reviewer.invitationRevoked = true
    setPool(poolClone)

    if (automation) runAutomation()
  }

  const handleRejectInvitation = () => {
    const poolClone = [...pool]
    const reviewer = poolClone.find(
      r =>
        r.invited &&
        !r.acceptedInvitation &&
        !r.rejectedInvitation &&
        !r.invitationRevoked,
    )
    if (!reviewer) return

    reviewer.rejectedInvitation = true
    setPool(poolClone)

    if (automation) runAutomation()
  }

  const handleAcceptInvitation = () => {
    const poolClone = [...pool]
    const reviewer = poolClone.find(
      r =>
        r.invited &&
        !r.acceptedInvitation &&
        !r.rejectedInvitation &&
        !r.invitationRevoked,
    )
    if (!reviewer) return

    reviewer.acceptedInvitation = true
    setPool(poolClone)

    if (automation) runAutomation()
  }

  const handleSubmitReview = () => {
    const poolClone = [...pool]
    const reviewer = poolClone.find(
      r => r.acceptedInvitation && !r.reviewSubmitted,
    )
    if (!reviewer) return

    reviewer.reviewSubmitted = true
    setPool(poolClone)

    if (automation) runAutomation()
  }

  const resetState = () => {
    setReviewers(makeReviewers(10))
    setPool(makeReviewers(8))
    setAutomation(false)
  }

  const handleClickStart = () => {
    if (automation) return // already on

    setAutomation(true)
    runAutomation()
  }

  const handleClickStop = () => {
    setAutomation(false)
  }

  const reviewerDataToOption = reviewer => ({
    label: reviewer.displayName,
    value: reviewer.id,
  })

  const optionToReviewerData = option =>
    reviewers.find(r => r.id === option.value)

  const search = input =>
    new Promise(resolve =>
      setTimeout(() => {
        const typed = input && input.toLowerCase()
        const found = reviewers.filter(person => {
          const fullName = person.displayName.toLowerCase()
          return fullName.includes(typed)
        })

        const results = found.map(item => reviewerDataToOption(item))

        resolve(results)
      }, 1000),
    )

  return (
    <Wrapper>
      <StyledNote>
        Add reviewers to the pool and order them by preference via drag and
        drop. Starting the automation will invite the first {amountOfReviewers}{' '}
        reviewers it finds from the top of the list that are available. While
        automation is on, revoking or rejecting an invitation will move to find
        a new reviewer. You can also invite or revoke invitations manually, as
        long as you are allowed to (eg. cannot invite more than{' '}
        {amountOfReviewers} reviewers). Automation can be turned off at any
        point. The buttons below simulate what would happen for certain events
        that would be triggered server-side. The reset button will bring this
        demo to its original state with new data.
      </StyledNote>

      <ButtonsWrapper>
        <button onClick={handleAcceptInvitation} type="button">
          Reviewer accepts invitation
        </button>

        <button onClick={handleRejectInvitation} type="button">
          Reviewer rejects invitation
        </button>

        <button onClick={handleSubmitReview} type="button">
          Reviewer submits review
        </button>

        <button onClick={resetState} type="button">
          Reset state
        </button>
      </ButtonsWrapper>

      <Separator />

      <AssignReviewers
        amountOfReviewers={amountOfReviewers}
        automate={automation}
        canInviteMore={canInviteMore()}
        onAmountOfReviewersChange={handleAmountOfReviewersChange}
        onClickInvite={handleClickInvite}
        onClickRemoveRow={handleClickRemoveRow}
        onClickReviewer={handleClickReviewer}
        onClickRevokeInvitation={handleClickRevokeInvitation}
        onClickStart={handleClickStart}
        onClickStop={handleClickStop}
        onReviewerPoolReorder={onReorder}
        reviewerPool={pool}
        search={search}
        suggestedReviewerName={suggestedReviewer}
      />
    </Wrapper>
  )
}

export default {
  component: AssignReviewers,
  title: 'Assign Reviewers/Assign Reviewers',
}

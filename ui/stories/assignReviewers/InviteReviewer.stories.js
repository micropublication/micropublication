import React from 'react'

import InviteReviewer from '../../src/assignReviewers/InviteReviewer'

export const Base = () => (
  <InviteReviewer addExternalReviewer={vals => console.log(vals)} />
)

export const Disabled = () => (
  <InviteReviewer addExternalReviewer={vals => console.log(vals)} disabled />
)

export default {
  component: InviteReviewer,
  title: 'Assign Reviewers/InviteReviewer',
}

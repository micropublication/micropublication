import React, { useState } from 'react'
import { internet, name, random } from 'faker'
import { range } from 'lodash'

import ReviewerList from '../../src/assignReviewers/ReviewerList'

const makeReviewers = n =>
  range(n).map(() => ({
    displayName: name.findName(),
    id: random.uuid(),
    isSignedUp: true,
    email: internet.email(),
  }))

export const Base = () => {
  const [reviewers, setReviewers] = useState(makeReviewers(8))

  const onReorder = newList => {
    setReviewers(newList)
  }

  const onClickRemoveRow = rowId => {
    setReviewers(reviewers.filter(r => r.id !== rowId))
  }

  return (
    <ReviewerList
      onClickRemoveRow={onClickRemoveRow}
      onReorder={onReorder}
      reviewers={reviewers}
    />
  )
}

export const Emtpy = () => {
  const onReorder = () => {}
  return <ReviewerList onReorder={onReorder} />
}

export const WithEmails = () => {
  const [reviewers, setReviewers] = useState(makeReviewers(3))

  const onReorder = newList => {
    setReviewers(newList)
  }

  const onClickRemoveRow = rowId => {
    setReviewers(reviewers.filter(r => r.id !== rowId))
  }

  return (
    <ReviewerList
      onClickRemoveRow={onClickRemoveRow}
      onReorder={onReorder}
      reviewers={reviewers}
      showReviewerEmails
    />
  )
}

export default {
  component: ReviewerList,
  title: 'Assign Reviewers/Reviewer List',
}

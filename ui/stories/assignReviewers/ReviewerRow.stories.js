import React from 'react'
import { internet, name } from 'faker'

import ReviewerRow from '../../src/assignReviewers/ReviewerRow'

export const Base = () => (
  <ReviewerRow
    acceptedInvitation={false}
    displayName={name.findName()}
    email={internet.email()}
    invitationRevoked={false}
    invited={false}
    isSignedUp
    rejectedInvitation={false}
    reviewSubmitted={false}
  />
)

export const Invited = () => (
  <ReviewerRow
    acceptedInvitation={false}
    displayName={name.findName()}
    email={internet.email()}
    invitationRevoked={false}
    invited
    isSignedUp
    rejectedInvitation={false}
    reviewSubmitted={false}
  />
)

export const RejectedInvitation = () => (
  <ReviewerRow
    acceptedInvitation={false}
    displayName={name.findName()}
    email={internet.email()}
    invitationRevoked={false}
    invited
    isSignedUp
    rejectedInvitation
    reviewSubmitted={false}
  />
)

export const InvitationRevoked = () => (
  <ReviewerRow
    acceptedInvitation={false}
    displayName={name.findName()}
    email={internet.email()}
    invitationRevoked
    invited
    isSignedUp
    rejectedInvitation={false}
    reviewSubmitted={false}
  />
)

export const ReviewPending = () => (
  <ReviewerRow
    acceptedInvitation
    displayName={name.findName()}
    email={internet.email()}
    invitationRevoked={false}
    invited
    isSignedUp
    rejectedInvitation={false}
    reviewSubmitted={false}
  />
)

export const ReviewSubmitted = () => (
  <ReviewerRow
    acceptedInvitation
    displayName={name.findName()}
    email={internet.email()}
    invitationRevoked={false}
    invited
    isSignedUp
    rejectedInvitation={false}
    reviewSubmitted
  />
)

export const NotSignedUp = () => (
  <ReviewerRow
    acceptedInvitation={false}
    displayName={name.findName()}
    email={internet.email()}
    invitationRevoked={false}
    invited
    isSignedUp={false}
    rejectedInvitation={false}
    reviewSubmitted={false}
  />
)

export const ShowEmail = () => (
  <ReviewerRow
    acceptedInvitation={false}
    displayName={name.findName()}
    email={internet.email()}
    invitationRevoked={false}
    invited
    isSignedUp={false}
    rejectedInvitation={false}
    reviewSubmitted={false}
    showEmail
  />
)

export default {
  component: ReviewerRow,
  title: 'Assign Reviewers/Reviewer Row',
}

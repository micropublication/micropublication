import React from 'react'
import { MemoryRouter } from 'react-router-dom'
import { internet } from 'faker'

import RequestPasswordReset from '../../src/authentication/RequestPasswordReset'

export const Base = () => <RequestPasswordReset />

export const EmailSubmitted = () => (
  <RequestPasswordReset recipient={internet.email()} />
)

export const Sending = () => <RequestPasswordReset sending />

export const Error = () => <RequestPasswordReset error="This is an error!" />

export default {
  component: RequestPasswordReset,
  decorators: [storyFn => <MemoryRouter>{storyFn()}</MemoryRouter>],
  title: 'Authentication/Request Password Reset',
}

import React from 'react'
import { MemoryRouter } from 'react-router-dom'

import ResetPassword from '../../src/authentication/ResetPassword'

export const Base = () => <ResetPassword />

export const VerifyingToken = () => <ResetPassword verifyingToken />

export const Error = () => <ResetPassword error="Token expired" />

export const Success = () => <ResetPassword success />

export const Expired = () => <ResetPassword expired />

export default {
  component: ResetPassword,
  decorators: [storyFn => <MemoryRouter>{storyFn()}</MemoryRouter>],
  title: 'Authentication/Reset Password',
}

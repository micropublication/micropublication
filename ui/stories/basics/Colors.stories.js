/* eslint-disable react/prop-types */

import React from 'react'
import styled, { withTheme } from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'

const ColorWrapper = styled.div`
  align-items: center;
  display: flex;
  margin: 32px 0;
  width: 25%;
`

const Label = styled.div`
  color: ${th('colorTextPlaceholder')};
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBaseSmall')};
  padding-left: 16px;
`

const ColorBox = styled.div`
  background-color: ${props => props.color};
  border-radius: 10%;
  /* box-shadow: -2px 2px 14px #d6d6d6; */
  height: 50px;
  width: 50px;

  /* stylelint-disable-next-line order/properties-alphabetical-order */
  ${props =>
    (props.color === 'white' || props.color === '#FFF') &&
    `border: 2px solid gray;`}
`

const Color = props => {
  const { color, label } = props

  return (
    <ColorWrapper>
      <ColorBox color={color} />
      <Label>{label}</Label>
    </ColorWrapper>
  )
}

const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
`

const Colors = props => {
  const {
    colorAddition,
    colorBackground,
    colorBackgroundHue,
    colorBorder,
    colorError,
    colorSuccess,
    colorWarning,
    colorFurniture,
    colorSecondary,
    colorPrimary,
    colorRemoval,
    colorText,
    colorTextPlaceholder,
    colorTextReverse,
  } = props.theme

  return (
    <Wrapper>
      <Color color={colorPrimary} label="colorPrimary" />
      <Color color={colorSecondary} label="colorSecondary" />
      <Color color={colorBackground} label="colorBackground" />
      <Color color={colorBackgroundHue} label="colorBackgroundHue" />
      <Color color={colorSuccess} label="colorSuccess" />
      <Color color={colorWarning} label="colorWarning" />
      <Color color={colorError} label="colorError" />
      <Color color={colorFurniture} label="colorFurniture" />
      <Color color={colorText} label="colorText" />
      <Color color={colorTextPlaceholder} label="colorTextPlaceholder" />
      <Color color={colorTextReverse} label="colorTextReverse" />
      <Color color={colorBorder} label="colorBorder" />
      <Color color={colorAddition} label="colorAddition" />
      <Color color={colorRemoval} label="colorRemoval" />
    </Wrapper>
  )
}

const ThemedColors = withTheme(Colors)

export const Base = () => <ThemedColors />

export default {
  title: 'Basics/Colors',
}

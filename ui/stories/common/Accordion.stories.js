import React from 'react'
import styled from 'styled-components'
import { lorem } from 'faker'

import Accordion from '../../src/common/Accordion'

const Box = styled.div`
  background: ${props => props.theme.colorPrimary};
  height: 100px;
  margin: 5px;
`

export const Base = () => (
  <Accordion label={lorem.words(4)}>
    <Box />
  </Accordion>
)

export const StartExpanded = () => (
  <Accordion label={lorem.words(4)} startExpanded>
    <Box />
  </Accordion>
)

export default {
  component: Accordion,
  title: 'Common/Accordion',
}

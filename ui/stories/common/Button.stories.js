import React from 'react'

import Button from '../../src/common/Button'

export const Base = () => <Button>Click me</Button>

export const Primary = () => <Button primary>Click me</Button>

export const PrimaryDisabled = () => (
  <Button disabled primary>
    Click me
  </Button>
)

export const PrimaryLoading = () => (
  <Button loading primary>
    Click me
  </Button>
)

export const PrimaryLoadingDisabled = () => (
  <Button disabled loading primary>
    Click me
  </Button>
)

export default {
  component: Button,
  title: 'Common/Button',
}

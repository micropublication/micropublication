import React from 'react'
import styled from 'styled-components'
import { grid, th } from '../../src/_helpers'

import LoaderInline from '../../src/common/LoaderInline'

const PrimaryBackground = styled.div`
  background: ${th('colorPrimary')};
  padding: ${grid(4)};
`

export const Base = () => <LoaderInline />

export const ChangeSizeToSmaller = () => <LoaderInline size={2} />

export const ChangeSizeToBigger = () => <LoaderInline size={10} />

export const PrimaryColor = () => <LoaderInline color="colorPrimary" />

export const ReverseTextColor = () => (
  <PrimaryBackground>
    <LoaderInline color="colorTextReverse" />
  </PrimaryBackground>
)

export default {
  component: LoaderInline,
  title: 'Common/Loader Inline',
}

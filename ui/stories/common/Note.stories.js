import React from 'react'
import { lorem } from 'faker'

import Note from '../../src/common/Note'

export const Base = () => <Note>{lorem.sentences(15)}</Note>

export default {
  component: Note,
  title: 'Common/Note',
}

import React from 'react'

import Status from '../../src/common/Status'

export const Base = () => <Status>some status</Status>

export const Success = () => <Status status="success">accepted</Status>
export const Error = () => <Status status="error">rejected</Status>
export const Warn = () => <Status status="warning">watch out</Status>
export const Primary = () => <Status status="primary">primary</Status>

export const NeutralReverse = () => <Status reverseColors>neutral</Status>

export const SuccessReverse = () => (
  <Status reverseColors status="success">
    success
  </Status>
)

export const ErrorReverse = () => (
  <Status reverseColors status="error">
    error
  </Status>
)

export const WarnReverse = () => (
  <Status reverseColors status="warning">
    watch out
  </Status>
)

export const PrimaryReverse = () => (
  <Status reverseColors status="primary">
    primary
  </Status>
)

export default {
  component: Status,
  title: 'Common/Status',
}

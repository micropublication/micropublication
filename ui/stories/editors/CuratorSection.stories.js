import React from 'react'
import { lorem, name, random } from 'faker'

import CuratorSection from '../../src/editors/CuratorSection'

export const Base = () => (
  <CuratorSection
    content={lorem.sentences(10)}
    curatorId={random.uuid()}
    curatorName={`${name.findName()}`}
    onClickChat={() => console.log('open curator chat')}
    openAcknowledgement
    pending={false}
    recommendation="accept"
    showChat
  />
)

export const Pending = () => (
  <CuratorSection
    curatorId={random.uuid()}
    curatorName={name.findName()}
    onClickChat={() => console.log('open curator chat')}
    pending
    showChat
  />
)

export const Empty = () => <CuratorSection />

export default {
  component: CuratorSection,
  title: 'Editors/Curator Section',
}

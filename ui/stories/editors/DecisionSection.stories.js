import React from 'react'
import { lorem, name } from 'faker'

import DecisionSection from '../../src/editors/DecisionSection'

export const Base = () => (
  <DecisionSection
    articleTitle={lorem.sentence()}
    authorName={name.findName()}
    submitDecision={values => console.log(values)}
  />
)

export const Submitted = () => (
  <DecisionSection
    decision="revise"
    decisionLetter={lorem.sentences(10)}
    submitted
  />
)

export default {
  component: DecisionSection,
  title: 'Editors/Decision Section',
}

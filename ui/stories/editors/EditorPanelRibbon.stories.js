import React from 'react'

import EditorPanelRibbon from '../../src/editors/EditorPanelRibbon'

export const Base = () => <EditorPanelRibbon />

export const DatatypeSelected = () => (
  <EditorPanelRibbon dataType="noDatatype" />
)

DatatypeSelected.story = {
  name: 'Datatype selected, but no editor decision yet',
}

export const Accepted = () => (
  <EditorPanelRibbon dataType="noDatatype" decision="accept" />
)

export const Declined = () => (
  <EditorPanelRibbon dataType="noDatatype" decision="decline" />
)

export const Rejected = () => (
  <EditorPanelRibbon dataType="noDatatype" decision="reject" />
)

export const Revise = () => (
  <EditorPanelRibbon dataType="noDatatype" decision="revise" />
)

export default {
  component: EditorPanelRibbon,
  title: 'Editors/Editor Panel Ribbon',
}

import React, { useState } from 'react'

import MetadataSection from '../../src/editors/MetadataSection'

export const Base = () => {
  const [values, setValues] = useState({
    doi: '743748923748239477238947923',
  })

  const update = vals => {
    setValues(vals)
    return Promise.resolve()
  }

  const createDoi = () =>
    new Promise(resolve => {
      setTimeout(() => resolve('564827509234758942370596546'), 1000)
    })

  return (
    <MetadataSection
      categories={values.categories}
      createDoi={createDoi}
      dbReferenceId={values.dbReferenceId}
      doi={values.doi}
      pmcId={values.pmcId}
      pmId={values.pmId}
      species={values.species}
      submissionTypes={values.submissionTypes}
      updateMetadata={update}
    />
  )
}

export default {
  component: MetadataSection,
  title: 'Editors/Metadata Section',
}

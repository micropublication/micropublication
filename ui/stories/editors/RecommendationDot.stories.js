import React from 'react'

import RecommendationDot from '../../src/editors/RecommendationDot'

export const Base = () => <RecommendationDot />

export const Accept = () => <RecommendationDot recommendation="accept" />
export const Decline = () => <RecommendationDot recommendation="decline" />
export const Reject = () => <RecommendationDot recommendation="reject" />
export const Revise = () => <RecommendationDot recommendation="revise" />

export default {
  component: RecommendationDot,
  title: 'Editors/Recommendation Dot',
}

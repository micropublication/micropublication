import React from 'react'
import { lorem, name } from 'faker'

import Review from '../../src/editors/Review'

export const Base = () => (
  <Review
    askedToSeeRevision
    content={lorem.sentences(20)}
    pending={false}
    recommendation="revise"
    reviewerName={name.findName()}
  />
)

export const Pending = () => <Review pending reviewerName={name.findName()} />

export const OpenAcknowledgement = () => (
  <Review
    askedToSeeRevision={false}
    content={lorem.sentences(20)}
    openAcknowledgement
    pending={false}
    recommendation="accept"
    reviewerName={name.findName()}
  />
)

export const ShowChat = () => (
  <Review
    askedToSeeRevision
    content={lorem.sentences(20)}
    pending={false}
    recommendation="revise"
    reviewerName={name.findName()}
    showChat
  />
)

export const HideRequestToSeeRevision = () => (
  <Review
    content={lorem.sentences(20)}
    pending={false}
    recommendation="revise"
    reviewerName={name.findName()}
    showChat
    showRequestToSeeRevision={false}
  />
)

export default {
  component: Review,
  title: 'Editors/Review',
}

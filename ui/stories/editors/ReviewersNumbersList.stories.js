import React from 'react'
import { random } from 'faker'

import ReviewersNumbers from '../../src/editors/ReviewersNumbers'

export const Base = () => (
  <ReviewersNumbers
    accepted={random.number(20)}
    invited={random.number(20)}
    rejected={random.number(20)}
  />
)

export default {
  component: ReviewersNumbers,
  title: 'Editors/Reviewers Numbers',
}

import React from 'react'
import { lorem, name } from 'faker'

import AdditionalData from '../../src/preview/AdditionalData'

const fakeData = {
  comments: lorem.sentences(7),
  dataType: 'noDatatype',
  laboratory: lorem.words(3),
  suggestedReviewer: `${name.firstName()} ${name.lastName()}`,
}

const fakeDataGeneExpression = {
  comments: lorem.sentences(7),
  dataType: 'geneExpression',
  geneExpression: {
    antibodyUsed: lorem.words(3),
    detectionMethod: 'antibody',
  },
  laboratory: lorem.words(3),
  suggestedReviewer: `${name.firstName()} ${name.lastName()}`,
}

export const Base = () => <AdditionalData {...fakeData} />

export const WithGeneExpression = () => (
  <AdditionalData {...fakeDataGeneExpression} />
)

const fakeDataPrevious = {
  previousDataType: 'geneExpression',
  previousGeneExpression: {
    antibodyUsed: lorem.words(3),
    detectionMethod: 'antibody',
  },
  previousLaboratory: lorem.words(3),
  previousSuggestedReviewer: `${name.firstName()} ${name.lastName()}`,
}

const fakeDataCurrent = {
  comments: lorem.sentences(7),
  dataType: 'geneExpression',
  laboratory: lorem.words(3),
  suggestedReviewer: `${name.firstName()} ${name.lastName()}`,
  geneExpression: {
    antibodyUsed: lorem.words(3),
    detectionMethod: 'antibody',
  },
}

export const WithDiff = () => (
  <AdditionalData {...fakeDataPrevious} {...fakeDataCurrent} />
)

export const HideRemoved = () => (
  <AdditionalData
    {...fakeDataPrevious}
    {...fakeDataCurrent}
    showRemoved={false}
  />
)

export const HideDiff = () => (
  <AdditionalData {...fakeDataPrevious} {...fakeDataCurrent} showDiff={false} />
)

export default {
  component: AdditionalData,
  title: 'Preview/Addtional Data',
}

import React from 'react'

import DiffArray from '../../src/preview/DiffArray'
import Affiliation from '../../src/preview/Affiliation'

export const Base = () => (
  <Affiliation index={1} value="University of Chicago" />
)

export const Added = () => (
  <Affiliation index={3} isAdded value="University of Chicago" />
)

export const Removed = () => (
  <Affiliation index={2} isRemoved value="University of Chicago" />
)

const oldData = [
  {
    id: 111,
    index: 1,
    value: 'University of Chicago',
  },
  {
    id: 222,
    index: 2,
    value: 'University of Milwaukee',
  },
]

const newData = [
  {
    id: 222,
    index: 1,
    value: 'University of Milwaukee',
  },
  {
    id: 333,
    index: 2,
    value: 'UCLA',
  },
]

export const InsideDiffArray = () => (
  <DiffArray
    component={Affiliation}
    currentArray={newData}
    previousArray={oldData}
  />
)

export const InsideDiffArrayWithDiffHidden = () => (
  <DiffArray
    component={Affiliation}
    currentArray={newData}
    previousArray={oldData}
    showDiff={false}
  />
)

export default {
  component: Affiliation,
  title: 'Preview/Affiliation',
}

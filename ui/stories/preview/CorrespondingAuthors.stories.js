import React from 'react'
import { internet, name, random } from 'faker'
import { range } from 'lodash'

import CorrespondingAuthors from '../../src/preview/CorrespondingAuthors'

const authors = range(3).map(() => ({
  email: internet.email(),
  firstName: name.firstName(),
  id: random.uuid(),
  lastName: name.lastName(),
}))

export const Base = () => <CorrespondingAuthors data={authors.slice(0, 2)} />

export const WithDiff = () => (
  <CorrespondingAuthors
    data={authors.slice(1, 3)}
    previousData={authors.slice(0, 2)}
  />
)

export const WithDiffHideRemoved = () => (
  <CorrespondingAuthors
    data={authors.slice(1, 3)}
    previousData={authors.slice(0, 2)}
    showRemoved={false}
  />
)

export const WithDiffButHidden = () => (
  <CorrespondingAuthors
    data={authors.slice(1, 3)}
    previousData={authors.slice(0, 2)}
    showDiff={false}
  />
)

export default {
  component: CorrespondingAuthors,
  title: 'Preview/Corresponding Authors',
}

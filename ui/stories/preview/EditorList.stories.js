import React from 'react'
import { lorem } from 'faker'

import EditorList from '../../src/preview/EditorList'

const oldFirstValue = lorem.sentences(7)
const oldSecondValue = lorem.sentences(8)

// const newFirstValue = `${oldFirstValue
//   .split('.')
//   .slice(0, -2)
//   .join('.')}.`

const newSecondValue = `${oldSecondValue
  .split('.')
  .slice(0, -1)
  .concat(` ${lorem.sentence()}`)
  .join('.')}`

export const Base = () => (
  <EditorList
    label={lorem.words(2)}
    values={[lorem.sentences(7), lorem.sentences(8)]}
  />
)

export const WithDiff = () => (
  <EditorList
    label={lorem.words(2)}
    previousValues={[oldFirstValue, oldSecondValue]}
    values={[oldFirstValue, newSecondValue, lorem.sentences(7)]}
  />
)

WithDiff.story = {
  parameters: {
    docs: {
      storyDescription:
        'Providing the "previousValues" prop will trigger diffing. Please note that since this is an array of values, it diffs the whole block of text.',
    },
  },
}

export const WithDiffHidden = () => (
  <EditorList
    label={lorem.words(2)}
    previousValues={[oldFirstValue, oldSecondValue]}
    showDiff={false}
    values={[oldFirstValue, newSecondValue, lorem.sentences(7)]}
  />
)

WithDiffHidden.story = {
  name: 'Diff triggered, but hidden',
}

export const WithDiffHiddenRemovals = () => (
  <EditorList
    label={lorem.words(2)}
    previousValues={[oldFirstValue, oldSecondValue]}
    showRemoved={false}
    values={[oldFirstValue, newSecondValue, lorem.sentences(7)]}
  />
)

WithDiffHiddenRemovals.story = {
  name: 'Diff triggered, hide only removals',
}

export default {
  component: EditorList,
  title: 'Preview/Editor List',
}

import React from 'react'
import { lorem } from 'faker'
import { cloneDeep } from 'lodash'

import GeneExpression from '../../src/preview/GeneExpression'

const data = {
  antibodyUsed: lorem.words(3),
  backboneVector: {
    name: lorem.words(3),
  },
  coinjected: lorem.words(3),
  constructComments: lorem.sentence(),
  constructionDetails: lorem.sentence(),
  detectionMethod: null,
  dnaSequence: [
    {
      name: lorem.words(3),
    },
    {
      name: lorem.words(3),
    },
  ],
  expressionPattern: {
    name: lorem.words(3),
  },
  fusionType: {
    name: lorem.words(3),
  },
  genotype: lorem.words(3),
  injectionConcentration: lorem.words(3),
  inSituDetails: lorem.words(3),
  integratedBy: {
    name: lorem.words(3),
  },
  // observeExpression: {
  //   certainly: [
  //     {
  //       certainly: {
  //         name: '',
  //       },
  //       during: {
  //         name: '',
  //       },
  //       subcellularLocalization: {
  //         name: '',
  //       },
  //     },
  //   ],
  //   not: [
  //     {
  //       during: {
  //         name: '',
  //       },
  //       not: {
  //         name: '',
  //       },
  //       subcellularLocalization: {
  //         name: '',
  //       },
  //     },
  //   ],
  //   partially: [
  //     {
  //       during: {
  //         name: '',
  //       },
  //       partially: {
  //         name: '',
  //       },
  //       subcellularLocalization: {
  //         name: '',
  //       },
  //     },
  //   ],
  //   possibly: [
  //     {
  //       during: {
  //         name: '',
  //       },
  //       possibly: {
  //         name: '',
  //       },
  //       subcellularLocalization: {
  //         name: '',
  //       },
  //     },
  //   ],
  // },
  reporter: {
    name: lorem.words(3),
  },
  species: {
    name: 'Homo Sapiens',
  },
  strain: lorem.words(3),
  transgeneName: lorem.words(3),
  transgeneUsed: [
    {
      name: lorem.words(3),
    },
    {
      name: lorem.words(3),
    },
  ],
  utr: {
    name: lorem.words(3),
  },
  variation: {
    name: lorem.words(3),
  },
}

export const Base = () => <GeneExpression data={data} />

export const Antibody = () => (
  <GeneExpression
    data={(() => {
      data.detectionMethod = 'antibody'
      return data
    })()}
  />
)

Antibody.story = { name: 'Detection method: Antibody' }

export const ExistingTransgene = () => (
  <GeneExpression
    data={(() => {
      data.detectionMethod = 'existingTransgene'
      return data
    })()}
  />
)

ExistingTransgene.story = { name: 'Detection method: Existing transgene' }

export const GenomeEditing = () => (
  <GeneExpression
    data={(() => {
      data.detectionMethod = 'genomeEditing'
      return data
    })()}
  />
)

GenomeEditing.story = { name: 'Detection method: Genome editing' }

export const InSituHybridization = () => (
  <GeneExpression
    data={(() => {
      data.detectionMethod = 'inSituHybridization'
      return data
    })()}
  />
)

InSituHybridization.story = { name: 'Detection method: In situ hybridization' }

export const NewTransgene = () => (
  <GeneExpression
    data={(() => {
      data.detectionMethod = 'newTransgene'
      return data
    })()}
  />
)

NewTransgene.story = { name: 'Detection method: New transgene' }

export const WithObserveExpression = () => (
  <GeneExpression
    data={(() => {
      data.detectionMethod = 'antibody'
      data.observeExpression = {
        certainly: [
          {
            certainly: { name: lorem.words(3) },
            during: { name: '' },
            subcellularLocalization: { name: '' },
          },
        ],
      }
      return data
    })()}
  />
)

WithObserveExpression.story = {
  name: 'Detection method: Antibody with observe expression data',
}

export const WithDiff = () => (
  <GeneExpression
    data={(() => {
      const output = cloneDeep(data)

      output.detectionMethod = 'newTransgene'

      output.dnaSequence = data.dnaSequence.slice(0, 1)

      output.observeExpression = {
        certainly: [
          {
            certainly: { name: lorem.words(3) },
            during: { name: '' },
            subcellularLocalization: { name: '' },
          },
        ],
      }

      return output
    })()}
    previousData={(() => {
      const output = cloneDeep(data)

      output.expressionPattern = { name: lorem.words(3) }
      output.species = { name: 'Homo Neanderthalis' }

      output.detectionMethod = 'newTransgene'
      output.constructComments = lorem.sentence()

      output.observeExpression = {
        possibly: [
          {
            possibly: { name: lorem.words(3) },
            during: { name: '' },
            subcellularLocalization: { name: '' },
          },
        ],
      }

      return output
    })()}
  />
)

export const HideRemoved = () => (
  <GeneExpression
    data={(() => {
      const output = cloneDeep(data)

      output.detectionMethod = 'newTransgene'

      output.dnaSequence = data.dnaSequence.slice(0, 1)

      output.observeExpression = {
        certainly: [
          {
            certainly: { name: lorem.words(3) },
            during: { name: '' },
            subcellularLocalization: { name: '' },
          },
        ],
      }

      return output
    })()}
    previousData={(() => {
      const output = cloneDeep(data)

      output.expressionPattern = { name: lorem.words(3) }
      output.species = { name: 'Homo Neanderthalis' }

      output.detectionMethod = 'newTransgene'
      output.constructComments = lorem.sentence()

      output.observeExpression = {
        possibly: [
          {
            possibly: { name: lorem.words(3) },
            during: { name: '' },
            subcellularLocalization: { name: '' },
          },
        ],
      }

      return output
    })()}
    showRemoved={false}
  />
)

export const HideDiff = () => (
  <GeneExpression
    data={(() => {
      const output = cloneDeep(data)

      output.detectionMethod = 'newTransgene'

      output.dnaSequence = data.dnaSequence.slice(0, 1)

      output.observeExpression = {
        certainly: [
          {
            certainly: { name: lorem.words(3) },
            during: { name: '' },
            subcellularLocalization: { name: '' },
          },
        ],
      }

      return output
    })()}
    previousData={(() => {
      const output = cloneDeep(data)

      output.expressionPattern = { name: lorem.words(3) }
      output.species = { name: 'Homo Neanderthalis' }

      output.detectionMethod = 'newTransgene'
      output.constructComments = lorem.sentence()

      output.observeExpression = {
        possibly: [
          {
            possibly: { name: lorem.words(3) },
            during: { name: '' },
            subcellularLocalization: { name: '' },
          },
        ],
      }

      return output
    })()}
    showDiff={false}
  />
)

export default {
  component: GeneExpression,
  title: 'Preview/Gene Expression',
}

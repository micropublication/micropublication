import React from 'react'
import { lorem, internet, name, random } from 'faker'
import { cloneDeep, range } from 'lodash'
import { v4 as uuid } from 'uuid'

import Manuscript from '../../src/preview/Manuscript'
import Image from './_sampleFiles/sample.png'
import NewImage from './_sampleFiles/sample2.png'

/*
  DATA 
*/

const makeAuthors = amount =>
  range(amount).map(() => ({
    affiliations: ['University of Chicago', 'University of Michigan'],
    correspondingAuthor: random.boolean(),
    id: uuid(),
    equalContribution: random.boolean(),
    email: internet.email(),
    firstName: name.firstName(),
    lastName: name.lastName(),
  }))

const makeHTMLData = () =>
  `<p>${lorem.sentences(4)}</p><p>${lorem.sentences(4)}</p>`

const imageData = {
  name: 'sample',
  url: Image,
}

const geneExpression = {
  antibodyUsed: lorem.words(3),
  backboneVector: {
    name: lorem.words(3),
  },
  coinjected: lorem.words(3),
  constructComments: lorem.sentence(),
  constructionDetails: lorem.sentence(),
  detectionMethod: null,
  dnaSequence: [
    {
      name: lorem.words(3),
    },
    {
      name: lorem.words(3),
    },
  ],
  expressionPattern: {
    name: lorem.words(3),
  },
  fusionType: {
    name: lorem.words(3),
  },
  genotype: lorem.words(3),
  injectionConcentration: lorem.words(3),
  inSituDetails: lorem.words(3),
  integratedBy: {
    name: lorem.words(3),
  },
  // observeExpression: {
  //   certainly: [
  //     {
  //       certainly: {
  //         name: '',
  //       },
  //       during: {
  //         name: '',
  //       },
  //       subcellularLocalization: {
  //         name: '',
  //       },
  //     },
  //   ],
  //   not: [
  //     {
  //       during: {
  //         name: '',
  //       },
  //       not: {
  //         name: '',
  //       },
  //       subcellularLocalization: {
  //         name: '',
  //       },
  //     },
  //   ],
  //   partially: [
  //     {
  //       during: {
  //         name: '',
  //       },
  //       partially: {
  //         name: '',
  //       },
  //       subcellularLocalization: {
  //         name: '',
  //       },
  //     },
  //   ],
  //   possibly: [
  //     {
  //       during: {
  //         name: '',
  //       },
  //       possibly: {
  //         name: '',
  //       },
  //       subcellularLocalization: {
  //         name: '',
  //       },
  //     },
  //   ],
  // },
  reporter: {
    name: lorem.words(3),
  },
  species: {
    name: 'Homo Sapiens',
  },
  strain: lorem.words(3),
  transgeneName: lorem.words(3),
  transgeneUsed: [
    {
      name: lorem.words(3),
    },
    {
      name: lorem.words(3),
    },
  ],
  utr: {
    name: lorem.words(3),
  },
  variation: {
    name: lorem.words(3),
  },
}

const version = {
  abstract: makeHTMLData(),
  acknowledgements: lorem.sentences(4),
  authors: makeAuthors(2),
  comments: makeHTMLData(),
  dataType: 'noDatatype',
  patternDescription: makeHTMLData(),
  funding: lorem.words(3),
  image: imageData,
  imageCaption: lorem.sentences(8),
  imageTitle: lorem.sentences(1),
  laboratory: {
    name: lorem.words(3),
  },
  methods: makeHTMLData(),
  reagents: makeHTMLData(),
  references: range(3).map(() => ({
    // doi: random.uuid(),
    // pubmedId: random.uuid(),
    reference: `<p>${lorem.sentences(2)}</p>`,
  })),
  suggestedReviewer: {
    name: `${name.firstName()} ${name.lastName()}`,
  },
  title: lorem.words(6),
}

const geneExpressionVersion = cloneDeep(version)
geneExpressionVersion.dataType = 'geneExpression'
geneExpressionVersion.geneExpression = geneExpression

const antibodyVersion = cloneDeep(geneExpressionVersion)
antibodyVersion.geneExpression.detectionMethod = 'antibody'

const newVersion = cloneDeep(antibodyVersion)
newVersion.title = version.title
  .split(' ')
  .slice(0, -1)
  .join(' ')
const newAuthor = makeAuthors(1)[0]
newAuthor.affiliations = ['UC Berkley']
// newAuthor.correspondingAuthor = false
newVersion.authors.push(newAuthor)
newVersion.image = {
  name: 'second sample',
  url: NewImage,
}
newVersion.imageCaption = `${version.imageCaption
  .split('.')
  .slice(0, -2)
  .join('.')}.`
newVersion.patternDescription = makeHTMLData()
newVersion.references = version.references.slice(0, -1)
newVersion.acknowledgements = `${version.acknowledgements
  .split('. ')
  .slice(0, 1)}. ${version.acknowledgements
  .split('. ')
  .slice(2)
  .join('. ')} ${lorem.sentence()}`

newVersion.funding = lorem.words(4)
newVersion.laboratory.name = lorem.words(4)
newVersion.geneExpression.antibodyUsed = lorem.words(3)

/* 
  END DATA
*/

export const Base = () => <Manuscript version={version} />

export const ShowAdditionalData = () => (
  <Manuscript showAdditionalData version={version} />
)

export const Antibody = () => (
  <Manuscript showAdditionalData version={antibodyVersion} />
)

Antibody.story = {
  name: 'Gene expression, Detection method: Antibody',
}

export const WithDiff = () => (
  <Manuscript
    previousVersion={antibodyVersion}
    showAdditionalData
    version={newVersion}
  />
)

export default {
  component: Manuscript,
  title: 'Preview/Manuscript',
}

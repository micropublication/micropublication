import React from 'react'

import Separator from '../../src/preview/Separator'

export const Base = () => <Separator />

export default {
  component: Separator,
  title: 'Preview/Separator',
}

/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'

import Split from '../../src/split/Split'

const StyledInnerComp = styled.div`
  align-items: center;
  background: ${th('colorBackground')};
  color: ${th('colorPrimary')};
  display: flex;
  height: 100%;
  justify-content: center;
`

const InnerComp = ({ text }) => (
  <StyledInnerComp>
    <span>{text}</span>
  </StyledInnerComp>
)

const Left = <InnerComp text="left" />
const Right = <InnerComp text="right" />

const StyledSplit = styled(Split)`
  height: 300px;
`

export const Base = () => <StyledSplit left={Left} right={Right} />

export const SplitAtThreeQuarters = () => (
  <StyledSplit left={Left} right={Right} splitOn={75} />
)

export default {
  component: Split,
  title: 'Article/Split',
}

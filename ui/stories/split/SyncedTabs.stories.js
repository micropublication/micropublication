import React from 'react'
import styled from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'

import SyncedTabs from '../../src/split/SyncedTabs'

const ContentComponent = styled.div`
  background: ${props =>
    props.primary ? th('colorPrimary') : th('colorBackground')};
  border: 1px solid
    ${props => (props.primary ? th('colorBackground') : th('colorPrimary'))};
  color: ${props =>
    props.primary ? th('colorTextReverse') : th('colorPrimary')};
  height: 200px;
  padding-top: 90px;
  text-align: center;
  width: 100%;
`

const sections = [
  {
    key: '1',
    label: 'First',
    content: <ContentComponent>something</ContentComponent>,
  },
  {
    key: '2',
    label: 'Second',
    content: <ContentComponent primary>something else</ContentComponent>,
  },
]

export const Base = () => (
  <SyncedTabs
    leftHeader="Left"
    leftSections={sections}
    rightHeader="Right"
    rightSections={sections}
  />
)

export default {
  component: SyncedTabs,
  title: 'Article/SyncedTabs',
}

import React from 'react'
import { lorem } from 'faker'

import Feedback from '../../src/submissionForm/Feedback'

const data = [
  {
    label: 'Original',
    decisionLetter: lorem.sentences(4),
  },
  {
    label: 'Version 1',
    decisionLetter: lorem.sentences(4),
  },
]

export const Base = () => <Feedback data={data} />

export default {
  component: Feedback,
  title: 'Submission Form/Feedback',
}

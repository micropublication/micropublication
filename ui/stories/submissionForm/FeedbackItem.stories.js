import React from 'react'
import { lorem } from 'faker'

import FeedbackItem from '../../src/submissionForm/FeedbackItem'

export const Base = () => <FeedbackItem decisionLetter={lorem.sentences(6)} />

export default {
  component: FeedbackItem,
  title: 'Submission Form/Feedback Item',
}

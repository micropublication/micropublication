import React from 'react'

import Password from '../../src/userProfile/Password'

export const Base = () => (
  <Password update={vals => console.log('update!', vals)} />
)

export default {
  component: Password,
  title: 'User Profile/Password',
}

import React from 'react'
import { name, random } from 'faker'

import PersonalInfo from '../../src/userProfile/PersonalInfo'

export const Base = () => (
  <PersonalInfo
    givenNames={name.firstName()}
    orcid={random.uuid()}
    surname={name.lastName()}
    update={vals => console.log('update!', vals)}
  />
)

export default {
  component: PersonalInfo,
  title: 'User Profile/Personal Info',
}
